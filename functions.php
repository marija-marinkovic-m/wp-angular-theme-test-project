<?php 

define('AGENT_AWARDS', json_encode(array(
    '1' => 'Diamond', 
    '2' => 'Platinum', 
    '3' => 'Gold', 
    '4' => 'Silver', 
    '5' => 'Bronze'
)));

require_once('includes/theme-core.php');
require_once('endpoints/core.php');

require_once('includes/users-management.php');
require_once('includes/agent-rankings-parser.php');

/**
* Require Authentication 
*/
add_filter( 'rest_pre_dispatch', function( $result ){
	if ( is_user_logged_in() ) {
		return $result;
	} else {
		return new WP_Error( 'rest_requires_authentication', __( 'Using REST requires authentication.' ), array( 'status' => 403 ) );
	}
});

/**
 * Disable admin bar on the frontend of your website
 * for subscribers.
 */
function remax_theme_disable_admin_bar() { 
	//if ( ! current_user_can('edit_posts') ) {
		add_filter('show_admin_bar', '__return_false');	
	//}
}
add_action( 'after_setup_theme', 'remax_theme_disable_admin_bar' );
 
/**
 * Redirect back to homepage and not allow access to 
 * WP admin for Subscribers.
 */
function remax_theme_redirect_admin(){
	if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
		wp_redirect( site_url() );
		exit;		
	}
}
add_action( 'admin_init', 'remax_theme_redirect_admin' );

function admin_inline_js(){
	?>
    <script type="text/javascript">
        if (null == localStorage.getItem('authorized')) {
        <?php 
            $current_user = wp_get_current_user();
            $the_user = new stdClass();
            $the_user = $current_user->data;
            $the_user->roles = array('administrator');
            ?>
        localStorage.setItem('authorized', JSON.stringify(<?php echo json_encode($the_user) ?>));
        }
    </script>
    <?php 
}
add_action( 'admin_print_scripts', 'admin_inline_js' );

function login_inline_js_remax_intranet_enqueue_script() {
    if (!is_user_logged_in()) {
	?>
    <script type="text/javascript">
        localStorage.clear();
    </script>
    <?php 
    }
}
add_action( 'login_enqueue_scripts', 'login_inline_js_remax_intranet_enqueue_script', 1 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function the_theme_widgets_init() {
	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'the_theme_widgets_init' );


add_action('init', 'remax_init'); 
function remax_init() {
    register_nav_menus(
        array(
            'main-menu' => __( 'Main Menu' ), 
            'secondary-menu' => __( 'Useful Links' )
        )
    );
}
add_action('after_setup_theme', 'remax_setup');
function remax_setup() {
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
}

/**
* Remax Theme Assets
* Styles and scripts 
*/
add_action('wp_enqueue_scripts', 'remax_enqueue_scripts');
function remax_enqueue_scripts() {
    
    /**
    * Styles 
    */ 
    wp_enqueue_style( 'main', get_template_directory_uri() . '/ng/dist/assets/css/style.css' );
    wp_enqueue_style( 'quillcss', get_template_directory_uri() .'/ng/dist/assets/css/quill.css' );

    /**
    * Remove/Deregister
    */
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    wp_deregister_script('jquery');
    wp_deregister_script( 'wp-embed' );
    wp_deregister_script('wp-api');

    /**
    * Javasript 
    */
    wp_register_script('polyfills', get_template_directory_uri() . '/ng/dist/polyfills.bundle.js', array(), false, true);
    wp_register_script('vendor', get_template_directory_uri() . '/ng/dist/vendor.bundle.js', array(), false, true);
    wp_register_script('main', get_template_directory_uri() . '/ng/dist/main.bundle.js', array(), false, true);

    wp_enqueue_script('polyfills');
    wp_enqueue_script('vendor');

    remax_config();

    wp_enqueue_script('main');
}

/**
* Login Via Ajax 
*/
function rmx_ng_login() {
    // first check nonce 
    $security_check = check_ajax_referer('wp_rest', 'rest_nonce', false);
    if (!$security_check) {
        header('HTTP/1.1 403 Forbidden');
        wp_die();
    }

    header('Content-Type: application/json');

    $userData = array(
        "user_login" => @$_POST['email'], 
        "user_password" => @$_POST['password'], 
        "remember" => (boolean) @$_POST['rememberme']
    );

    $user_signon = wp_signon($userData, false);

    if (is_wp_error($user_signon)) {
        echo json_encode(array('loggedin' => false, 'message' => 'Wrong username or password'));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => 'Login successful, redirecting...', 'user' => $user_signon));
    }

    wp_die();
}
add_action('wp_ajax_rmxlogin', 'rmx_ng_login');
add_action('wp_ajax_nopriv_rmxlogin', 'rmx_ng_login');

/**
* Logout via Ajax 
*/
function rmx_ng_logout() {
    $security_check = check_ajax_referer('wp_rest', 'rest_nonce', false);
    if (!$security_check) {
        header('HTTP/1.1 403 Forbidden');
        wp_die();
    }

    header('Content-Type: application/json');
    wp_logout();

    echo json_encode(array('loggedout' => true, 'message' => 'You are now logged out'));
    wp_die();
}
add_action('wp_ajax_rmxlogout', 'rmx_ng_logout');
add_action('wp_ajax_nopriv_rmxlogout', 'rmx_ng_logout');

/**
* Forgot password via ajax 
*/
function rmx_ng_forgot_pass() {
    $security_check = check_ajax_referer('wp_rest', 'rest_nonce', false);
    if (!$security_check) {
        header('HTTP/1.1 403 Forbidden');
        wp_die();
    }

    header('Content-Type: application/json');    

    $account = @$_POST['user_login'];
    $msg = '';

    if (empty($account)) {
        $msg = 'Enter your e-mail address.';
    } else {
        if (!is_email($account)) {
            $msg = 'Invalid e-mail address.';
        }
        if (is_email() && !email_exists($account)) {
            $msg = 'There is no user registered with that email address.';
        }
    }

    if (empty($msg)) {
        $random_password = wp_generate_password();
        $user = get_user_by('email', $account);
        $update_user = wp_update_user(array(
            'ID' => $user->ID, 
            'user_pass' => $random_password
        ));

        // If updatae user returned and id (not is_wp_error) than send email containing new password 
        if (!is_wp_error($update_user)) {
            add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
            $from = get_option('admin_email', 'marija.marinkovic@eutelnet.com');
            $to = $user->user_email;
            $subject = 'Your new password';
            $sender = 'From: '. get_option('blogname') .' <'. $from .'>' ."\r\n";
            $message = 'Your new password is: '. $random_password;
            $headers[] = $sender;

            $mail = wp_mail($to, $subject, $message, $headers);
            remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

            if ($mail) {
                $success = 'Check your email address for your new password.';
            } else {
                $msg = 'System is unable to send you mail containing your new password';
            }
        } else {
            $msg = 'Oops! Something went wrong while updating your account.';
        }

        if (!empty($msg)) {
            // @TODO: remove new_pass from response 
            echo json_encode(array('loggedin' => false, 'message' => $msg, 'new_pass' => @$random_password));   
        }

        wp_die();
        
    }
}
add_action('wp_ajax_rmxforgot', 'rmx_ng_forgot_pass');
add_action('wp_ajax_nopriv_rmxforgot', 'rmx_ng_forgot_pass');

/**
* Reset Password via ajax 
*/
function rmx_ng_reset_pass() {
    $security_check = check_ajax_referer('wp_rest', 'rest_nonce', false);
    if (!$security_check) {
        header('HTTP/1.1 403 Forbidden');
        wp_die();
    }
    header('Content-Type: application/json');
    $errors = new WP_Error();

    $new_pass = @$_POST['pass1'];
    $confirmed_pass = @$_POST['pass2'];
    $user_id = (int) @$_POST['user_id'];

    $user = get_user_by('id', $user_id);

    if ($user === false) {
        $errors->add('user_not_validated', 'Something went wrong while trying to access your account. Please try later.');
    } else {
        // user valid 
        if (empty($new_pass) || empty($confirmed_pass)) {
            $errors->add('password_required', 'Password is required field.');
        }
        if (!empty($new_pass) && $confirmed_pass !== $new_pass) {
            $errors->add('password_reset_mismatch', 'The passwords do not match.');
        }

        do_action('validate_password_reset', $errors, $user);

        if (empty($errors->errors)) {
            reset_password($user, $new_pass);
            $errors->add('password_reset', 'Your password has been reset successfully.');
        }
    }

    echo json_encode(array('loggedin' => true, 'messages' => $errors->get_error_messages()));

    wp_die();
}
add_action('wp_ajax_rmxreset', 'rmx_ng_reset_pass');
add_action('wp_ajax_nopriv_rmxreset', 'rmx_ng_reset_pass');

/**
* Intercept 404s 
*/
function rmx_intercept_404() {
    global $wp_query;
    $wp_query->is_404 = false; 
    $wp_query->is_archive = true;
}
add_action('template_redirect', 'rmx_intercept_404', 0);

/**
* Helper functions and api classes
*/ 
class RemaxFn {
    public static function image_src(
        $attachment_id, 
        $size = 'medium', 
        $default = ''
    ) {
        $att_data = wp_get_attachment_image_src($attachment_id, $size);
        if (false === $att_data) {
            return $default;
        }

        return $att_data[0];
    }
}

class RemaxApiHelpers {
    /**
    * Expand post data to match wp api post controller model 
    *
    * @param $the_post Object WP post object
    * @return expanded post object (content, title, excerpt, slug and featured_src)
    */
    public static function expand_post_item_data($the_post) {
        $the_post->date = date(DATE_RSS, strtotime($the_post->post_date));
        $the_post->content = array("rendered" => apply_filters('the_content', $the_post->post_content));
        $the_post->title = array("rendered" => $the_post->post_title);
        $the_post->type = $the_post->post_type;
        $the_post->excerpt = array(
            "rendered" => 
            apply_filters(
                'the_content', 
                empty($the_post->post_excerpt) ? wp_trim_words($the_post->post_content, 20, '...') : $the_post->post_excerpt
            )
        );
        if ($the_post->post_type === 'post') {
            $the_post->slug = $the_post->post_name; 

            // add author (int), author_nicename (str) and categories (int[])
            $the_post->author = (int)$the_post->post_author;
            unset($the_post->post_author);

            $the_post->author_nicename = get_the_author_meta('display_name', $the_post->author);

            $the_post->categories = wp_get_post_categories($the_post->ID);

        } else {
            $the_post->slug = str_replace('_', '-', $the_post->post_type) .'/'. $the_post->post_name;
        }

        $the_post->featured_src = RemaxFn::image_src((int)get_post_thumbnail_id($the_post->ID));

        return $the_post;
    }

    /**
    * Expand Document training with downloadabe document path 
    * post_type: 'trainings' / taxonomy: 'training-type' / term: 'document'
    * 
    * @param $posts Object WP post object array 
    * @return expanded post object (doc_url)
    */
    public static function expand_document_trainings($posts) {
        // meta_key: rt_training_doc_url
        $result = array_map(function($i) {
            $i->doc_url = get_post_meta($i->ID, 'rt_training_doc_url', true);
            return $i;
        }, $posts);

        // filter: document so doc_url must always be present and valid
        return array_filter($result, function($p) {
            return !empty($p->doc_url);
        });
    }

    /**
    * Expand Event training with time and place informations 
    * post_type: 'trainings' / taxonomy: 'training-type' / term: 'event'
    *
    * @param $posts Array of WP post objects 
    * @param $filter boolean
    * @return array of expanden WP post objects 
    */
    public static function expand_event_trainings($posts, $filter = true) {
        // meta keys (time related): rt_all_day_event, rt_start_date, rt_end_data, 
        // rt_start_hour, rt_start_min, rt_start_ampm, rt_end_hour, rt_end_min, rt_end_ampm
        $result = array_map(function($i) {
            $i->time = self::format_event_date_time($i->ID);
            return $i;
        }, $posts);
        
        // filter: events so event start or end date must be specified and in the future 
        if ($filter) {
            $result = array_filter($result, function($p) {
                return time() < strtotime($p->time->start) || time() < strtotime($p->time->end);
            });
        }

        // sort upcoming events by time start 
        usort($result, function($a, $b) {
            return strtotime($a->time->start) > strtotime($b->time->start);
        });

        return array_map(function($key, $val) {
            $val->place = self::format_event_place($val->ID);
            return $val;
        }, array_keys($result), $result);
    }

    /**
    * Retreive event date info 
    *
    * @param $post_id int 
    * @return $res Object {start: string, end: string}
    */
    public static function format_event_date_time($post_id) {
        $res = new stdClass();

        // if all day event checked 
            $res->all_day = (bool) get_post_meta($post_id, 'rt_all_day_event', true);
        // start date 
            $meta_start_date = get_post_meta($post_id, 'rt_start_date', true);
            // if start date not specified use current date 
            $start_date = !empty($meta_start_date) ? $meta_start_date : date_format(new DateTime(), 'Y-m-d');
        // end date 
            $end_date = get_post_meta($post_id, 'rt_end_date', true);


        //----- start date time property
            if (!$res->all_day) {
                $hour = get_post_meta($post_id, 'rt_start_hour', true);
                $min = get_post_meta($post_id, 'rt_start_min', true);
                $meridiem = get_post_meta($post_id, 'rt_start_ampm', true);

                $start_date = "{$start_date} {$hour}:{$min}:00 {$meridiem}";
            }
            $res->start = date_format(new DateTime($start_date), 'D, d M Y H:i:s');

        //----- end date time property
        if (!empty($end_date)) {
            if (!$res->all_day) {
                $hour = get_post_meta($post_id, 'rt_end_hour', true);
                $min = get_post_meta($post_id, 'rt_end_min', true);
                $meridiem = get_post_meta($post_id, 'rt_end_ampm', true);

                $end_date = "{$end_date} {$hour}:{$min}:00 {$meridiem}";               
            }
            $res->end = date_format(new DateTime($end_date), 'D, d M Y H:i:s');
        } else {
            $res->end = null;
        }

        return $res;

    }

    /**
    * Retreive event place info 
    *
    * @param $post_id int 
    * @return $res Object { venue: string, map: null | {long: float, lat: float, zoom: int} }
    */
    public static function format_event_place($post_id) {
        $res = new stdClass();
        // venue string 
            $res->venue = get_post_meta($post_id, 'rt_event_venue', true);
        // show map?
            $show_map = get_post_meta($post_id, 'rt_display_map', true) !== 'yes';
            if (!$show_map) {
                $res->map = null;
            } else {
                $res->map = new stdClass();
                $map_data = array(
                    'lat' => 40.7141667, 
                    'long' => -74.0063889, 
                    'zoom' => 10
                );
                foreach ($map_data as $map_field => $default_val) {
                    $val = floatval(get_post_meta($post_id, "rt_{$map_field}", true));
                    $$map_field = empty($val) ? $default_val : $val;
                }

                // assing values 
                $res->map->lng = $long;
                $res->map->lat = $lat; 
                $res->map->zoom = $zoom;
            }
        return $res;
    }

    /**
    * Format user info string to ease the query users <=> trainings 
    * ATTENDEE format
    *
    * @param $user object (WP_User object)
    * @return string __{$user->user_login}__{$user->ID}__ 
    */
    public static function attendee($user) {
        return '__{'. $user->user_login .'}__{'. $user->ID .'}__';
    }

    /**
    * Get all courses for specific agent (user subscriber)
    * 
    * @param $user object (WP_User object)
    * @return WP_Query object 
    */
    public static function courses_attended($user) {
        $user_query = self::attendee($user);

        return new WP_Query(array(
            'post_type' => 'trainings', 
            'posts_per_page' => -1, 
            'post_status' => 'publish', 
            // 'tax_query' => array(
            //     array(
            //         'taxonomy' => 'training-type', 
            //         'field' => 'slug', 
            //         'terms' => 'event'
            //     )
            // ), 
            'meta_query' => array(
                array(
                    'key' => 'rt_training_attendees', 
                    'value' => $user_query, 
                    'compare' => 'LIKE'
                )
            )
        ));
    }
}
 ?>