## Remax Intranet NG-Wp theme
[ ![Codeship Status for eutelnet/remax-ng-theme](https://codeship.com/projects/eb667fa0-2fb0-0134-992f-0637f2a9daca/status?branch=master)](https://codeship.com/projects/163982)

### Requirements 
* WP installed and running
* WP REST API v2 plugin activated

##### Theme Development Requirements 
* Node >=5.0 and NPM >= 3
* `ngm install --global webpack webpack-dev-server karma-cli protractor typings typescript rimraf`
* cd to ng folder and run `npm install`
* `$ npm run postinstall` # just for windows (install typings)

##### cli commands 
```
$ npm run build:dev
$ npm run watch # builds and watches dev
$ npm run start:hmr # build dev + hot module replacement

$ npm run build:prod # production build
```

#### must-use plugins 
Folder `mu-plugins` should be moved `root/wp-content` prior theme activation