<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title() ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <base href="/">
    <?php wp_head(); ?>
</head>
<body>
<?php //var_dump(get_userdata(1)); die(); ?>
    <app>
        <figure class="logo-loading" style="background-image: url(<?php echo get_template_directory_uri() .'/remax.png' ?>)"></figure>
    </app>
    
    <script async defer src="<?php echo get_template_directory_uri() .'/ng/dist/assets/quill.js' ?>"></script>
    <?php wp_footer(); ?>
</body>
</html>