<?php 
/**
* Get knowledge_base posts 
* grouped by knowledge_topic taxonomy 
* 
* @param array $data Options for the function 
* @return posts Array
*/
function remax_get_knowledge_base($data) {
    $posts = array();

    $knowledge_topics = get_terms('knowledge_topic', 'orderby=name');

    if (is_wp_error($knowledge_topics)) {
        return new WP_Error('not_found', 'Taxonomy "knowledge_topic" could not be retrieved', array('status' => 404));
    }

    foreach ($knowledge_topics as $topic) {
        $query_args = array(
            'post_type' => 'knowledge_base', 
            'posts_per_page' => -1, 
            'tax_query' => array(
                array(
                    'taxonomy' => 'knowledge_topic', 
                    'field' => 'term_id', 
                    'terms' => $topic->term_id
                )
            )
        );
        $posts_query = new WP_Query($query_args);
        if ($posts_query->have_posts()) {

            $topic_group = new stdClass();
            $topic_group->category_name = $topic->name;
            $topic_group->category_id = $topic->term_id;

            $topic_group->posts = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $posts_query->posts);

            array_push($posts, $topic_group);
        }
    }

    return $posts;
}
add_action('rest_api_init', 'add_rmx_knowledge_base_endpoint');

function add_rmx_knowledge_base_endpoint() {
    register_rest_route('remax/v1', '/knowledge-base', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_knowledge_base'
    ));
}