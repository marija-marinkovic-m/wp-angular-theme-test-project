<?php 
/**
* GET trainings posts 
* grouped by `training-type` 
* 
* @param array $data Options for the function 
* @return array $result (array of post objects)
*/
function remax_get_trainings($data) {
    // check if asked for events archive 
    $events_archive = isset($_GET['events-archive']);

    $result = array();

    if (!$events_archive) {
        // fetch all trainings 
        $training_types = get_terms('training-type');

        if (is_wp_error($training_types)) {
            return new WP_Error('not_found', 'Taxonomy "training-type" could not be retrieved', array('status' => 404));
        }

        foreach ($training_types as $type) {
            $posts_query = new WP_Query(array(
                'post_type' => 'trainings', 
                'posts_per_page' => -1, 
                'tax_query' => array(
                    array(
                        'taxonomy' => 'training-type', 
                        'field' => 'term_id', 
                        'terms' => $type->term_id
                    )
                )
            ));
            if ($posts_query->have_posts()) {
                $type_group = new stdClass();
                $type_group->category_name = $type->name; 
                $type_group->category_id = $type->term_id; 
                $all_posts = $posts_query->posts;

                /**
                * Here we need specific meta fields for specific training type
                *
                */
                switch ($type->slug) {
                    case 'document':
                        $all_posts = RemaxApiHelpers::expand_document_trainings($posts_query->posts);
                        break;
                    case 'event': 
                        $all_posts = RemaxApiHelpers::expand_event_trainings($posts_query->posts);
                        break;
                    default:
                        break;
                }
                $type_group->posts = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $all_posts);


                //array_push($result, $type_group);
                $result[$type->slug] = $type_group;
            } 
        }
    } else {
        // fetch only archive events trainings 
        $per_page = isset($_GET['per_page']) ? (int) $_GET['per_page'] : 2;
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $events_query = new WP_Query(array(
            'post_type' => 'trainings', 
            'posts_per_page' => $per_page, 
            'paged' => $page,
            'tax_query' => array(array(
                'taxonomy' => 'training-type', 
                'field' => 'slug', 
                'terms' => 'event'
            )), 
            'meta_query' => array(
                'relation' => 'AND', 
                array(
                    'key' => 'rt_start_date', 
                    'value' => date_format(new DateTime(), 'Y-m-d'), 
                    'type' => 'DATE', 
                    'compare' => '<'
                ), 
                array(
                    'relation' => 'OR', 
                    array(
                        'key' => 'rt_end_date', 
                        'value' => '', 
                        'compare' => '='
                    ), 
                    array(
                        'key' => 'rt_end_date', 
                        'value' => date_format(new DateTime(), 'Y-m-d'), 
                        'type' => 'DATE', 
                        'compare' => '<'
                    )
                )
            )
        ));
        if ($events_query->have_posts()) {
           $result = array_map(
               array('RemaxApiHelpers', 'expand_post_item_data'), 
               RemaxApiHelpers::expand_event_trainings($events_query->posts, false)
            );

            $response = new WP_REST_Response($result);
            $response->set_status(201);
            $response->header('X-WP-TotalPages', $events_query->max_num_pages);
            return $response;
        }
    }
    return $result;
}
add_action('rest_api_init', 'add_rmx_trainings_endpoint');

function add_rmx_trainings_endpoint() {
    register_rest_route('remax/v1', '/trainings', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_trainings'
    ));
}