<?php 
/**
* Check if post/page exists by given slug 
* NEW ENPOINT
* 
* @param array $data Options for the function. 
* @return boolean
*/
function remax_check_by_path($data) {
    $post_type = isset($_GET['type']) ? $_GET['type'] : 'page';
    $result = get_page_by_path($data['slug'], OBJECT, $post_type);

    $result = is_null($result) ? get_page_by_path($data['slug'], OBJECT, 'post') : $result;


    if (is_null($result)) {
        return new WP_Error('not_found', 'Invalid Slug', array('status' => 404));
    }
    return $result;
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/check/(?P<slug>[a-z0-9\-]+)', array(
        'methods' => 'GET', 
        'callback' => 'remax_check_by_path'
    ));
});

/**
* Get post of trainings post type with all corresponding fields (by tax -> training_type)
* NEW ENDPOINT 
*
* @param array $data Options for the function. 
* @return boolean 
*/
function remax_get_single_training($data) {
    $training = get_post($data['id']);

    if (is_null($training)) {
        return new WP_Error('not_found', 'This training doesn\'t exist', array('status' => 404));
    }

    $type = get_the_terms( $training->ID, 'training-type' );

    if (!$type || is_wp_error($type)) {
        return new WP_Error('not_found', 'This training has no type specified. Please specify type.', array('status' => 404));
    }

    // add standart api fields 
    $training = RemaxApiHelpers::expand_post_item_data($training);

    // expand fields for specific types 
    switch ($type[0]->slug) {
        case 'event':
            $expanded = array_shift(RemaxApiHelpers::expand_event_trainings(array($training), false));
            return !is_null($expanded) ? $expanded : new WP_Error('not_found', 'This event training is invalid. Please check the entry', array('status' => 404));
        case 'document': 
            return new WP_Error('not_allowed', 'Document trainings are unreachable by rule', array('status' => 404));
    }

    // all other types 
    return $training;
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/training/(?P<id>\d+)', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_single_training', 
        'args' => array(
            'id' => array(
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric($param);
                }
            )
        )
    ));
});

/**
* Add featured img source field to posts endpoint
* MODIFY RESPONSE
*
*/
function add_featured_src_to_post_response() {
    register_rest_field(array('post', 'page'), 
        'featured_src', 
        array(
            'get_callback' => 'fetch_featured_src', 
            'update_callback' => null, 
            'schema' => null
        )
    );
}
add_action('rest_api_init', 'add_featured_src_to_post_response');

/**
* Add author nicename to posts endpoint 
* MODIFY RESPONSE 
*/
function add_author_nicename_to_post_response() {
    register_rest_field(array('post'), 
        'author_nicename', 
        array(
            'get_callback' => 'fetch_author_nicename', 
            'update_callback' => null, 
            'schema' => null
        )
    );
}
add_action('rest_api_init', 'add_author_nicename_to_post_response');

/**
* Get the value of `featured_src` field 
*
* @param array $object Details of current post 
* @param string $field_name Name of the field. 
* @param WP_REST_Request $request Current request. 
*
* @return string 
*/
function fetch_featured_src($object, $field_name, $request) {
    return RemaxFn::image_src($object['featured_media']);
}

/**
* Get te value of authors display name 
*
* @param array $object Details of current post 
* @param string $field_name Name of the field. 
* @param WP_REST_Request $request Current request. 
*
* @return string 
*/
function fetch_author_nicename($object, $field_name, $request) {
    return get_the_author_meta('display_name', $object['author']);
}

include('marketing.php');
include('knowledge-base.php');
include('trainings.php');
include('dashboard-home-widgets.php');