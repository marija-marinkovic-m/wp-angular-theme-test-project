<?php 
/**
* Get Dashboard home widgets 
* Welcome (image + text)
* News (3 x company-news, 3 x marketing-news)
* Trainings (3 x event)
*
* @param array $data Options for the function 
* @return Object {
*               welcome: {img: string, desc: string}
*               news: [
*                   {
*                       category_name: string, 
*                       category_id: string, 
*                       posts: Post[]
*                   }
*               ], 
*               trainings: Post[]
*         }
*/
function remax_get_dashboard_home_data($data) {
    $response = new stdClass();

    // @TODO: implement this customisation
    // Welcome widget
    $response->welcome = array(
        "img" => home_url("/wp-content/themes/remax-ng-theme/ng/dist/assets/img/tim.png"), 
        "desc" => "<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules.</p><p>It va esser tam simplic quam Occidental in fact, it va esser Occidental. </p>"
    );

    // News Widget 
    $response->news = array();
    $news_terms = get_terms(array('taxonomy' => 'category'));

    if (is_array($news_terms)) {
        $first_two = array_slice($news_terms, 0, 2);
        foreach ($first_two as $term) {
            $news_query = new WP_Query(array(
                'post_type' => 'post', 
                'posts_per_page' => 2, 
                'tax_query' => array(array(
                    'taxonomy' => 'category', 
                    'field' => 'term_id', 
                    'terms' => $term->term_id
                ))
            ));
            if ($news_query->have_posts()) {
                $response->news[] = array(
                    'category_name' => $term->name, 
                    'category_id' => $term->slug, 
                    'posts' => array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $news_query->posts)
                );
            }
        }
    }

    // Trainings widget
    $response->trainings = array();
    $training_events_query = new WP_Query(array(
        'post_type' => 'trainings', 
        'posts_per_page' => 3, 
        'orderby' => 'none', 
        'tax_query' => array(array(
            'taxonomy' => 'training-type', 
            'field' => 'slug', 
            'terms' => 'event'
        )), 
        'meta_query' => array(
            'relation' => 'OR', 
            array(
                'key' => 'rt_start_date', 
                'value' => date_format(new DateTime(), 'Y-m-d'), 
                'type' => 'DATE', 
                'compare' => '>'
            ), 
            array(
                'relation' => 'OR', 
                array(
                    'key' => 'rt_end_date', 
                    'value' => '', 
                    'compare' => '='
                ), 
                array(
                    'key' => 'rt_end_date', 
                    'value' => date_format(new DateTime(), 'Y-m-d'), 
                    'type' => 'DATE', 
                    'compare' => '>'
                )
            )
        )
    ));
    if ($training_events_query->have_posts()) {
        $response->trainings = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), RemaxApiHelpers::expand_event_trainings($training_events_query->posts, false));
    }

    return $response;
}

add_action('rest_api_init', 'add_rmx_dashboard_home_endpoint');

function add_rmx_dashboard_home_endpoint() {
    register_rest_route('remax/v1', '/home', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_dashboard_home_data'
    ));
}