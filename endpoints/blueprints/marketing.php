<?php 
/**
* Get marketing posts
* grouped by marketing-guidelines => marketing-category 
*
* @param array $data Options for the function 
* @return posts 
*/
function remax_get_marketing($data) {
    $result = array();

    $guidelines_terms = get_terms('marketing-guidelines', 'orderby=name');
    $categories_terms = get_terms('marketing-category', 'orderby=count&order=DESC');

    if (is_wp_error($guidelines_terms)) {
        return new WP_Error('not_found', 'Taxonomy "marketing-guidelines" does not exist', array('status' => 404));
    }
    
    foreach ($guidelines_terms as $guideline) {

        // grab posts here via wp_query class 
        $res_args = array(
            'post_type' => 'marketing', 
            'posts_per_page' => -1, 
            'tax_query' => array(
                array(
                    'taxonomy' => 'marketing-guidelines', 
                    'field' => 'term_id', 
                    'terms' => $guideline->term_id
                )
            )
        );
        $resource = new WP_Query($res_args);

        if ($resource->have_posts()) {

            $guidelineObj = new stdClass();
            $guidelineObj->term = $guideline;

            $guidelineObj->groups = array();
           
            if (!is_wp_error($categories_terms)) {
                foreach ($categories_terms as $cat_term) {

                    $cat_posts = array();

                    foreach ($resource->posts as $the_post) {
                        
                        $the_terms = wp_get_post_terms($the_post->ID, 'marketing-category', array('fields' => 'ids'));

                        if (!is_wp_error($the_terms) && in_array($cat_term->term_id, $the_terms)) {
                            $the_post = RemaxApiHelpers::expand_post_item_data($the_post);

                            // add meta field 'rt_marketing_doc_url' to the bundle 
                            $the_post->doc_url = get_post_meta($the_post->ID, 'rt_marketing_doc_url', true);

                            // concat with other results 
                            array_push($cat_posts, $the_post);
                        }
                    }

                    if (!empty($cat_posts)) {
                        $postsObj = new stdClass();
                        $postsObj->category_name = $cat_term->name;
                        $postsObj->category_id = $cat_term->term_id;
                        $postsObj->posts = $cat_posts;
                        array_push($guidelineObj->groups, $postsObj);
                    }
                }
            }
            
            
            
            array_push($result, $guidelineObj);
        }        
    }

    return $result;
}
add_action('rest_api_init', 'add_rmx_marketing_endpoint');

function add_rmx_marketing_endpoint() {
    register_rest_route('remax/v1', '/marketing', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_marketing'
    ));
}