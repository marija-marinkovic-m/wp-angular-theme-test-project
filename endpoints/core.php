<?php 
/**
* remax/v1/check/(?P<slug>[a-z0-9\-]+) (remax_check_by_path) ----------------> 27
* remax/v1/training/(?P<id>\d+) (remax_get_single_training) -----------------> 53
* remax/v1/marketing (remax_get_marketing) ----------------------------------> 102
* remax/v1/knowledge-base (remax_get_knowledge_base) ------------------------> 189
* remax/v1/trainings (remax_get_trainings) ----------------------------------> 241
* remax/v1/home (remax_get_dashboard_home_data) -----------------------------> 373
* 
* wp/v2/post & wp/v2/page (modify response width featured_src) --------------> 477
* wp/v2/post (modify response with author_nicename) -------------------------> 495
*
* remax/v1/users/?P<slug>[a-z0-9\-]+) (remax get user data by slug) ---------> 524
* remax/v1/agent-rankings (remax_agent_rankings) ----------------------------> 589
* remax/v1/search?term={a-z0-9_} (remax_search_api) -------------------------> 709
*/



/**
* Check if post/page exists by given slug 
* NEW ENPOINT
* 
* @param array $data Options for the function. 
* @return WP_Error or WP_Post object
*/
function remax_check_by_path($data) {
    $post_type = isset($_GET['type']) ? $_GET['type'] : 'page';
    $result = get_page_by_path($data['slug'], OBJECT, $post_type);

    $result = is_null($result) ? get_page_by_path($data['slug'], OBJECT, 'post') : $result;


    if (is_null($result)) {
        return new WP_Error('not_found', 'Invalid Slug', array('status' => 404));
    }
    return $result;
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/check/(?P<slug>[a-z0-9\-]+)', array(
        'methods' => 'GET', 
        'callback' => 'remax_check_by_path'
    ));
});

/**
* Get post of trainings post type with all corresponding fields (by tax -> training_type)
* NEW ENDPOINT 
*
* @param array $data Options for the function. 
* @return boolean 
*/
function remax_get_single_training($data) {
    $training = get_post($data['id']);

    if (is_null($training)) {
        return new WP_Error('not_found', 'This training doesn\'t exist', array('status' => 404));
    }

    $type = get_the_terms( $training->ID, 'training-type' );

    if (!$type || is_wp_error($type)) {
        return new WP_Error('not_found', 'This training has no type specified. Please specify type.', array('status' => 404));
    }

    // add standart api fields 
    $training = RemaxApiHelpers::expand_post_item_data($training);

    // expand fields for specific types 
    switch ($type[0]->slug) {
        case 'event':
            $expanded = array_shift(RemaxApiHelpers::expand_event_trainings(array($training), false));
            return !is_null($expanded) ? $expanded : new WP_Error('not_found', 'This event training is invalid. Please check the entry', array('status' => 404));
        case 'document': 
            return new WP_Error('not_allowed', 'Document trainings are unreachable by rule', array('status' => 404));
    }

    // all other types 
    return $training;
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/training/(?P<id>\d+)', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_single_training', 
        'args' => array(
            'id' => array(
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric($param);
                }
            )
        )
    ));
});

/**
* Get marketing posts
* grouped by marketing-guidelines => marketing-category 
*
* @param array $data Options for the function 
* @return posts 
*/
function remax_get_marketing($data) {
    $result = array();

    $guidelines_terms = get_terms('marketing-guidelines', 'orderby=name');
    $categories_terms = get_terms('marketing-category', 'orderby=count&order=DESC');

    if (is_wp_error($guidelines_terms)) {
        return new WP_Error('not_found', 'Taxonomy "marketing-guidelines" does not exist', array('status' => 404));
    }
    
    foreach ($guidelines_terms as $guideline) {

        // grab posts here via wp_query class 
        $res_args = array(
            'post_type' => 'marketing', 
            'posts_per_page' => -1, 
            'tax_query' => array(
                array(
                    'taxonomy' => 'marketing-guidelines', 
                    'field' => 'term_id', 
                    'terms' => $guideline->term_id
                )
            )
        );
        $resource = new WP_Query($res_args);

        if ($resource->have_posts()) {

            $guidelineObj = new stdClass();
            $guidelineObj->term = $guideline;

            $guidelineObj->groups = array();
           
            if (!is_wp_error($categories_terms)) {
                foreach ($categories_terms as $cat_term) {

                    $cat_posts = array();

                    foreach ($resource->posts as $the_post) {
                        
                        $the_terms = wp_get_post_terms($the_post->ID, 'marketing-category', array('fields' => 'ids'));

                        if (!is_wp_error($the_terms) && in_array($cat_term->term_id, $the_terms)) {
                            $the_post = RemaxApiHelpers::expand_post_item_data($the_post);

                            // add meta field 'rt_marketing_doc_url' to the bundle 
                            $the_post->doc_url = get_post_meta($the_post->ID, 'rt_marketing_doc_url', true);

                            // concat with other results 
                            array_push($cat_posts, $the_post);
                        }
                    }

                    if (!empty($cat_posts)) {
                        $postsObj = new stdClass();
                        $postsObj->category_name = $cat_term->name;
                        $postsObj->category_id = $cat_term->term_id;
                        $postsObj->posts = $cat_posts;
                        array_push($guidelineObj->groups, $postsObj);
                    }
                }
            }
            
            
            
            array_push($result, $guidelineObj);
        }        
    }

    return $result;
}
add_action('rest_api_init', 'add_rmx_marketing_endpoint');

function add_rmx_marketing_endpoint() {
    register_rest_route('remax/v1', '/marketing', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_marketing'
    ));
}

/**
* Get knowledge_base posts 
* grouped by knowledge_topic taxonomy 
* 
* @param array $data Options for the function 
* @return posts Array
*/
function remax_get_knowledge_base($data) {
    $posts = array();

    $knowledge_topics = get_terms('knowledge_topic', 'orderby=name');

    if (is_wp_error($knowledge_topics)) {
        return new WP_Error('not_found', 'Taxonomy "knowledge_topic" could not be retrieved', array('status' => 404));
    }

    foreach ($knowledge_topics as $topic) {
        $query_args = array(
            'post_type' => 'knowledge_base', 
            'posts_per_page' => -1, 
            'tax_query' => array(
                array(
                    'taxonomy' => 'knowledge_topic', 
                    'field' => 'term_id', 
                    'terms' => $topic->term_id
                )
            )
        );
        $posts_query = new WP_Query($query_args);
        if ($posts_query->have_posts()) {

            $topic_group = new stdClass();
            $topic_group->category_name = $topic->name;
            $topic_group->category_id = $topic->term_id;

            $topic_group->posts = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $posts_query->posts);

            array_push($posts, $topic_group);
        }
    }

    return $posts;
}
add_action('rest_api_init', 'add_rmx_knowledge_base_endpoint');

function add_rmx_knowledge_base_endpoint() {
    register_rest_route('remax/v1', '/knowledge-base', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_knowledge_base'
    ));
}

/**
* GET trainings posts 
* grouped by `training-type` 
* 
* @param array $data Options for the function 
* @return array $result (array of post objects)
*/
function remax_get_trainings($data) {
    // check if asked for events archive 
    $events_archive = isset($_GET['events-archive']);

    $result = array();

    if (!$events_archive) {
        // fetch all trainings 
        $training_types = get_terms('training-type');

        if (is_wp_error($training_types)) {
            return new WP_Error('not_found', 'Taxonomy "training-type" could not be retrieved', array('status' => 404));
        }

        foreach ($training_types as $type) {
            $posts_query = new WP_Query(array(
                'post_type' => 'trainings', 
                'posts_per_page' => -1, 
                'tax_query' => array(
                    array(
                        'taxonomy' => 'training-type', 
                        'field' => 'term_id', 
                        'terms' => $type->term_id
                    )
                )
            ));
            if ($posts_query->have_posts()) {
                $type_group = new stdClass();
                $type_group->category_name = $type->name; 
                $type_group->category_id = $type->term_id; 
                $all_posts = $posts_query->posts;

                /**
                * Here we need specific meta fields for specific training type
                *
                */
                switch ($type->slug) {
                    case 'document':
                        $all_posts = RemaxApiHelpers::expand_document_trainings($posts_query->posts);
                        break;
                    case 'event': 
                        $all_posts = RemaxApiHelpers::expand_event_trainings($posts_query->posts);
                        break;
                    default:
                        break;
                }
                $type_group->posts = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $all_posts);


                //array_push($result, $type_group);
                $result[$type->slug] = $type_group;
            } 
        }
    } else {
        // fetch only archive events trainings 
        $per_page = isset($_GET['per_page']) ? (int) $_GET['per_page'] : 2;
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $events_query = new WP_Query(array(
            'post_type' => 'trainings', 
            'posts_per_page' => $per_page, 
            'paged' => $page,
            'tax_query' => array(array(
                'taxonomy' => 'training-type', 
                'field' => 'slug', 
                'terms' => 'event'
            )), 
            'meta_query' => array(
                'relation' => 'AND', 
                array(
                    'key' => 'rt_start_date', 
                    'value' => date_format(new DateTime(), 'Y-m-d'), 
                    'type' => 'DATE', 
                    'compare' => '<'
                ), 
                array(
                    'relation' => 'OR', 
                    array(
                        'key' => 'rt_end_date', 
                        'value' => '', 
                        'compare' => '='
                    ), 
                    array(
                        'key' => 'rt_end_date', 
                        'value' => date_format(new DateTime(), 'Y-m-d'), 
                        'type' => 'DATE', 
                        'compare' => '<'
                    )
                )
            )
        ));
        if ($events_query->have_posts()) {
           $result = array_map(
               array('RemaxApiHelpers', 'expand_post_item_data'), 
               RemaxApiHelpers::expand_event_trainings($events_query->posts, false)
            );

            $response = new WP_REST_Response($result);
            $response->set_status(201);
            $response->header('X-WP-TotalPages', $events_query->max_num_pages);
            return $response;
        }
    }
    return $result;
}
add_action('rest_api_init', 'add_rmx_trainings_endpoint');

function add_rmx_trainings_endpoint() {
    register_rest_route('remax/v1', '/trainings', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_trainings'
    ));
}

/**
* Get Dashboard home widgets 
* Welcome (image + text)
* News (3 x company-news, 3 x marketing-news)
* Trainings (3 x event)
*
* @param array $data Options for the function 
* @return Object {
*               welcome: {img: string, desc: string}
*               news: [
*                   {
*                       category_name: string, 
*                       category_id: string, 
*                       posts: Post[]
*                   }
*               ], 
*               trainings: Post[]
*         }
*/
function remax_get_dashboard_home_data($data) {
    $response = new stdClass();

    // @TODO: implement this customisation
    // Welcome widget
    $page_on_front = get_option('page_on_front');
    if ($page_on_front) {
        $homepage = get_post($page_on_front);
        $home_thumb = wp_get_attachment_url(get_post_thumbnail_id($page_on_front));
        $response->welcome = array(
            "img" => $home_thumb, 
            "desc" => apply_filters('the_content', $homepage->post_content)
        );
    } else {
        $response->welcome = array(
            "img" => home_url("/wp-content/themes/remax-ng-theme/ng/dist/assets/img/tim.png"), 
            "desc" => "<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules.</p><p>It va esser tam simplic quam Occidental in fact, it va esser Occidental. </p>"
        );
    }

    // News Widget 
    $response->news = array();
    $news_terms = get_terms(array('taxonomy' => 'category'));

    if (is_array($news_terms)) {
        $first_two = array_slice($news_terms, 0, 2);
        foreach ($first_two as $term) {
            $news_query = new WP_Query(array(
                'post_type' => 'post', 
                'posts_per_page' => 2, 
                'tax_query' => array(array(
                    'taxonomy' => 'category', 
                    'field' => 'term_id', 
                    'terms' => $term->term_id
                ))
            ));
            if ($news_query->have_posts()) {
                $response->news[] = array(
                    'category_name' => $term->name, 
                    'category_id' => $term->slug, 
                    'posts' => array_map(array('RemaxApiHelpers', 'expand_post_item_data'), $news_query->posts)
                );
            }
        }
    }

    // Trainings widget
    $response->trainings = array();
    $training_events_query = new WP_Query(array(
        'post_type' => 'trainings', 
        'posts_per_page' => 3, 
        'orderby' => 'none', 
        'tax_query' => array(array(
            'taxonomy' => 'training-type', 
            'field' => 'slug', 
            'terms' => 'event'
        )), 
        'meta_query' => array(
            'relation' => 'OR', 
            array(
                'key' => 'rt_start_date', 
                'value' => date_format(new DateTime(), 'Y-m-d'), 
                'type' => 'DATE', 
                'compare' => '>'
            ), 
            array(
                'relation' => 'OR', 
                array(
                    'key' => 'rt_end_date', 
                    'value' => '', 
                    'compare' => '='
                ), 
                array(
                    'key' => 'rt_end_date', 
                    'value' => date_format(new DateTime(), 'Y-m-d'), 
                    'type' => 'DATE', 
                    'compare' => '>'
                )
            )
        )
    ));
    if ($training_events_query->have_posts()) {
        $response->trainings = array_map(array('RemaxApiHelpers', 'expand_post_item_data'), RemaxApiHelpers::expand_event_trainings($training_events_query->posts, false));
    }

    return $response;
}

add_action('rest_api_init', 'add_rmx_dashboard_home_endpoint');

function add_rmx_dashboard_home_endpoint() {
    register_rest_route('remax/v1', '/home', array(
        'methods' => 'GET', 
        'callback' => 'remax_get_dashboard_home_data'
    ));
}

/**
* Get the value of `featured_src` field 
*
* @param array $object Details of current post 
* @param string $field_name Name of the field. 
* @param WP_REST_Request $request Current request. 
*
* @return string 
*/
function fetch_featured_src($object, $field_name, $request) {
    return RemaxFn::image_src($object['featured_media']);
}
/**
* Add featured img source field to posts endpoint
* MODIFY RESPONSE
*
*/
function add_featured_src_to_post_response() {
    register_rest_field(array('post', 'page'), 
        'featured_src', 
        array(
            'get_callback' => 'fetch_featured_src', 
            'update_callback' => null, 
            'schema' => null
        )
    );
}
add_action('rest_api_init', 'add_featured_src_to_post_response');


/**
* Get te value of authors display name 
*
* @param array $object Details of current post 
* @param string $field_name Name of the field. 
* @param WP_REST_Request $request Current request. 
*
* @return string 
*/
function fetch_author_nicename($object, $field_name, $request) {
    return get_the_author_meta('display_name', $object['author']);
}
/**
* Add author nicename to posts endpoint 
* MODIFY RESPONSE 
*/
function add_author_nicename_to_post_response() {
    register_rest_field(array('post'), 
        'author_nicename', 
        array(
            'get_callback' => 'fetch_author_nicename', 
            'update_callback' => null, 
            'schema' => null
        )
    );
}
add_action('rest_api_init', 'add_author_nicename_to_post_response');

/**
* Get the user's data (agent data object) by slug (user_nicename) as param 
* NEW ENDPOINT 
*
* @param array $data Options for the function. 
* @return WP_Error or User object
*/
function remax_user_profile($data) {

    // first check if logged in user shoud be returned 
    if ($data['slug'] === 'me') {
        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $the_user = new stdClass();
            $the_user = $current_user->data;
            $the_user->roles = array();
            if (current_user_can('edit-posts')) {
                $the_user->roles = array('administrator');
            }
            return $the_user;
        }
        return new WP_Error('not_allowed', 'You are not logged in. Please log in.', array('status' => 404));
    }

    // username_exists returns user ID of false
    $user_id = username_exists($data['slug']);

    if (false === $user_id) {
        return new WP_Error('not_found', 'Invalid Username', array('status' => 404));
    }

    $the_user = get_user_by('ID', $user_id);

    if (false === $the_user) {
        return new WP_Error('not_found', 'User could not be retreived', array('status' => 404));
    }

    if (!isset($_POST['action'])) {
        $user_contact_data = array_map(function($item) {
            return explode("\r\n", $item);
        }, (array)get_user_meta($the_user->ID, 'rmx_contact', true));

        $courses = null;
        $courses_attended = RemaxApiHelpers::courses_attended($the_user);
        if ($courses_attended->have_posts()) {
            $courses = array_map(function($course) {
                if (has_term('document', 'training-type', $course)) {
                    $course = array_shift(RemaxApiHelpers::expand_document_trainings(array($course)));
                }
                if (has_term('event', 'training-type', $course)) {
                    $course = array_shift(RemaxApiHelpers::expand_event_trainings(array($course), false));
                }
                return RemaxApiHelpers::expand_post_item_data($course);
            }, $courses_attended->posts);
        }

        $response_obj = array(
            "id" => $the_user->ID, 
            "name" => $the_user->display_name, 
            "slug" => $the_user->user_nicename, 
            "avatar_urls" => array(
                "24" => get_avatar_url($the_user->ID, array('size' => 24)), 
                "48" => get_avatar_url($the_user->ID, array('size' => 48)), 
                "96" => get_avatar_url($the_user->ID, array('size' => 96))
            ), 
            "office" => get_user_meta($the_user->ID, 'rmx_office', true), 
            "bio" => apply_filters('the_content', get_user_meta($the_user->ID, 'rmx_basic_info', true)), 
            "contact" => $user_contact_data, 
            "social" => get_user_meta($the_user->ID, 'rmx_social', true), 
            "courses" => $courses, 
            "email" => $the_user->user_email
        );


        return $response_obj;


    } else {
        // $the_user 
        // check editor_id
        $editor_id = (int)$_POST['editor_id'];
        $the_editor = get_userdata($editor_id);

        if ($the_editor === false) {
            return new WP_Error('not_found', 'You do not exist.', array('status' => 404));
        }

        if ( $editor_id !== $the_user->ID && !in_array('administrator', $the_editor->roles) ) {
            return new WP_Error('not_allowed', 'You are not allowed to change this user\'s data', array('status' => 403));
        } 

        $post_data = $_POST;

        $user_data = array(
            'ID' => $the_user->ID, 
            'user_email' => @$post_data['email'], 
            'display_name' => @$post_data['displayName'], 
            'user_login' => $the_user->user_login
        );
        $user_meta = array(
            'rmx_basic_info' => @$post_data['bio'], 
            'rmx_contact' => array(
                'phone' => @$post_data['publicPhone'], 
                'email' => @$post_data['publicEmail']
            ), 
            'rmx_social' => array(
                'facebook' => @$post_data['facebook'], 
                'twitter' => @$post_data['twitter'], 
                'linkedin' => @$post_data['linkedin']
            )
        );
        
        $update_user_data = wp_insert_user($user_data);

        if (is_wp_error($update_user_data)) {
            return $update_user_data;
        }

        foreach ($user_meta as $meta_key => $meta_value) {
            update_user_meta($the_user->ID, $meta_key, $meta_value);
        }

        return array('msg' => 'Success. Changes are saved. Redirecting...');
    }
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/users/(?P<slug>[a-z0-9\-_\.]+)', array(
        'methods' => 'GET,POST', 
        'callback' => 'remax_user_profile'
    ));
});

/**
* Get agent rankings lists (ordered by award(1lvl) & props(2lvl))
* NEW ENDPOINT 
* ie remax/v1/agent-rankings?type={ 'sales' | 'letting' }&per_page=-1
*
* @param array $data Options for the function.
* @return WP_Error or list of agent stats objects 
*/
function remax_agent_rankings($data) {
    global $wpdb; 

    $allowed_types = array('sales', 'letting');
    $current_type = in_array(@$_GET['type'], $allowed_types) ? $_GET['type'] : 'sales'; 

    $query_str = "SELECT 
        users.display_name, 
        users.user_nicename, 
        meta.meta_value as stats, 
        meta_office.meta_value as office
    FROM 
        {$wpdb->users} as users 
        LEFT JOIN {$wpdb->usermeta} as meta ON users.ID = meta.user_id 
        LEFT JOIN {$wpdb->usermeta} as meta_office ON users.ID = meta_office.user_id 
    WHERE 
        meta.meta_key = 'rmx_ranking_{$current_type}' 
        AND meta_office.meta_key = 'rmx_office'
    ";

    $result = array_map(function($item) {
        $stats = unserialize($item->stats);
        $res = array(
            "name" => $item->display_name, 
            "slug" => $item->user_nicename, 
            "award" => (int)@$stats['award'], 
            "props" => (int)@$stats['props'], 
            "bonus" => (int)@$stats['bonus'], 
            "office" => $item->office
        );
        return $res;
    }, $wpdb->get_results($query_str));

    usort($result, function($a, $b) {
        if ($a['award'] != $b['award']) {
            return strcmp($a['award'], $b['award']);
        }
        return (int)$b['props'] - (int)$a['props'];
    });

    return $result;
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/agent-rankings', array(
        'methods' => 'GET', 
        'callback' => 'remax_agent_rankings'
    ));
});

/**
* Search RemaxIntranet database 
* NEW ENDPOINT 
* remax/v1/search?term={}
* 
* @param array $data Options for the function
* @return WP_Error or list of post/page/cpt/user objects: Result
* Result {title: string; slug: string;}
*/
function remax_search_api () {
    if ( !isset($_GET['term']) || empty($_GET['term']) ) {
        return new WP_Error('empty_search', 'Search term empty', array('status', 404));
    }
    $term = $_GET['term'];
    $search_query = new WP_Query(array(
        'post_type' => array('post', 'page', 'knowledge_base', 'trainings'), 
        's' => $term, 
        'exact' => false, 
        'sentance' => true
    ));
    $marketing_search = new WP_Query(array(
        'post_type' => 'marketing', 
        's' => $term, 
        'exact' => false, 
        'sentance' => true
    ));
    if ( $search_query->have_posts() || $marketing_search->have_posts() ) {

        $response = array('data' => array_map(function($i) {
                $postType = str_replace('_', '-', $i->post_type);
                $post_slug = in_array($postType, array('post', 'page')) ? "/{$i->post_name}" : "/{$postType}/{$i->post_name}";

                switch ($postType) {
                    case 'trainings':
                        $icon = 'fa fa-graduation-cap';
                        break;
                    case 'knowledge-base': 
                        $icon = 'fa fa-lightbulb-o';
                        break;
                    case 'post': 
                        $icon = 'fa fa-newspaper-o';
                        break;
                    default:
                        $icon = 'fa fa-file-text-o';
                        break;
                }

                $r = new stdClass();
                
                $r->slug = $post_slug;
                $r->title = $i->post_title;
                $r->desc = strip_tags(empty($i->post_excerpt) ? wp_trim_words($i->post_content, 10, '...') : $i->post_excerpt);
                $r->icon = $icon;

                return $r;
            }, $search_query->posts)
        );

        if ($marketing_search->have_posts()) {
            $mr = new stdClass();
            $mr->title = 'Marketing resources';
            $mr->slug = '/marketing';
            $mr->desc = implode(', ', array_map(function($m) {return $m->post_title;}, $marketing_search->posts));     
            $mr->icon = 'fa fa-bullseye';

            array_push($response['data'], $mr); 
        }

        return $response;
    }
    return array(
        'data' => array()
    );
}
add_action('rest_api_init', function() {
    register_rest_route('remax/v1', '/search', array(
        'methods' => 'GET', 
        'callback' => 'remax_search_api'
    ));
});
