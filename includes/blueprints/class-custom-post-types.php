<?php 
class Remax_Custom_Post_Types {

    public static $instance = null;

    public static function Instance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        add_action('init', array($this, 'register_post_types'));
        add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts'));
        add_action('save_post', array($this, 'save_meta'), 10, 2);
    }

    public function register_post_types() {
        $this->register_marketing();
        $this->register_knowledge_base();
        $this->register_training();
    }

    public function load_admin_scripts() {
        // google map
        wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyD1K4V0pqu0PEyVwZzhAnPP5GhGz_7tVzw');

        wp_enqueue_style('cupertion-ui-datepicker-style', get_template_directory_uri() .'/includes/assets/css/jquery-ui.min.css');
        wp_enqueue_style('custom_admin_styles', get_template_directory_uri() .'/includes/assets/css/main.css', false, '1.0.0'); 
        wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );
        wp_enqueue_script('custom_admin_scripts', get_template_directory_uri() .'/includes/assets/js/init.js');
    }

    public function register_marketing() {
        // marketing post type 
        $args = array(
            'labels' => array(
                'name' => 'Marketing', 
                'menu_name' => 'Marketing'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-megaphone', 
            'supports' => array('title', 'excerpt', 'thumbnail'), 
            'register_meta_box_cb' => array($this, 'marketing_meta'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'marketing'), 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type('marketing', $args);

        // guidelines taxonomy 
        $guidelines_tax_args = array(
            'labels' => array(
                'name' => 'Guidelines', 
                'menu_name' => 'Guidelines'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('marketing-guidelines', 'marketing', $guidelines_tax_args);

        // other marketing categories
        $categories_args = array(
            'labels' => array(
                'name' => 'Categories', 
                'menu_name' => 'Categories'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('marketing-category', 'marketing', $categories_args);
    }

    public function register_knowledge_base() {
        // knowledge_base post type 
        $args = array(
            'labels' => array(
                'name' => 'Knowledge base', 
                'menu_name' => 'Knowledge base'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-book-alt', 
            'supports' => array('title', 'editor'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'knowledge-base'), 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type('knowledge_base', $args);

        // topic taxonomy
        $topic_args = array(
            'labels' => array(
                'name' => 'Topics', 
                'menu_name' => 'Topics'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('knowledge_topic', 'knowledge_base', $topic_args);
    }

    public function register_training() {
        // training post type 
        $args = array(
            'labels' => array(
                'name' => 'Trainings', 
                'menu_name' => 'Trainings'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-welcome-learn-more', 
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'author'), 
            'register_meta_box_cb' => array($this, 'training_meta'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'trainings')
        );
        register_post_type('trainings', $args);

        // training type 
        register_taxonomy('training-type', 'trainings', array(
            'labels' => array('name' => 'Training type'), 
            'hierarchical' => false, 
            'meta_box_cb' => array($this, 'training_tax')
        ));
    }

    public function marketing_meta() {
        add_meta_box(
            'marketing-document', 
            'Asset path', 
            function() {
                wp_nonce_field( 'remax_custom_nonce_action', 'remax_custom_nonce' );
                include_once 'views/meta-marketing-document.php';
            }, 
            'marketing', 'normal', 'high'
        );
    }

    public function training_meta() {
        // EVENT type
        add_meta_box(
            'training-event', 
            'Event info', 
            function() {
                wp_nonce_field( 'remax_custom_nonce_action', 'remax_custom_nonce' );
                include_once 'views/meta-training-event.php';
            }, 
            'trainings', 'normal', 'high'
        );
        // DOCUMENT type 
        add_meta_box(
            'training-document', 
            'Document info', 
            function() {
                include_once 'views/meta-training-document.php';
            }, 
            'trainings', 'normal', 'high'
        );
    }
    public function training_tax() {
        include 'views/meta-training-type.php';
    }


    /**
    * Save post hook for saving meta 
    */
    public function save_meta($post_id, $post) {
        $nonce_name   = isset( $_POST['remax_custom_nonce'] ) ? $_POST['remax_custom_nonce'] : '';
        $nonce_action = 'remax_custom_nonce_action';
 
        if ( ! isset( $nonce_name ) ) {return;}
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {return;}
        if ( ! current_user_can( 'edit_post', $post_id ) ) {return;}
        if ( wp_is_post_autosave( $post_id ) ) {return;}
        if ( wp_is_post_revision( $post_id ) ) {return;}

        foreach ($_POST as $field => $value) {
            if (strpos($field, 'rt_') === 0) {
                update_post_meta($post_id, $field, trim($value));
            }
        }

        // Checkboxes
        $checks = array('rt_display_map', 'rt_all_day_event');
        foreach ($checks as $box) {
            if (!isset($_POST[$box])) {
                delete_post_meta($post_id, $box);
            }
        }
    }

}

Remax_Custom_Post_Types::Instance();