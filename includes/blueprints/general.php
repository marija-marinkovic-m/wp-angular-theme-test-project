<?php 
class RemaxHelperFn {

    public static $_instance = null;

    public static function Instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {

    }

    public function padTwoDigits($num) {
        $new_num = "00". $num;
        return substr($new_num, strlen($num), 2);
    }

    public function selectBuilder($values, $name, $curr) {
        $options = '';
        foreach ($values as $key => $val) {
            $options .= sprintf(
                '<option value="%s" %s>%s</option>', 
                $val, selected($val, $curr, false), $val
            );
        }
        return sprintf('<select name="%s">%s</select>', $name, $options);
    }

    public function getHours() {
        $hours = array();
        for ($i = 1; $i<=12; $i++) {
            $hours[] = $this->padTwoDigits($i);
        }
        return $hours;
    }

    public function getMinutes() {
        $minutes = array();
        for ($i = 0; $i<60; $i+=5) {
            $minutes[] = $this->padTwoDigits($i);
        }
        return $minutes;
    }

    public function getTimeUnits() {
        $time = new stdClass();
        $time->hours = $this->getHours();
        $time->minutes = $this->getMinutes();
        $time->period = array('am', 'pm');

        return $time;
    }

}

function RMX() {
    return RemaxHelperFn::Instance();
}

RMX();