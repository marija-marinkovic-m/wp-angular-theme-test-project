<?php 

/**
* Remax_Custom_Post_Types (class definition) -----------------------------------> 18
* RemaxTheme "Frontend helpers" (class definition) -----------------------------> 231
* RemaxHelperFn "Backend helpers" (class definition) ---------------------------> 293
* remax_config "app config data export function" (function definition) ---------> 359
*
* Remax_Custom_Post_Types (class instantiation) --------------------------------> 362
* RemaxHelperFn (class instantiation) ------------------------------------------> 363
*/

/**
* Custom Post types and appropriate meta fields and hooks 
* including marketing, knowledge_base and training CPT
*
*/
class Remax_Custom_Post_Types {

    public static $instance = null;

    public static function Instance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        add_action('init', array($this, 'register_post_types'));
        add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts'));
        add_action('save_post', array($this, 'save_meta'), 10, 2);
    }

    public function register_post_types() {
        $this->register_marketing();
        $this->register_knowledge_base();
        $this->register_training();
    }

    public function load_admin_scripts() {
        // google map
        wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyD1K4V0pqu0PEyVwZzhAnPP5GhGz_7tVzw');

        wp_enqueue_style('cupertion-ui-datepicker-style', get_template_directory_uri() .'/includes/assets/css/jquery-ui.min.css');
        wp_enqueue_style('custom_admin_styles', get_template_directory_uri() .'/includes/assets/css/main.css', false, '1.0.0'); 
        wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );
        wp_enqueue_script('custom_admin_scripts', get_template_directory_uri() .'/includes/assets/js/init.js');
    }

    public function register_marketing() {
        // marketing post type 
        $args = array(
            'labels' => array(
                'name' => 'Marketing', 
                'menu_name' => 'Marketing'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-megaphone', 
            'supports' => array('title', 'excerpt', 'thumbnail'), 
            'register_meta_box_cb' => array($this, 'marketing_meta'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'marketing'), 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type('marketing', $args);

        // guidelines taxonomy 
        $guidelines_tax_args = array(
            'labels' => array(
                'name' => 'Guidelines', 
                'menu_name' => 'Guidelines'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('marketing-guidelines', 'marketing', $guidelines_tax_args);

        // other marketing categories
        $categories_args = array(
            'labels' => array(
                'name' => 'Categories', 
                'menu_name' => 'Categories'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('marketing-category', 'marketing', $categories_args);
    }

    public function register_knowledge_base() {
        // knowledge_base post type 
        $args = array(
            'labels' => array(
                'name' => 'Knowledge base', 
                'menu_name' => 'Knowledge base'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-book-alt', 
            'supports' => array('title', 'editor'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'knowledge-base'), 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Posts_Controller'
        );
        register_post_type('knowledge_base', $args);

        // topic taxonomy
        $topic_args = array(
            'labels' => array(
                'name' => 'Topics', 
                'menu_name' => 'Topics'
            ), 
            'hierarchical' => true, 
            'show_in_rest' => true, 
            'rest_controller_class' => 'WP_REST_Terms_Controller'
        );
        register_taxonomy('knowledge_topic', 'knowledge_base', $topic_args);
    }

    public function register_training() {
        // training post type 
        $args = array(
            'labels' => array(
                'name' => 'Trainings', 
                'menu_name' => 'Trainings'
            ), 
            'public' => true, 
            'menu_icon' => 'dashicons-welcome-learn-more', 
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'author'), 
            'register_meta_box_cb' => array($this, 'training_meta'), 
            'has_archive' => true, 
            'rewrite' => array('slug' => 'trainings')
        );
        register_post_type('trainings', $args);

        // training type 
        register_taxonomy('training-type', 'trainings', array(
            'labels' => array('name' => 'Training type'), 
            'hierarchical' => false, 
            'meta_box_cb' => array($this, 'training_tax')
        ));
    }

    public function marketing_meta() {
        add_meta_box(
            'marketing-document', 
            'Asset path', 
            function() {
                wp_nonce_field( 'remax_custom_nonce_action', 'remax_custom_nonce' );
                include_once 'views/meta-marketing-document.php';
            }, 
            'marketing', 'normal', 'high'
        );
    }

    public function training_meta() {
        // EVENT type
        add_meta_box(
            'training-event', 
            'Event info', 
            function() {
                wp_nonce_field( 'remax_custom_nonce_action', 'remax_custom_nonce' );
                include_once 'views/meta-training-event.php';
            }, 
            'trainings', 'normal', 'high'
        );
        // DOCUMENT type 
        add_meta_box(
            'training-document', 
            'Document info', 
            function() {
                include_once 'views/meta-training-document.php';
            }, 
            'trainings', 'normal', 'high'
        );

        // AGENT <=> Courses (training events) attended data 
        add_meta_box(
            'training-agents', 
            'Agents attendees', 
            function() {
                include_once 'views/meta-training-attendees.php';
            }, 
            'trainings', 'side', 'default'
        );
    }
    public function training_tax() {
        include 'views/meta-training-type.php';
    }


    /**
    * Save post hook for saving meta 
    */
    public function save_meta($post_id, $post) {

        $nonce_name   = isset( $_POST['remax_custom_nonce'] ) ? $_POST['remax_custom_nonce'] : '';
        $nonce_action = 'remax_custom_nonce_action';
 
        if ( ! isset( $nonce_name ) ) {return;}
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {return;}
        if ( ! current_user_can( 'edit_post', $post_id ) ) {return;}
        if ( wp_is_post_autosave( $post_id ) ) {return;}
        if ( wp_is_post_revision( $post_id ) ) {return;}

        foreach ($_POST as $field => $value) {
            if (strpos($field, 'rt_') === 0) {
                update_post_meta($post_id, $field, $value);
            }
        }

        // Checkboxes
        $checks = array('rt_display_map', 'rt_all_day_event');
        foreach ($checks as $box) {
            if (!isset($_POST[$box])) {
                delete_post_meta($post_id, $box);
            }
        }
    }

}

/**
* Frontend Helper Functions
* RemaxTheme Class for fetching menus and categories
*/
class RemaxTheme {
    /**
    * Register and return items for main menu 
    * @return array of menu items or empty array
    */
    public static function main_menu() {
        $menu_items = wp_get_nav_menu_items('main-menu');
        return self::fetch_menu_data($menu_items);
    }

    /**
    * Get list of usefull links 
    */
    public static function useful_links_menu() {
        $usefull_menu_items = wp_get_nav_menu_items('useful-links');
        return self::fetch_menu_data($usefull_menu_items);
    }

    /**
    * Return all post categories 
    * @TODO: see if needed 
    */
    public static function categories() {
        return get_categories();
    }

    /**
    * Fetch menu data 
    * @param $menu_items => array of menu items 
    * @return array 
    */
    private static function fetch_menu_data($menu_items) {
        if (!is_array($menu_items)) {
            return false;
        } 
        return array_map(function($menu_item) {

            $menu_type = $menu_item->type_label; 
            if ($menu_type == "Post" || $menu_type == "Page") {
                $item_slug = get_post($menu_item->object_id)->post_name; 
            } else if ($menu_type == "Category") {
                $item_slug = get_category($menu_item->object_id)->slug;
            }

            return array(
                'item_id' => $menu_item->ID, 
                'item_parent' => $menu_item->menu_item_parent, 
                'title' => $menu_item->title, 
                'slug' => @$item_slug, 
                'link_type' => $menu_type,
                'url' => $menu_item->url, 
                'class' => implode(' ', $menu_item->classes)
            );
        }, $menu_items);
    }
}

/**
* Backend Helper Functions
* for building meta boxes data
* including padTwoDigits, selectBuilder, getHours, getMinutes,...
*/
class RemaxHelperFn {

    public static $_instance = null;

    public static function Instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {

    }

    public function padTwoDigits($num) {
        $new_num = "00". $num;
        return substr($new_num, strlen($num), 2);
    }

    public function selectBuilder($values, $name, $curr) {
        $options = '';
        foreach ($values as $key => $val) {
            $options .= sprintf(
                '<option value="%s" %s>%s</option>', 
                $val, selected($val, $curr, false), $val
            );
        }
        return sprintf('<select name="%s">%s</select>', $name, $options);
    }

    public function getHours() {
        $hours = array();
        for ($i = 1; $i<=12; $i++) {
            $hours[] = $this->padTwoDigits($i);
        }
        return $hours;
    }

    public function getMinutes() {
        $minutes = array();
        for ($i = 0; $i<60; $i+=5) {
            $minutes[] = $this->padTwoDigits($i);
        }
        return $minutes;
    }

    public function getTimeUnits() {
        $time = new stdClass();
        $time->hours = $this->getHours();
        $time->minutes = $this->getMinutes();
        $time->period = array('am', 'pm');

        return $time;
    }

}

function RMX() {
    return RemaxHelperFn::Instance();
}

/**
* Add Remax Theme Configration
* It will be localized in main script 
*/
function remax_config() {
    $config = array(
        'template_directory' => get_template_directory_uri() .'/ng/dist/', 
        'site_url' => get_option('siteurl'), 
        'site_title' => get_option('blogname'), 
        'site_description' => get_option('blogdescription'), 
        'home_id' => get_option('page_on_front'), 
        'blog_id' => get_option('page_for_posts'), 
        'admin_email' => get_option('admin_email'), 
        'menu' => RemaxTheme::main_menu(), 
        'links' => RemaxTheme::useful_links_menu(), 
        'categories' => RemaxTheme::categories(), 
        'tags' => get_tags(), 
        'authors' => get_users(array('who' => 'authors', 'fields' => array('ID', 'user_nicename', 'display_name'), 'number' => -1)), 
        'api_root' => esc_url_raw( rest_url() ), 
        'nonce' => wp_create_nonce('wp_rest'), 
        'ajax_url' => admin_url('admin-ajax.php'), 
        'posts_per_page' => get_option('posts_per_page'), 
        'awards' => json_decode(AGENT_AWARDS)
    );
    wp_localize_script('main', 'app_config', $config);
}

Remax_Custom_Post_Types::Instance();
RemaxHelperFn::Instance();