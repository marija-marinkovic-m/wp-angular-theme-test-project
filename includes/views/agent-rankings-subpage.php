<h2>Agent Rankings Files</h2>
<div id="agent-rankings-subpage" class="wrap">

    <div id="poststuff">

        <div id="post-body" class="metabox-holder columns-2">

        <!-- main content -->
        <div id="post-body-content">

            <h1>Upload</h1>
            <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                    <h2 class="hndle"><span>Select the spreadsheet file (.xls or .xlsx) to upload:</span>
                    </h2>

                    <div class="inside">
                        <!-- tab content goes here -->
                        <form method="POST" enctype="multipart/form-data" style="margin: 20px 0 40px">
                            <input type="file" accept=".xls,.xlsx" name="ratings_file" class="large-text" style="width: auto"> <input type="submit" class="button-secondary" value="Upload">
                            <?php wp_nonce_field('rmx_admin_upload_action', 'rmx_admin_upload_box') ?>
                        </form>

                        <div class="notice notice-warning inline"><p><strong>Note:</strong> Before you upload new spreadsheet it is advisable to create backup of current ranking states (see <mark>Download from database</mark> section).</p></div>
                    </div>
                </div>
            </div><!-- UPLOAD -->

            <h1>Download from database</h1>
            <div class="meta-box-sortables ui-sortable">
                <div class="postbox">
                    <h2 class="hndle"><span>Get existing data, generate and download spreadsheet: </span>
                    </h2>

                    <div class="inside">
                        <!-- tab content goes here -->
                        <form method="POST" style="margin: 20px 0 40px">
                            <input class="button-secondary" type="submit" value="Download Agent Rankings" />

                            <?php wp_nonce_field('rmx_admin_download_action', 'rmx_admin_download_box') ?>
                        </form>

                        <div class="notice notice-info inline">
                            <p><em>Sidenote:</em> This file will be generated using side instructions and blank spreadsheet as a blueprint. The name of the file will be <code>remax-agent-rankings-<?php echo date('m-d-Y'); ?>.xlsx</code> and will contain all existing agents data and current awards.</p>
                        </div>
                    </div>
                </div>
            </div><!-- DOWNLOAD FROM DATABASE -->
        </div>

        <!-- sidebar -->
        <div id="postbox-container-1" class="postbox-container">
            <h1>Guide</h1>
            <div class="meta-box-sortables">
                <div class="postbox">

                    <div class="handlediv" title="Click to toggle"><br></div>
                    <!-- Toggle -->

                    <div class="inside">
                        <h3>How to run automated agent rankings update: </h3>

                        <a class="button-primary" href="/wp-content/themes/remax-ng-theme/agent-rankings-import.xlsx" download>Download blank spreadsheet</a>
                        
                        <p>Agent rankings spreadsheet contains 5 columns: </p>
                        <dl>
                            <dt><h3>Type</h3></dt>
                            <dd>
                                <p>Here you can specify in which category agent achieved his/hers scores.</p>
                                <p>The types are <code>sales</code> and <code>letting</code></p>
                                <p>It is important that the row value of this column have 's' or 'l' as first letters so system can place agent's scores in 'sales' or 'letting' category.</p>
                                <p>Default: <strong>sales</strong></p>
                            </dd>
                        </dl>

                        <dl>
                            <dt><h3>Email</h3></dt>
                            <dd>
                                <p>This is required field. Value of this field is checked for validity and used to decide wheter we should update an existing agent account or create a new one.</p>
                                <p>All created users will receive mail notification about their new account on RemaxIntranet.</p>
                                <p>Default: <strong>none</strong></p>
                            </dd>
                        </dl>

                        <dl>
                            <dt><h3>Nr of Props</h3></dt>
                            <dd>
                                <p>This is the total number of properties in specified category the agent let or sold.</p>
                                <p>Default: <strong>0</strong></p>
                            </dd>
                        </dl>

                        <dl>
                            <dt><h3>Award</h3></dt>
                            <dd>
                                <p>Here you should enter number in range 1 to 5.</p>
                                <p>Current definitions are: </p>
<pre>
Diamond ------ 1
Platinum ----- 2
Gold --------- 3
Silver ------- 4
Bronze ------- 5
</pre>
                                <p>Default: <strong>1</strong></p>
                            </dd>
                        </dl>

                        <dl>
                            <dt><h3>This Week</h3></dt>
                            <dd>
                                <p>This is the number of properties agent let or sold in this period of time.</p>
                                <p>A value of this field is used for the <em>Top This Week</em> charts.</p>
                                <p>Default: <strong>0</strong></p>
                            </dd>
                        </dl>
                    </div>
                    <!-- .inside -->

					</div>
            </div>
        </div>

        </div><!-- .metabox-holder.columns-2 -->
    </div><!-- #poststuff -->

</div><!-- .wrap -->