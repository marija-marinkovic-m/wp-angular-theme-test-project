<?php 
$office = get_user_meta($user->ID, 'rmx_office', true);
?>
<h3>Office</h3>
<table class="form-table">
    <tr>
        <th><label for="user_office">User's office </label></th>
        <td><input type="text" id="user_office" name="rmx_office" value="<?php echo $office ?>"></td>
    </tr>
</table>