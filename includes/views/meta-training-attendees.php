<?php 
$attendees = (array)get_post_meta(get_the_ID(), 'rt_training_attendees', true);
$all_users = get_users(array(
    'role' => 'Subscriber', 
    'fields' => array('ID', 'user_login', 'display_name')
));
echo '<ul>';
foreach ($all_users as $curr_user) {
    printf(
        '<li><label><span class="dashicons dashicons-admin-users"></span> <input type="checkbox" name="rt_training_attendees[]" value="%s"%s> %s</label></li>', 
        RemaxApiHelpers::attendee($curr_user), 
        in_array(RemaxApiHelpers::attendee($curr_user), $attendees) ? ' checked="checked"' : '', 
        $curr_user->display_name
    );
}
echo '</ul>';

