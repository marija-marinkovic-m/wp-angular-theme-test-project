<?php 
$biography = get_user_meta($user->ID, 'rmx_basic_info', true);
 ?>
<h3>User Biography (Basic info section)</h3>
<table class="form-table">
    <tr>
        <th><label for="user_biography">About user</label></th>
        <td>
            <?php wp_editor($biography, 'rmx_basic_info_box_id', array('textarea_name' => 'rmx_basic_info')); ?>
            <p class="description">This will be to content of the main section on the user's profile page.</p>
        </td>
    </tr>
</table>