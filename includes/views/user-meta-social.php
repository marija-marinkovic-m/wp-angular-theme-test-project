<?php 
$social = get_user_meta($user->ID, 'rmx_social', true);

$fb = @$social['facebook'];
$tw = @$social['twitter'];
$in = @$social['linkedin'];

 ?>
<h3>User Profile Links</h3>
<table class="form-table">
    <tr>
        <th><label for="facebook_profile">Facebook Profile</label></th>
        <td><input id="facebook_profile" type="text" name="rmx_social[facebook]" value="<?php echo $fb ?>" class="regular-text"></td>
    </tr>
    <tr>
        <th><label for="twitter_profile">Twitter Profile</label></th>
        <td><input id="twitter_profile" type="text" name="rmx_social[twitter]" value="<?php echo $tw ?>" class="regular-text"></td>
    </tr>
    <tr>
        <th><label for="linkedin_profile">Linkedin Profile</label></th>
        <td><input id="linkedin_profile" type="text" name="rmx_social[linkedin]" value="<?php echo $in ?>" class="regular-text"></td>
    </tr>
</table>