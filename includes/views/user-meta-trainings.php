<?php 
$attended_trainings = @RemaxApiHelpers::courses_attended($user);
if ($attended_trainings->have_posts()) : 
 ?>
<h3>Remax Courses attended info</h3>
<table class="form-table">
    <tr>
        <td colspan="2">
            <ul>
            <?php 
            foreach ($attended_trainings->posts as $training_post) {
                printf(
                    '<li><span class="dashicons dashicons-yes"></span> %s</li>', 
                    $training_post->post_title
                );
            }
             ?>
            </ul>
        </td>
    </tr>
</table>
<?php endif; ?>