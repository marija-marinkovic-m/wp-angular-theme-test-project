<?php 
$contact_info = get_user_meta($user->ID, 'rmx_contact', true);

$telephones = @$contact_info['phone'];
$email_addresses = @$contact_info['email'];

 ?>
<h3>Contact data</h3>
<table class="form-table">
    <tr>
        <th><label for="telephone_number">Telephone</label></th>
        <td>
            <textarea id="telephone_number" name="rmx_contact[phone]"><?php echo $telephones ?></textarea>
            <p class="description">One telephone number per line.</p>
        </td>
    </tr>
    <tr>
        <th><label for="email_address">Public (contact) address</label></th>
        <td>
            <textarea id="email_address" name="rmx_contact[email]"><?php echo $email_addresses ?></textarea>
            <p class="description">One email per line.</p>
        </td>
    </tr>
</table>