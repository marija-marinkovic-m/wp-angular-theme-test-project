<?php 
$the_types = get_the_terms(get_the_ID(), 'training-type');
$selected_type = !empty($the_types) ? $the_types[0]->slug : 'event';
$all_terms = get_terms('training-type', array('hide_empty' => false));
?>
<fieldset id="training-types-select">
    <div>
    <input type="hidden" id="current-training-type" value="<?php echo $selected_type ?>">
    <legend class="screen-reader-text">Training types</legend>
    <?php foreach ($all_terms as $term) {
        printf(
            '<input name="tax_input[training-type][]" id="ttype-%s" type="radio" value="%s" %s><label class="training-type-icon" for="ttype-%s">%s</label><br>', 
            $term->term_id, 
            $term->slug, checked($selected_type, $term->slug, false),
            $term->term_id, $term->name
        );
    } ?>
    </div>
</fieldset>