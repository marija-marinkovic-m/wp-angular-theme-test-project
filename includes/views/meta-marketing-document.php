<?php $doc_url = get_post_meta(get_the_ID(), 'rt_marketing_doc_url', true); ?>
<div class="etl-meta boxForm">
    <table class="form-table">
        <tr>
            <td>
                <label for="doc-link">Enter link to downloadable document: </label>
                <input type="text" name="rt_marketing_doc_url" value="<?php echo $doc_url ?>" id="doc-link"> <button type="button" class="button add-doc-url">Insert form media library</button>
            </td>
        </tr>
    </table>
</div>