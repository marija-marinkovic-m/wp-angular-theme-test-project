<div class="etl-meta boxForm">

    <h1>Event time</h1>
    <table class="form-table">
  <!-- EVENT DATE -->
        <?php  
        $data = array(
            'all_day_event', 
            'start_date', 
            'end_date', 
            'start_hour', 
            'start_min', 
            'start_ampm', 
            'end_hour', 
            'end_min', 
            'end_ampm'
        );
        
        foreach ($data as $field) {
            $$field = get_post_meta(get_the_ID(), "rt_{$field}", true);
        }
        $times = RMX()->getTimeUnits(); // hours / minutes / period 
         ?>
        <tr class="date-time">
            <td><label for="all-day">All day event? </label></td>
            <td><input type="checkbox" id="all-day" value="yes" name="rt_all_day_event" <?php checked($all_day_event, 'yes') ?>></td>
        </tr>

        <!-- start -->
        <tr class="date-time">
            <td><label for="date-meta-picker">Choose the event start date (required): </label></td>
            <td class="two-cols">
                <div class="col">
                    <input type="text" id="date-meta-picker" name="rt_start_date" value="<?php echo $start_date ?>">
                </div>
                <div class="col last" <?php echo !empty($all_day_event) ? 'style="display: none"' : '' ?>><i>@</i>
                    <?php printf(
                        '%s %s %s',
                        RMX()->selectBuilder($times->hours, 'rt_start_hour', $start_hour), 
                        RMX()->selectBuilder($times->minutes, 'rt_start_min', $start_min), 
                        RMX()->selectBuilder($times->period, 'rt_start_ampm', $start_ampm)
                    ) ?>
                </div>
            </td>
        </tr>

        <!-- end -->
        <tr class="date-time">
            <td><label for="end-date-meta-picker">Event ends: </label></td>
            <td class="two-cols">
                <div class="col">
                    <input type="text" id="end-date-meta-picker" name="rt_end_date" value="<?php echo $end_date ?>">
                </div>
                <div class="col last" <?php echo !empty($all_day_event) ? 'style="display: none"' : '' ?>><i>@</i>
                    <?php printf(
                        '%s %s %s',
                        RMX()->selectBuilder($times->hours, 'rt_end_hour', $end_hour), 
                        RMX()->selectBuilder($times->minutes, 'rt_end_min', $end_min), 
                        RMX()->selectBuilder($times->period, 'rt_end_ampm', $end_ampm)
                    ) ?>
                </div>
            </td>
        </tr>
  <!-- EVENT DATE -->
    </table>


    <h1>Location details</h1>
    <table class="form-table">
  <!-- LOCATION DETAILS -->
        <?php 
        $map_data = array(
            'lat', 
            'long', 
            'zoom', 
            'display_map'
        );
        foreach ($map_data as $map_field) {
            $$map_field = get_post_meta(get_the_ID(), "rt_{$map_field}", true);
        }
         ?>
        <tr>
            <td>
                <label for="event-venue">Venue: </label>
            </td>
            <td>
                <textarea id="event-venue" name="rt_event_venue"><?php echo get_post_meta(get_the_ID(), 'rt_event_venue', true) ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label>
                    <input type="checkbox" value="yes" name="rt_display_map" <?php checked($display_map, 'yes') ?>> Hide Map?
                </label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <script>
                    var mapLat = '<?php echo $lat ?>', 
                        mapLng = '<?php echo $long ?>', 
                        mapZoom = '<?php echo $zoom ?>';
                </script>
                <?php 
                array_walk($map_data, function($field_name) {
                    if ($field_name === 'display_map') {return false;}
                    printf(
                        '<input type="hidden" name="rt_%s" id="etl_map_%s" value="%s">', 
                        $field_name, $field_name, get_post_meta(get_the_ID(), 'rt_'. $field_name, true)
                    );
                });
                 ?>
                 <input id="pac-input" class="controls" type="text" placeholder="Search...">
                <div id="etl_map_holder"></div>
            </td>
        </tr>
  <!-- LOCATION -->
    </table>
</div>