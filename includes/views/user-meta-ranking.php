<?php 
$sales = get_user_meta($user->ID, 'rmx_ranking_sales', true);
$letting = get_user_meta($user->ID, 'rmx_ranking_letting', true);

$agent_awards = json_decode(AGENT_AWARDS);
 ?>
<h3>Agent Ranking</h3>
<table class="form-table">
    <!-- office -->

    <tr><th colspan="2"><h4 style="border-bottom: 1px dashed #dedede">LETTING</h4></th></tr>
    <!-- LETTING ************************************************ -->
        <!-- award -->
        <tr>
            <th><label>Current award</label></th>
            <td>
                <select name="rmx_ranking_letting[award]">
                <?php 
                foreach ($agent_awards as $id => $award_name) {
                    printf(
                        '<option value="%d"%s>%s</option>', 
                        absint($id), 
                        @$letting['award'] == $id ? ' selected="selected"' : '', 
                        $award_name
                        );
                }
                ?> 
                </select>
            </td>
        </tr>
        <!-- Nr of props -->
        <tr>
            <th><label>Number of properties</label></th>
            <td><input type="number" name="rmx_ranking_letting[props]" value="<?php echo absint(@$letting['props']) ?>"></td>
        </tr>
        <!-- Bonus points -->
        <tr>
            <th><label>This Week</label></th>
            <td><input type="text" name="rmx_ranking_letting[bonus]" value="<?php echo @$letting['bonus'] ?>"></td>
        </tr>

    <tr><th colspan="2"><h4 style="border-bottom: 1px dashed #dedede;">SALES</h4></th></tr>        
    <!-- SALES ************************************************ -->
        <!-- award -->
        <tr>
            <th><label>Current award</label></th>
            <td>
                <select name="rmx_ranking_sales[award]">
                <?php 
                foreach ($agent_awards as $id => $award_name) {
                    printf(
                        '<option value="%d"%s>%s</option>', 
                        absint($id), 
                        @$sales['award'] == $id ? ' selected="selected"' : '', 
                        $award_name
                        );
                }
                ?> 
                </select>
            </td>
        </tr>
        <!-- Nr of props -->
        <tr>
            <th><label>Number of properties</label></th>
            <td><input type="number" name="rmx_ranking_sales[props]" value="<?php echo absint(@$sales['props']) ?>"></td>
        </tr>
        <!-- Bonus points -->
        <tr>
            <th><label>Bonus</label></th>
            <td><input type="text" name="rmx_ranking_sales[bonus]" value="<?php echo @$sales['bonus'] ?>"></td>
        </tr>
</table>