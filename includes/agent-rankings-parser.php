<?php 
/**
* XLS parser 
* Agent rankings upload, parse and store / fetch from db, write and download
*
* Formats to read and write: 
*
* BIFF Format --------------------------------
* Used by Microsoft Excel between versions 95 and 2003 File
* extension: xls
* PHPEXcel Writer: PHPExcel_Writer_Excel5
* Mime Type: application/vnd.ms-excel
*
* OfficeOpenXML Format -----------------------
* Used by Microsoft Excel since version 2007
* File extension: xlsx
* PHPEXcel Writer: PHPExcel_Writer_Excel2007
* Mime Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
*
*/
require_once __DIR__ . '/../vendor/autoload.php';

class AgentRankingsParser {
    public static $_instance = null;
    public $option_key = 'agent_rankings_data';
    public $allowed_extensions = array('xls', 'xlsx');
    public $max_file_size = 2 * 1024 * 1024;

    // notice stuff 
    public $notice_msg = array();
    public $notice_class = '';

    public static function Instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
        // register subpage for handle files UI 
        add_action('admin_menu', array($this, 'register_subpage'));
        // file upload action hook 
        add_action('init', array($this, 'file_upload'));
    }

    public function register_subpage() {
        add_submenu_page(
            'users.php', 
            'Upload Agent Rankings', 
            'Agent Rankings', 
            'manage_options', 
            $this->option_key, 
            function() {
                include 'views/agent-rankings-subpage.php';
            }
        );
    }

    public function file_upload() {
        if (isset($_POST['rmx_admin_upload_box']) && wp_verify_nonce($_POST['rmx_admin_upload_box'], 'rmx_admin_upload_action') === 1) {
            if (!empty($_FILES['ratings_file']['name'])) {
                $this->process_upload('ratings_file');
            }
        }
        if (isset($_POST['rmx_admin_download_box']) && wp_verify_nonce($_POST['rmx_admin_download_box'], 'rmx_admin_download_action') === 1) {
            $this->process_download_from_database();
        }
    }

    /**
    * Upload and store methods 
    *
    * process_upload($input_name)
    * excelToArray($filePath, $header =true)
    * store_data($dataArray)
    */
    public function process_upload($input_name) {
        $extension_arr = array_slice(explode(".", @$_FILES[$input_name]['name']), -1);
        $extension = end($extension_arr);

        $errors = array();

        if (intval($_FILES[$input_name]['error']) !== 0) {
            $errors[] = 'There was an upload error.';
        }
        if ($_FILES[$input_name]['size'] >= $this->max_file_size) {
            $errors[] = 'File size exceeded maximum file size (2MB).';
        }
        if (!in_array($extension, $this->allowed_extensions)) {
            $errors[] = 'File type error. Allowed file extensions: '. implode(', ', $this->allowed_extensions) .'.';
        }

        if (empty($errors)) {
            
            $target_path = dirname(__FILE__) . '/files/' . $_FILES[$input_name]['name'];
            move_uploaded_file($_FILES[$input_name]['tmp_name'], $target_path);

            $this->notice_data('notice notice-success is-dismissible', array('Successfully uploaded file.'));

            // analyze sheet data ...
            $dataArray = $this->excelToArray($target_path);
            // insert data 
            $report = $this->store_data($dataArray);
            // notify user about results 
            $this->notify_report($report);

        } else {
            $this->notice_data('notice notice-error', $errors);
        }
    }

    public function excelToArray($filePath, $header = true) {
        $inputFileName = $filePath; 
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        //excel with first row header, use header as key
        if($header){
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
            $headingsArray = $headingsArray[1];
            $r = -1;
            $namedDataArray = array();
            for ($row = 2; $row <= $highestRow; ++$row) {
                $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
                if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                    ++$r;
                    foreach($headingsArray as $columnKey => $columnHeading) {
                        $namedDataArray[$r][$columnKey] = $dataRow[$row][$columnKey];
                    }
                }
            }
        } else {
            //excel sheet with no header
            $namedDataArray = $objWorksheet->toArray(null,true,true,true);
        }
        return $namedDataArray;
    }

    public function store_data($dataArray) {
        // model 
        // array('A' => 'sales' | 'letting', 'B' => email, 'C' => props, 'D' => award, 'E' => bonus)
        $report = array(
            'created' => array(), 
            'updated' => array(), 
            'invalid_email' => array(), 
            'invalid_row' => array()
        );
        foreach ($dataArray as $row_key => $row) {
            // map values 
            $type = $row['A']; 
            $email = $row['B']; 
            $props = $row['C']; 
            $award = $row['D']; 
            $bonus = $row['E'];

            if (is_null($email)) {
                array_push($report['invalid_row'], $row_key);
                continue;
            }

            if (is_null($type)) {
                array_push($report['invalid_row'], $row_key);
            }            

            if (!is_email($email)) {
                array_push($report['invalid_email'], $email);
                continue;
            }

            $user_id = email_exists($email);

            // prepare data
            $current_type = substr(strtolower(trim($type)), 0, 1) === 'l' ? 'letting' : 'sales';
            $meta_value = array(
                "props" => absint($props), 
                "award" => (absint($award) > 0 && absint($award) <= 5) ? absint($award) : 1, 
                "bonus" => absint($bonus)
            );
            if ($user_id !== false) {
                // update user meta
                update_user_meta($user_id, "rmx_ranking_{$current_type}", $meta_value);

                array_push($report['updated'], $user_id);
            } else {
                // create user
                $plaintext_pass = wp_generate_password();
                $new_user_login = explode('@', $email)[0];
                $new_user_id = wp_create_user($new_user_login, $plaintext_pass, $email);
                add_user_meta($new_user_id, "rmx_ranking_{$current_type}", $meta_value);

                // add office to be visible
                add_user_meta($new_user_id, 'rmx_office', '');                

                // notify user
                wp_new_user_notification($new_user_id, $plaintext_pass);

                array_push($report['created'], $user_id);
            }

        }

        return $report;
    }


    /**
    * Download from database methods 
    * 
    * process_download_from_database() 
    * write_file($file_name)
    */
    public function process_download_from_database() {
        $file_name = 'remax-agent-rankings-'. date('m-d-Y') .'.xlsx';
        $file_created = $this->write_file($file_name);

        if ($file_created) {

            header('Content-disposition: attachment; filename='. $file_name);
            header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            readfile($file_name);

        } else {

            $this->notice_data('notice notice-error is-dismissible', array('There were no rankings to be exported.'));

        }
    }

    /*
    * grab file name as @param, generate file and return boolean 
    */
    public function write_file($file_name) {
        global $wpdb; 

        // insert data 
        $rankings_query = "SELECT 
            users.user_email, 
            typemeta.meta_key as type, 
            typemeta.meta_value as stats 
        FROM 
            {$wpdb->users} as users 
            LEFT JOIN {$wpdb->usermeta} as typemeta ON users.ID = typemeta.user_id 
        WHERE 
            typemeta.meta_key = 'rmx_ranking_letting' 
            OR typemeta.meta_key = 'rmx_ranking_sales'
        ";
        $rankings = $wpdb->get_results($rankings_query);

        if ($rankings) {
            $objPHPExcel = new PHPExcel();

            // set properties 
            $objPHPExcel->getProperties()
                ->setCreator('RemaxIntranet')
                ->setTitle('Agent Rankings')
                ->setDescription('RemaxIntranet Agent Rankings for '. date(DATE_RSS));

            // prepare spreadsheet headers  
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()
                ->SetCellValueByColumnAndRow(0, 1, 'Type')
                ->SetCellValueByColumnAndRow(1, 1, 'Email')
                ->SetCellValueByColumnAndRow(2, 1, 'Nr of Props')
                ->SetCellValueByColumnAndRow(3, 1, 'Award')
                ->SetCellValueByColumnAndRow(4, 1, 'This Week');

            // headers color 
            $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID, 
                'startcolor' => array('rgb' => 'C0C0C0')
            ));

            $rowNum = 2;

            foreach ($rankings as $agent) {
                $type = substr($agent->type, strrpos($agent->type, '_') + 1);
                $stats = unserialize($agent->stats);

                $objPHPExcel->getActiveSheet()
                    ->SetCellValueByColumnAndRow(0, $rowNum, $type)
                    ->SetCellValueByColumnAndRow(1, $rowNum, $agent->user_email)
                    ->SetCellValueByColumnAndRow(2, $rowNum, $stats['props'])
                    ->SetCellValueByColumnAndRow(3, $rowNum, $stats['award'])
                    ->SetCellValueByColumnAndRow(4, $rowNum, $stats['bonus']);

                $rowNum++;

            }

            // write 
            $writer = new PHPExcel_Writer_Excel2007($objPHPExcel);
            $writer->save($file_name);
            return true;
        }
        return false;

    }


    /**
    * General helper methods 
    */
    public function notice_data($class, $msg) {
        $this->notice_class = $class;
        $this->notice_msg = $msg;
        add_action('admin_notices', array($this, 'show_notice'));
    }

    public function show_notice() {
        include 'views/admin-notices.php';
    }

    public function notify_report($report) {
        $msg = array();

        // errors 
        if (count($report['invalid_row']) > 0) {
            $row_nums = count($report['invalid_row']);
            $plural = $row_nums === 1 ? '': 's';

            array_push($msg, 'You have '. $row_nums .' invalid row'. $plural .' (email or type not specified). Please check your worksheet.');
        }
        if (count($report['invalid_email']) > 0) {
            $email_nums = count($report['invalid_email']);
            $plural = $email_nums === 1 ? '' : 's';
            $invalid_emails = implode(', ', $report['invalid_email']);

            array_push($msg, 'You have '. $email_nums .' invalid email'. $plural .' specified: '. $invalid_emails .'. Please check your worksheet.');
        }

        // success 
        if (count($report['created'])) {
            $created_num = count($report['created']);
            $plural_is_are = $created_num === 1 ? 'is' : 'are';
            $plural_s = $created_num === 1 ? '' : 's';

            array_push($msg, 'There '. $plural_is_are .' '. $created_num .' user account'. $plural_s .' successfully created.');
        }
        if (count($report['updated'])) {
            $updated_num = count($report['updated']);
            $plural_is_are = $updated_num === 1 ? 'is' : 'are';
            $plural_s = $updated_num === 1 ? '' : 's';

            array_push($msg, 'There '. $plural_is_are .' '. $updated_num .' user account'. $plural_s .' successfully updated.');
        }

        // report 
        if (!empty($msg)) {
            $this->notice_data('notice notice-success is-dismissible', $msg);
        }
    }
}


// Instantiate 
AgentRankingsParser::Instance();