var $ = jQuery.noConflict();

$(document).ready(function() {

    /**
    * GOOGLE MAP (v3)
    * inits
    */
    var geocoder, newyork, map, marker, initLatLng, zoomLevel,
        dateMetaPicker = document.getElementById('date-meta-picker');

    if (typeof(dateMetaPicker) != 'undefined' && dateMetaPicker != null) {
        
        // handle search field keypress
        function khandle(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
        }
        // Map init
        function initialize() {
            newyork = new google.maps.LatLng(40.7141667, -74.0063889);

            // set coordinates
            if (typeof(mapLat) != 'undefined' && typeof(mapLng) != 'undefined' && mapLat.length > 0 && mapLng.length > 0) {
                initLatLng = new google.maps.LatLng(mapLat, mapLng);
            } else {
                initLatLng = newyork;
            }
            // set zoom level
            if (typeof(mapZoom) != 'undefined' && mapZoom.length > 0) {
                zoomLevel = parseInt(mapZoom);
            } else {
                zoomLevel = 10;
            }
            // set map and marker
            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                zoom: zoomLevel,
                center: initLatLng
            };
            markers = [];
            map = new google.maps.Map(document.getElementById('etl_map_holder'), mapOptions);
            marker = new google.maps.Marker({
                position: initLatLng,
                map: map
            });

            // event listeners
            google.maps.event.addListener(map, 'click', function(event) {
                marker.setMap(null);
                placeMarker(event.latLng, map);

                var lat = event.latLng.lat();
                var lng = event.latLng.lng();

                document.getElementById('etl_map_lat').value = lat;
                document.getElementById('etl_map_long').value = lng;
            });
            google.maps.event.addListener(map, 'zoom_changed', function() {
                zoomLevel = map.getZoom();
                document.getElementById('etl_map_zoom').value = zoomLevel;
            });

            // Create the search box and link it to the UI element.
            document.getElementById('pac-input').onkeypress = khandle;

            var input = (document.getElementById('pac-input'));
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var searchBox = new google.maps.places.SearchBox((input));

            google.maps.event.addListener(searchBox, 'places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                return;
                }

                for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
                }
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                markers.push(marker);

                bounds.extend(place.geometry.location);
                }
                map.fitBounds(bounds);
            });

            // Bias the SearchBox results towards places that are within the bounds of the
            // current map's viewport.
            google.maps.event.addListener(map, 'bounds_changed', function() {
                var bounds = map.getBounds();
                searchBox.setBounds(bounds);
            });
        }
        // place marker function
        function placeMarker(position, map) {
            marker = new google.maps.Marker({
                position: position,
                map: map
            });
            map.panTo(position);
        }
        // INITIALIZE
        google.maps.event.addDomListener(window, 'load', initialize);

        function hide(id) {
            var d = document.getElementById(id);
            d.className = d.className.replace(' hide', '');
            d.className = d.className + ' hide';
        }
    }

    /**
     * On training types change 
     * display proper box for metadata
     */
    var currentType = $('#current-training-type').val(), 
        showTypeBoxes = function(currentType) {
            if (typeof currentType !== "undefined") {
                switch(currentType) {
                    case 'video': 
                        $('#training-video, #postdivrich').addClass('showEl');
                        break;
                    case 'document': 
                        $('#training-document').addClass('showEl');
                        break;
                    default: // event
                        $('#postdivrich, #training-event').addClass('showEl');
                        break;
                }
            }
        };
    showTypeBoxes(currentType);
    $('#training-types-select').find('input[type="radio"]').on('change', function() {
        $('.showEl').removeClass('showEl');
        showTypeBoxes($(this).val());
    });

    // Datepicker settings
    $(function($) {
        $( "#date-meta-picker, #end-date-meta-picker" ).datepicker({dateFormat: "yy-mm-dd"});
    });

    $('#all-day').change(function(){
        if($('#all-day')[0].checked)
            $('div.col.last').fadeOut('slow');
        else
            $('div.col.last').fadeIn('slow');

    });

    // Document type settings 
    // doc_url via media lib 
    $('.add-doc-url').on('click', function() {
        var button = $(this);
        wp.media.editor.send.attachment = function(props, attachment) {
            button.prev().val(attachment.url);
        };
        wp.media.editor.open(button);
    });
});