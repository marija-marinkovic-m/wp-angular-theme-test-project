<?php 
/**
* Extra fields for handling user data 
*/

class Rmx_User_Management {

    public static $_instance = null;

    public static function Instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct() {
        // show / edit
        add_action('show_user_profile', array($this, 'add_extra_fields'));
        add_action('edit_user_profile', array($this, 'add_extra_fields'));
        // save 
        add_action('personal_options_update', array($this, 'save_extra_fields'));
        add_action('edit_user_profile_update', array($this, 'save_extra_fields'));
    }

    public function add_extra_fields($user) {
        include_once('views/user-meta-office.php');
        include_once('views/user-meta-biography.php');
        include_once('views/user-meta-contact.php');
        include_once('views/user-meta-social.php');
        include_once('views/user-meta-ranking.php');
        include_once('views/user-meta-trainings.php');
    }

    public function save_extra_fields($user_id) {
        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }
        foreach ($_POST as $post_field => $post_value) {
            if (strpos($post_field, 'rmx_') === 0) {
                update_user_meta(absint($user_id), $post_field, $post_value);
            }
        }
    }

}

// Instantiate
Rmx_User_Management::Instance();
