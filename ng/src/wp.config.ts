/**
 * @module WordpressConfig
 */
/**
 * 
 */
import { mockWpConfig } from './assets/mock-data/wp-config';
import { Post } from './app/components/posts-pages/page.models';

export interface MenuItem {
    item_id: number,
    item_parent: string, 
    title: string, 
    slug: string, 
    link_type: string, 
    url: string, 
    class: string, 
    children?: Array<MenuItem>
};

export interface Taxonomy {
    term_id: number,
    slug: string, 
    taxonomy: string, 
    name: string, 
    count: number
}

export interface TaxonomyGroup {
    category_id: string, 
    category_name: string, 
    posts: Array<Post>
}

export interface Author {
    ID: number, 
    user_login: string, 
    display_name: string, 
    user_nicename: string
}

export interface Config {
    template_directory: string, 
    api_root: string, 
    menu: MenuItem[], 
    site_url?: string, 
    site_title?: string, 
    home_id?: string | number, 
    blog_id?: string | number, 
    categories?: Taxonomy[], 
    tags?: Taxonomy[], 
    authors: Author[], 
    posts_per_page: number;
    nonce: string;
    ajax_url: string;
    awards: {}
};
declare var app_config;
let main_config = typeof(app_config) !== "undefined" ? app_config : mockWpConfig, 
    api_link = 'http://remax.dev/wp-json/';

const default_config: Config = {
    template_directory: '', 
    api_root: (() => {
        const links = document.getElementsByTagName('link'); 
        const link = Array.prototype.filter.call(links, function(item) {
            return item.rel === 'https://api.w.org/';
        });
        return `${link.length ? link[0].href : api_link}`;
    })(), 
    menu: [], 
    authors: [], 
    posts_per_page: 6, 
    nonce: '', 
    ajax_url: '', 
    awards: {}
}

export const wp_data: Config = Object.assign(default_config, main_config);

export var ANIM_SPEED = 300;