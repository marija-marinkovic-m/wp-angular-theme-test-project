/**
 * @module BrowserConfig
 */
/**
 * 
 */
// For vendors for example jQuery, Lodash, angular2-jwt just import them here unless you plan on
// chunking vendors files for async loading. You would need to import the async loaded vendors
// at the entry point of the async loaded file. Also see custom-typings.d.ts as you also need to
// run `typings install x` where `x` is your module

// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/forms';
import '@angular/http';
import '@angular/router';

// AngularClass
import '@angularclass/webpack-toolkit';
import '@angularclass/request-idle-callback';

// RxJS
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

// Angular2 Google Maps
import './app/components/trainings/shared/angular2-google-maps/core/core.umd.js';

/**
 * MATERIAL DESIGN LIGHT
 * https://getmdl.io
 *  
 */ 
// Component handler
import './assets/scss/mdl/mdlComponentHandler';
// Base components
import './assets/scss/mdl/button/button';
import './assets/scss/mdl/checkbox/checkbox';
// import './assets/scss/mdl/icon-toggle/icon-toggle';
import './assets/scss/mdl/menu/menu';
// import './assets/scss/mdl/progress/progress';
import './assets/scss/mdl/radio/radio';
// import './assets/scss/mdl/slider/slider';
import './assets/scss/mdl/spinner/spinner';
// import './assets/scss/mdl/switch/switch';
import './assets/scss/mdl/tabs/tabs';
import './assets/scss/mdl/textfield/textfield';
// import './assets/scss/mdl/tooltip/tooltip';
// Complex components (which reuse base components)
import './assets/scss/mdl/layout/layout';
import './assets/scss/mdl/data-table/data-table';
// And finally, the ripples
import './assets/scss/mdl/ripple/ripple';

/**
 * QUILL EDITOR 
 * http://quilljs.com/
 */
//import './assets/quill';

if ('production' === ENV) {
  // Production


} else {
  // Development
  require('angular2-hmr');

}
