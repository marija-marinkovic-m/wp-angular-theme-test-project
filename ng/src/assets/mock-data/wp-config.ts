/**
 * @module MockUpConfig
 */
/**
 * 
 */
export let mockWpConfig = {
	"template_directory" : "http:\/\/remax.dev\/wp-content\/themes\/remax-ng-theme\/ng\/dist\/",
	"site_url" : "http:\/\/remax.dev",
	"site_title" : "Remax Intranet",
	"site_description" : "Just another WordPress site",
	"home_id" : "7",
	"blog_id" : "11",
	"admin_email" : "marija.marinkovic@eutelnet.com",
	"menu" : [{
			"item_id" : 10,
			"item_parent" : "0",
			"title" : "Home",
			"slug" : "home",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/",
			"class" : "fa fa-home"
		}, {
			"item_id" : 38,
			"item_parent" : "0",
			"title" : "Marketing",
			"slug" : "marketing",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/marketing\/",
			"class" : "fa fa-bullseye"
		}, {
			"item_id" : 13,
			"item_parent" : "0",
			"title" : "News",
			"slug" : "news",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/news\/",
			"class" : "fa fa-newspaper-o"
		}, {
			"item_id" : 15,
			"item_parent" : "13",
			"title" : "Company News",
			"slug" : "company-news",
			"link_type" : "Category",
			"url" : "http:\/\/remax.dev\/category\/company-news\/",
			"class" : ""
		}, {
			"item_id" : 16,
			"item_parent" : "13",
			"title" : "Marketing News",
			"slug" : "marketing-news",
			"link_type" : "Category",
			"url" : "http:\/\/remax.dev\/category\/marketing-news\/",
			"class" : ""
		}, {
			"item_id" : 45,
			"item_parent" : "0",
			"title" : "Trainings",
			"slug" : "trainings",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/trainings\/",
			"class" : "fa fa-graduation-cap"
		}, {
			"item_id" : 46,
			"item_parent" : "0",
			"title" : "Knowledge base",
			"slug" : "knowledge-base",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/knowledge-base\/",
			"class" : "fa fa-lightbulb-o"
		}
	],
	"links" : [{
			"item_id" : 19,
			"item_parent" : "0",
			"title" : "Company Structure",
			"slug" : "company-structure",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/company-structure\/",
			"class" : ""
		}, {
			"item_id" : 20,
			"item_parent" : "0",
			"title" : "Remax Europe",
			"slug" : "remax-europe",
			"link_type" : "Page",
			"url" : "http:\/\/remax.dev\/remax-europe\/",
			"class" : ""
		}, {
			"item_id" : 21,
			"item_parent" : "0",
			"title" : "Hello world!",
			"slug" : "hello-world",
			"link_type" : "Post",
			"url" : "http:\/\/remax.dev\/hello-world\/",
			"class" : ""
		}, {
			"item_id" : 48,
			"item_parent" : "0",
			"title" : "External link",
			"slug" : null,
			"link_type" : "Custom Link",
			"url" : "http:\/\/google.com",
			"class" : ""
		}
	],
	"categories" : [{
			"term_id" : 3,
			"name" : "Company News",
			"slug" : "company-news",
			"term_group" : 0,
			"term_taxonomy_id" : 3,
			"taxonomy" : "category",
			"description" : "",
			"parent" : 0,
			"count" : 2,
			"filter" : "raw",
			"cat_ID" : 3,
			"category_count" : 2,
			"category_description" : "",
			"cat_name" : "Company News",
			"category_nicename" : "company-news",
			"category_parent" : 0
		}, {
			"term_id" : 4,
			"name" : "Marketing News",
			"slug" : "marketing-news",
			"term_group" : 0,
			"term_taxonomy_id" : 4,
			"taxonomy" : "category",
			"description" : "",
			"parent" : 0,
			"count" : 2,
			"filter" : "raw",
			"cat_ID" : 4,
			"category_count" : 2,
			"category_description" : "",
			"cat_name" : "Marketing News",
			"category_nicename" : "marketing-news",
			"category_parent" : 0
		}, {
			"term_id" : 1,
			"name" : "Uncategorized",
			"slug" : "uncategorized",
			"term_group" : 0,
			"term_taxonomy_id" : 1,
			"taxonomy" : "category",
			"description" : "",
			"parent" : 0,
			"count" : 1,
			"filter" : "raw",
			"cat_ID" : 1,
			"category_count" : 1,
			"category_description" : "",
			"cat_name" : "Uncategorized",
			"category_nicename" : "uncategorized",
			"category_parent" : 0
		}
	],
	"tags" : [{
			"term_id" : 7,
			"name" : "appartment",
			"slug" : "appartment",
			"term_group" : 0,
			"term_taxonomy_id" : 7,
			"taxonomy" : "post_tag",
			"description" : "",
			"parent" : 0,
			"count" : 1,
			"filter" : "raw"
		}, {
			"term_id" : 6,
			"name" : "flat",
			"slug" : "flat",
			"term_group" : 0,
			"term_taxonomy_id" : 6,
			"taxonomy" : "post_tag",
			"description" : "",
			"parent" : 0,
			"count" : 2,
			"filter" : "raw"
		}, {
			"term_id" : 8,
			"name" : "new",
			"slug" : "new",
			"term_group" : 0,
			"term_taxonomy_id" : 8,
			"taxonomy" : "post_tag",
			"description" : "",
			"parent" : 0,
			"count" : 1,
			"filter" : "raw"
		}
	],
	"authors" : [{
			"ID" : "1",
			"user_login" : "admin",
			"display_name" : "admin"
		}
	], 
	"ajax_url":"http:\/\/remax.dev\/wp-admin\/admin-ajax.php", 
	"awards": {"1":"Diamond","2":"Platinum","3":"Gold","4":"Silver","5":"Bronze"}
};