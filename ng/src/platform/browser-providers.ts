
/**
 * @module BrowserConfig
 */
/**
 * 
 */
/*
 * These are globally available services in any component or any other service
 */
import { provide } from '@angular/core';
import { HttpInterceptor } from '../app/auth';

// Angular 2
// import { HashLocationStrategy, LocationStrategy } from '@angular/common';
// Angular 2 Http
import { HTTP_PROVIDERS, XHRBackend, RequestOptions, Http } from '@angular/http';
// Angular 2 Router
import { provideRouter, Router } from '@angular/router';
// Angular 2 forms
import { disableDeprecatedForms, provideForms } from '@angular/forms';

// Title 
import { Title } from '@angular/platform-browser';

// AngularClass
import { provideWebpack } from '@angularclass/webpack-toolkit';
import { providePrefetchIdleCallbacks } from '@angularclass/request-idle-callback';
import { routes, asyncRoutes, prefetchRouteCallbacks } from '../app/app.routes';
/*
* Application Providers/Directives/Pipes
* providers/directives/pipes that only live in our browser environment
*/
export const APPLICATION_PROVIDERS = [
  // new Angular 2 forms
  disableDeprecatedForms(),
  provideForms(),

  provideRouter(routes),
  provideWebpack(asyncRoutes),
  providePrefetchIdleCallbacks(prefetchRouteCallbacks),

  ...HTTP_PROVIDERS,
  provide(Http, {
    useFactory: (xhrBackend: XHRBackend, requestOptions: RequestOptions, router: Router) => new HttpInterceptor(xhrBackend, requestOptions, router), 
    deps: [XHRBackend, RequestOptions, Router]
  }), 
  Title

  //{ provide: LocationStrategy, useClass: HashLocationStrategy }
];

export const PROVIDERS = [
  ...APPLICATION_PROVIDERS
];
