/**
 * @module AgentProfile
 */
/**
 * 
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { AgentsData } from './agents-data.service';
import { Agent } from './agent.model';
import { User } from '../auth';

import { routeInfoCollector, AppState } from '../app.service';
import { ANIM_SPEED } from '../../wp.config';
import { ProfileSidebar } from '../partials';

/**
 * Agent Profile component (view)
 */
@Component({
    templateUrl: './profile.template.html', 
    providers: [AgentsData], 
    directives: [ProfileSidebar]
})
export class AgentProfile implements OnInit {
    public user: Agent;
    public viewer: User;
    constructor(
        private _route: ActivatedRoute, 
        private _dataService: AgentsData, 
        private _titleService: Title, 
        private _appState: AppState
    ) {}

    ngOnInit() {
        this._route.params.subscribe(
            params => {
                let slug = params['slug'];
                this._dataService.one(slug).subscribe(
                    data => {
                        this.user = data;
                        this._titleService.setTitle(this.user.name);
                        this._appState.set('routeInfo', routeInfoCollector(this.user.name, this.user.office, '', this.user.avatar_urls['96']));
                    }
                );
            }
        );
        this.viewer = JSON.parse(localStorage.getItem('authorized'));
    }
}