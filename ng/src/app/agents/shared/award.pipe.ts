/**
 * @module Agents
 */
/**
 * 
 */
import { Pipe, PipeTransform } from '@angular/core';
import { wp_data, Config } from '../../../wp.config';

@Pipe({name: 'award'})
export class AwardPipe implements PipeTransform {
    wp: Config = Object.assign({}, wp_data);

    transform(value: string) {
        if (this.wp.awards.hasOwnProperty(value)) {
            return this.wp.awards[value];
        }
        return value;
    }
}