/**
 * @module Agents
 */
/**
 * 
 */
export interface Agent {
    id: number, 
    name: string, 
    slug: string, 
    email: string, 
    avatar_urls: {
        "24": string, 
        "48": string, 
        "96": string
    }, 
    office: string, 
    bio: string, // safe, rendered HTML
    contact: {
        phone: Array<string>, 
        email: Array<string>
    }, 
    social: {
        facebook: string, 
        twitter: string, 
        linkedin: string
    }, 
    courses: Array<{
        title: {
            rendered: string
        }, 
        slug: string, 
        time?: {
            all_day: boolean, 
            start: string, 
            end: string
        }, 
        doc_url?: string
    }>
}

export interface AgentRanking {
    name: string;
    award: number; 
    props: number; 
    bonus: number; // or "This Week" I gues number of props
    office: string;
}