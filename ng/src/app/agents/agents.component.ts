/**
 * @module Agents
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';

import { Agent, AgentRanking } from './agent.model';
import { AgentsData } from './agents-data.service';
import { AwardPipe } from './shared';
import { AppState, routeInfoCollector } from '../app.service';

/*
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`Agents` component loaded asynchronously');

@Component({
	selector: 'agents-rankings',
	styles: [`
		.mdl-grid.rankings {justify-content: space-between;}
			.mdl-grid.rankings .mdl-cell {overflow:auto;}
		.mdl-data-table {width: 100%;}
		.mdl-data-table.topThisWeek {width: 30%;}
		.mdl-tabs__tab-bar > a {text-transform: capitalize;}
		.mdl-tabs__panel {padding: 45px; overflow: auto;}
		@media (max-width: 839px) {
			.mdl-tabs__panel {padding: 20px;}
		}		
	`],
	templateUrl: './rankings-list.template.html', 
	pipes: [AwardPipe]
})
export class Agents {
	resolvedState;
	currentTab: string = '';
	allTabs: Array<string> = ['sales', 'letting'];

	panels: Array<{
		slug: string, 
		agents: Observable<AgentRanking[]>, 
		topThisWeek: Observable<AgentRanking[]>
	}> = [];

	constructor(
		private _appState: AppState, 
		private _titleService: Title, 
		private _router: Router, 
		private _activatedRoute: ActivatedRoute, 
		private _dataService: AgentsData
	) {}

	ngOnInit() {
		this._appState.set('routeInfo', routeInfoCollector('Agent Rankings'));
		this._titleService.setTitle('Remax - Agent Rankings');

		// define tabs 
		this.allTabs.forEach(tab => {
			this.panels.push({
				slug: tab, 
				agents: this._dataService.rankings(tab), 
				topThisWeek: this._dataService.topThisWeek(5, tab)
			});
		});

		this._activatedRoute
			.params.subscribe(
				params => {
					if (typeof params['tab'] !== "undefined" && this.allTabs.indexOf(params['tab']) > -1) {
						this.currentTab = params['tab'];	
					} else {
						this.currentTab = 'sales';
					}				
				}
			);
	}

}
