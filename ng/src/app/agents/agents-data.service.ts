/**
 * @module Agents
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { AppState } from '../app.service';
import { wp_data } from '../../wp.config';
import { Agent, AgentRanking } from './agent.model';

@Injectable() 
export class AgentsData {
    private dataUrl = wp_data.api_root + 'remax/v1/users';
    private rankingUrl = wp_data.api_root + 'remax/v1/agent-rankings';
    constructor(
        private _http: Http, 
        private _router: Router, 
        private _app: AppState
    ) {}
    one(slug: string): Observable<Agent> {
        return this._http.get(`${this.dataUrl}/${slug}`)
            .map(i => i.json())
            .catch(() => {
                this._router.navigate(['/not_found']);
                return Observable.throw('User does not exist');
            });
    }
    rankings(type: string = 'sales'): Observable<AgentRanking[]> {
        // check for table in app state | if empty get from api 
        let stateClone = this._app.state;
        if (stateClone.hasOwnProperty(`${type}Rankings`)) {
            return Observable.of(this._app.get(`${type}Rankings`));
        }
        return this._http.get(this.rankingUrl + '?type=' + type)
            .map(response => {
                let body: AgentRanking[] = response.json();
                let rankings = body.filter(agent => agent.props > 0);
                this._app.set(`${type}Rankings`, rankings);
                return rankings;
            });
    }

    topRankings(num: number = 5, type: string = 'sales'): Observable<AgentRanking[]> {
        return this.rankings(type)
            .map(res => res.slice(0, num));
    }

    topThisWeek(num: number = 5, type: string = 'sales') {
        return this.rankings(type)
            .map(res => {
                return res
                        .filter(item => item.bonus > 0)
                        .sort((prev, next) => next.bonus - prev.bonus)
                        .slice(0, num);
            });
    }
}
