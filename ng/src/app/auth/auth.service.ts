/**
 * @module Auth
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/concatMap';

import { User } from './shared/user.model';
import { wp_data, Config } from '../../wp.config';

@Injectable()
export class AuthService {
    wp: Config;
    constructor(
        private _http: Http
    ) {
        this.wp = Object.assign({}, wp_data);
    }

    authenticate(): Observable<boolean> {
        if (null !== localStorage.getItem('authorized')) {
            return Observable.of(true);
        }
        return this.getUserData()
            .map(r => true);
    }

    getUserData() {
        return this._http.get(this.wp.api_root + 'remax/v1/users/me')
            .catch((error, source) => {
                localStorage.clear();
                return Observable.throw('Not authorized.');
            });
    }

    setUserData() {
        return this.getUserData()
            .map(r => {
                let body = r.json();
                let user = Object.assign({}, body.data);
                    user.roles = body.roles;
                localStorage.setItem('authorized', JSON.stringify(user));
                return body;
            });
    }

    login(body: {email: string, password: string, rememberme: boolean}): Observable<{}> {

        let data = new FormData();
        data.append("action", "rmxlogin");
        data.append("rest_nonce", this.wp.nonce);
        data.append("email", body.email);
        data.append("password", body.password);
        data.append("rememberme", body.rememberme);

        return this._http.post(this.wp.ajax_url, data)
            .concatMap(r => {
                let loginData = r.json();
                if (loginData.loggedin && loginData.hasOwnProperty('user')) {
                    let user = Object.assign({}, loginData.user.data);
                    user.roles = loginData.user.roles;
                    localStorage.setItem('authorized', JSON.stringify(user));
                    return Observable.of(loginData);
                }
                return Observable.throw(loginData.message);
            });
    }

    logout(): Observable<Response> {
        let data = new FormData();
        data.append("action", "rmxlogout");
        data.append("rest_nonce", this.wp.nonce);

        return this._http.post(this.wp.ajax_url, data)
            .map(r => {
                localStorage.removeItem('authorized');
                return r.json();
            });
    }

    forgotPassword(value: {email: string}) {
        let data = new FormData();
        data.append("action", "rmxforgot");
        data.append("rest_nonce", this.wp.nonce);
        data.append("user_login", value.email);

        return this._http.post(this.wp.ajax_url, data)
            .map(r => r.json());
    }

    resetPassword(values: {pass1: string, pass2: string}) {
        let user: User = JSON.parse(localStorage.getItem('authorized'));
        let userID = user.ID;
        let data = new FormData();
        data.append("action", "rmxreset");
        data.append("rest_nonce", this.wp.nonce);
        data.append("user_id", userID);

        data.append("pass1", values.pass1);
        data.append("pass2", values.pass2);

        return this._http.post(this.wp.ajax_url, data)
            .map(r => r.json());

    }

    getAvatar(size: '24' | '48' | '96') {
        return this._http.get(this.wp.api_root + 'wp/v2/users/me')
            .map(response => {
                let body = response.json();
                if (body.hasOwnProperty('avatar_urls') && body.avatar_urls.hasOwnProperty(size)) {
                    return body.avatar_urls[size];
                }
                return false;
            });
    }

}