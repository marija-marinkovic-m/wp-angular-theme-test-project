/**
 * @module Auth
 */
/**
 * 
 */
import { Http, Request, RequestOptionsArgs, Response, XHRBackend, RequestOptions, ConnectionBackend, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import { wp_data, Config } from '../../wp.config';

export class HttpInterceptor extends Http {
    public wp: Config = Object.assign({}, wp_data); 

    constructor(
        backend: ConnectionBackend, 
        defaultOptions: RequestOptions, 
        private _router: Router
    ) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.request(url, options));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url, this.getRequestOptionArgs(options)));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {   
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, options));
    }

    getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('X-WP-Nonce', this.wp.nonce);
        return options;
    }

    intercept(observable: Observable<Response>): Observable<any> {
        return observable.catch((err, source) => {
            if (err.status === 403) {
                if (localStorage.length > 0) {
                    this._router.navigate(['/not_authorized']);
                    localStorage.clear();
                    location.assign(this.wp.site_url + '/login');
                } else {
                    this._router.navigate(['/login']);
                }
                return Observable.empty();
            }
            return Observable.throw(err);
        });
    }
}