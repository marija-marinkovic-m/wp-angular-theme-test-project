/**
 * @module Auth
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { wp_data, Config } from '../../wp.config';

@Injectable()
export class AuthGuard implements CanActivate {
    wp: Config = Object.assign({}, wp_data);

    constructor(
        private _authService: AuthService, 
        private _router: Router
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        return this._authService.authenticate()
            .catch((error, status) => {
                this._router.navigate(['/login']);
                return Observable.throw('Please, authorize');
            });
    }
}