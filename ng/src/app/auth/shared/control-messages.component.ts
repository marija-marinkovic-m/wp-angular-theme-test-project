/**
 * @module AuthShared
 */
/**
 * 
 */
import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from './validation.service';

@Component({
  selector: 'control-messages',
  template: `<span *ngIf="errorMessage !== null">{{errorMessage}}</span>`, 
  host: {class: 'mdl-textfield__error'}, 
  styles: [`
    :host {visibility: visible;} 
    :host span {display: block; text-align:left;}
  `]
})
export class ControlMessages {
  
  @Input() control: FormControl;
  constructor() { }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }
    
    return null;
  }
}