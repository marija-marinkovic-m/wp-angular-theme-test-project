/**
 * @module AuthShared
 */
/**
 * 
 */
export interface User {
    ID: string, 
    user_login: string, 
    user_pass: string, 
    user_nicename: string, 
    user_email: string, 
    user_url: string, 
    user_registered: string, 
    user_status: string, 
    display_name: string, 
    roles: string[]
}