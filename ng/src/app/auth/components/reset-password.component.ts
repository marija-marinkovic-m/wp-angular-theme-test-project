/**
 * @module AuthComponents
 */
/**
 * 
 */
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { ControlMessages, ValidationService } from '../shared';
import { AppState, routeInfoCollector } from '../../app.service';
import { AuthService } from '../auth.service';
import { wp_data, Config } from '../../../wp.config';

@Component({
    selector: 'reset-password', 
    directives: [ControlMessages], 
    template: `
    <main class="mdl-grid">
        <div class="mdl-cell mdl-cell--9-col mdl-card mdl-shadow--2px">

            <div class="main-placeholder content">
                <h2>Enter your new password</h2>
                <form 
                    novalidate 
                    [formGroup]="resetForm" 
                    (submit)="onSubmit()" 
                    class="auth-form mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col">

                    <div 
                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-group" 
                        [ngClass]="{'is-invalid': resetForm.controls.pass1.touched && resetForm.controls.pass1.errors}"
                        >
                        <input type="password" formControlName="pass1" class="mdl-textfield__input" id="new-pass">
                        <label class="mdl-textfield__label" for="pass1">New password...</label>
                        <control-messages [control]="resetForm.controls.pass1"></control-messages>
                    </div>

                    <div 
                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-group" 
                        [ngClass]="{'is-invalid': resetForm.controls.pass2.touched && resetForm.controls.pass2.errors}"
                        >
                        <input type="password" formControlName="pass2" class="mdl-textfield__input" id="new-pass">
                        <label class="mdl-textfield__label" for="pass2">Confirm password...</label>
                        <control-messages [control]="resetForm.controls.pass2"></control-messages>
                    </div>

                    <div>
                        <button [disabled]="!resetForm.valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised">Submit your new password</button>
                    </div>

                </form>

                <p *ngIf="responseMsg">{{ responseMsg }}</p>
            </div>
        </div>
    </main>
    `, 
    styleUrls: ['./auth-form.style.css']
})
export class ResetPassword implements OnInit, AfterViewInit {
    wp: Config = Object.assign({}, wp_data);
    title: string = 'Reset Your Password';
    resetForm: FormGroup;
    responseMsg: string; 

    constructor(
        public titleService: Title, 
        private _appState: AppState, 
        private _authService: AuthService, 
        private _formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.resetForm = this._formBuilder.group({
            pass1: ['', [Validators.required]], 
            pass2: ['']
        })

        this.titleService.setTitle(this.title);
        this._appState.set('routeInfo', routeInfoCollector(this.title, '', 'fa fa-unlock-alt'));

        this.resetForm.controls['pass2'].valueChanges
            .debounceTime(400)
            .subscribe(val => {
                this.validateConfirmedPass(val == this.resetForm.controls['pass1'].value);
            });

        this.resetForm.controls['pass1'].valueChanges
            .debounceTime(400)
            .subscribe(val => {
                this.validateConfirmedPass(val == this.resetForm.controls['pass2'].value);
            });
    }

    ngAfterViewInit() {
        componentHandler.upgradeDom();
    }

    onSubmit() {
        this.responseMsg = '';
        this._authService.resetPassword(this.resetForm.value)
            .subscribe(
                r => {
                    this.responseMsg = JSON.stringify(r.messages);
                    setTimeout(() => {
                        localStorage.clear();
                        location.reload();
                    }, 300);
                }, 
                e => this.responseMsg = JSON.stringify(e)
            );
        
        //this.responseMsg = JSON.stringify(this.resetForm.value);
    }

    validateConfirmedPass(condition: boolean) {
        if (condition) {
            this.resetForm.controls['pass2'].clearValidators();
        } else {
            this.resetForm.controls['pass2'].setErrors({
                'notMatch': true
            }, {emitEvent: true});
        }
    }
}