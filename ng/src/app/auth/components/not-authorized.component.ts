/**
 * @module AuthComponents
 */
/**
 * 
 */
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AppState, routeInfoCollector } from '../../app.service';

@Component({
    template: `
    <main class="mdl-grid" *ngIf="storageEmpty">
        <div class="mdl-cell mdl-cell--9-col mdl-card mdl-shadow--2p">
            <div class="main-placeholder content">
                <h5>{{ message }}</h5>
                <p>Redirecting...</p>
            </div>
        </div>
    </main>`
})
export class NotAuthorized implements OnInit {
    storageEmpty: boolean = localStorage.length === 0;
    message: string = 'You have been logged out. Please log in.';
    constructor(
        public titleService: Title, 
        private _appState: AppState
    ) {}

    ngOnInit() {
        let mainTitle = this.storageEmpty ? 'Logged Out' : 'You are Logged In';
        this.titleService.setTitle('Authorized?');
        this._appState.set('routeInfo', routeInfoCollector(mainTitle, '', 'fa fa-sign-in'));
    }
}