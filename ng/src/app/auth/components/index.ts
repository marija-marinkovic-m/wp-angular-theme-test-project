/**
 * @module AuthComponents
 * @preferred 
 * 
 * Here are defined screens / controllsers for login, forgo password, reset password 
 * and not authorized scenarios
 */
/**
 * 
 */
export { LoginComponent } from './login.component';
export { ForgotPassword } from './forgot-password.component';
export { ResetPassword } from './reset-password.component';
export { NotAuthorized } from './not-authorized.component';