/**
 * @module AuthComponents
 */
/**
 * 
 */
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { AuthService } from '../auth.service';
import { ValidationService, ControlMessages } from '../shared';

@Component({
    template: `
    <main class="mdl-grid">
        <div class="mdl-cell mdl-cell--4-col"></div>
        <div class="mdl-cell mdl-cell--4-col-desktop mdl-cell--12-col">
            <h1>{{ title }}</h1>
            <form class="auth-form" novalidate [formGroup]="forgotForm" (submit)="onSubmit()" action="forgotPassword">
                <div
                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                    [ngClass]="{'is-invalid': forgotForm.controls.email.touched && forgotForm.controls.email.errors}"
                    >
                    <input formControlName="email" class="mdl-textfield__input" type="text" id="email">
                    <label class="mdl-textfield__label" for="email">Email...</label>
                    <control-messages [control]="forgotForm.controls.email"></control-messages>
                </div>

                <div>
                    <button [disabled]="!forgotForm.valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised">Send me my new password</button>
                </div>

                <div class="additional clearfix">
                    <a [routerLink]="['/login']">Back to login</a>
                </div>
            </form>

            <p *ngIf="responseMsg">{{ responseMsg }}</p>
        </div>
        <div class="mdl-cell mdl-cell--4-col"></div>
    </main>
    `, 
    directives: [ControlMessages], 
    styleUrls: ['./auth-form.style.css']
})
export class ForgotPassword implements OnInit, AfterViewInit {
    title: string = 'Forgot Password';
    forgotForm: FormGroup;
    responseMsg: any;

    constructor(
        public titleService: Title, 
        private _authService: AuthService, 
        private _formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.forgotForm = this._formBuilder.group({
            email: ['', [Validators.required, ValidationService.emailValidator]]
        });
        this.titleService.setTitle('Forgot Password - Remax Intranet');
    }
    ngAfterViewInit() {
        componentHandler.upgradeDom();
    }

    onSubmit() {
        this.responseMsg = '';
        this._authService.forgotPassword(this.forgotForm.value)
            .subscribe(
                r => {
                    console.log(r);
                    this.responseMsg = r.message;
                    //location.reload();
                }, 
                e => this.responseMsg = e
            );
    }

}