/**
 * @module AuthComponents
 */
/**
 * 
 */
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { ValidationService, ControlMessages } from '../shared';

import { AuthService } from '../auth.service';

import { wp_data, Config } from '../../../wp.config';

@Component({
    template: `
    <main class="mdl-grid" *ngIf="storageEmpty">
        <div class="mdl-cell mdl-cell--4-col"></div>
        <div class="form-holder mdl-cell mdl-cell--4-col-desktop mdl-cell--12-col">
            <figure class="remax-balloon">
                <img [src]="remaxBalloon" alt="Remax">
            </figure>
            <h1>{{ title }}</h1>
            <form class="auth-form" novalidate [formGroup]="loginForm" (submit)="onSubmit()" action="login">
                <div
                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
                    [ngClass]="{'is-invalid': loginForm.controls.email.touched && loginForm.controls.email.errors}"
                    >
                    <input formControlName="email" class="mdl-textfield__input" type="text" id="email">
                    <label class="mdl-textfield__label" for="email">Username or Email...</label>
                    <control-messages [control]="loginForm.controls.email"></control-messages>
                </div>

                <div 
                    class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label form-group" 
                    [ngClass]="{'is-invalid': loginForm.controls.password.touched && loginForm.controls.password.errors}"
                    >
                    <input type="password" formControlName="password" class="mdl-textfield__input" id="password">
                    <label class="mdl-textfield__label" for="password">Password...</label>
                    <control-messages [control]="loginForm.controls.password"></control-messages>
                </div>

                <div class="additional clearfix">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-2">
                        <input formControlName="rememberme" type="checkbox" id="checkbox-2" class="mdl-checkbox__input">
                        <span class="mdl-checkbox__label">Remember Me</span>
                    </label>

                    <a [routerLink]=" ['/forgot-password'] ">Lost your password?</a>
                </div>

                <div>
                    <button [disabled]="!loginForm.valid" type="submit" class="mdl-button mdl-js-button mdl-button--raised">Login</button>
                </div>
            </form>

            <p *ngIf="responseMsg">{{ responseMsg }}</p>

        </div>
        <div class="mdl-cell mdl-cell--4-col"></div>
    </main>
    `, 
    directives: [ControlMessages], 
    styleUrls: ['./auth-form.style.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
    wp: Config = Object.assign({}, wp_data);
    title: string = 'Login';
    loginForm: FormGroup; 
    responseMsg: string; 
    storageEmpty: boolean = localStorage.length === 0;
    remaxBalloon: string = `${wp_data.template_directory}assets/img/remax-balloon-gray.png`;

    constructor(
        private _authService: AuthService, 
        private _formBuilder: FormBuilder, 
        public titleService: Title
    ) {}

    ngOnInit() {
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required]], 
            password: ['', [Validators.required]], 
            rememberme: [true]
        });
        this.titleService.setTitle('Login - Remax Intranet');
    }

    ngAfterViewInit() {
        componentHandler.upgradeDom();
    }

    onSubmit() {
        this.responseMsg = '';
        this._authService.login(this.loginForm.value)
            .subscribe(
                (r: any) => {
                    console.log(r);
                    this.responseMsg = r.message;
                    location.assign(this.wp.site_url);
                }, 
                e => this.responseMsg = e
            );
    }
}