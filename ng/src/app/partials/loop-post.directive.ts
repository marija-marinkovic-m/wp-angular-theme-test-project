/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { PostMeta } from './post-meta.component';
import { wp_data, Config, Taxonomy, ANIM_SPEED } from '../../wp.config';
import { Post } from '../components/posts-pages/page.models';

@Component({
    selector: 'loop-post', 
    template: `
    <article *ngIf="post">

        <!-- POST LINKS START -->
        <a routerLink="/{{post.slug}}">
            <figure *ngIf="post.hasOwnProperty('featured_src')" [style.backgroundImage]="'url('+ post.featured_src +')'">
                <i [class]="'' === post.featured_src ? watermark : ''" aria-hidden="true"></i>
                <img [src]="imgPlaceholder">
            </figure>
        </a>
        <a routerLink="/{{post.slug}}" class="post-title">
            <h5 *ngIf="post.title">
                {{post.title.rendered}}
            </h5>
        </a>
        <!-- POST END -->

        <post-meta [post]="post"></post-meta>
        <div class="post-excerpt" *ngIf="post.excerpt" [innerHtml]="post.excerpt.rendered">
        </div>
        <div class="footer" *ngIf="post.slug">
            <a routerLink="/{{post.slug}}"><p>Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></p></a>
        </div>
    </article>
    `, 
    styles: [
        `
        article {margin: 15px;}
        figure {
            position: relative;
            background-color: #eee; background-size: cover; background-repeat: no-repeat; background-position: 50% 50%;
            }
            figure img {display: block; width: 100%; height: auto;}
            figure i {
                position: absolute; top: 50%; left: 0; right: 0; 
                display: block; 
                margin-top: -22.5px;
                text-align: center; font-size: 45px; color: #cfcfcf;
            }
        a figure, a figure i {transition: all 300ms ease;}
        a:hover figure {
            background-color: #dfdfdf;
            -webkit-filter: hue-rotate(40deg);
            filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' ><filter id='huerotate'><feColorMatrix type='hueRotate' values='40' /></filter></svg>#huerotate");
            filter:hue-rotate(40deg);
            }
            a:hover figure i {color: #fff;}
        .footer a {color: #4d4d4d; font-size: 80%;}
        .footer i {color: #ce0000;}
        `
    ], 
    directives: [PostMeta],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoopPostDirective {
    @Input() post: Post;
    @Input() watermark: string = 'fa fa-newspaper-o';
    imgPlaceholder: string = `${wp_data.template_directory}assets/img/img-placeholder.png`;
    wp: Config;
    constructor() {
        this.wp = Object.assign({}, wp_data);
    }

    getCategoriesData(post: Post): Array<Taxonomy> {
        return this.wp.categories
            .filter(c => post.categories.indexOf(c.term_id) > -1);
    }
    authorPostSlug(post: Post): string {
        let author = this.wp.authors.filter(a => a.ID == post.author);
        return author.length ? author[0].user_login : '';
    }

}