/**
 * @module Partials
 */
/**
 * 
 */
import { Component, OnInit } from '@angular/core';
import { wp_data, Taxonomy } from '../../wp.config';

@Component({
    selector: 'news-sidebar', 
    template: `
        <template [ngIf]="cats">
            <h4>Categories</h4>
            <ul>
                <li *ngFor="let cat of cats">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a routerLink="/category/{{cat.slug}}" routerLinkActive="active-link">{{ cat.name }}</a>
                </li>
            </ul>
        </template>

        <template [ngIf]="tags">
            <h4>Tags</h4>
            <ul class="tag-cloud">
                <li *ngFor="let tag of tags">
                    <i class="fa fa-tag" aria-hidden="true"></i>
                    <a routerLink="/tag/{{tag.slug}}" routerLinkActive="active-link">{{ tag.name }}</a>
                </li>
            </ul>
        </template>
    `, 
    styleUrls: ['./sidebar.style.css']
})
export class NewsSidebarDirective implements OnInit {

    cats: Array<Taxonomy>;
    tags: Array<Taxonomy>;

    constructor() {}

    ngOnInit() {
        if (wp_data.hasOwnProperty('categories') && wp_data.categories.length) {
            this.cats = wp_data.categories;
        }
        if (wp_data.hasOwnProperty('tags') && wp_data.tags.length) {
            this.tags = wp_data.tags;
        }
    }
}