/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core'; 
import { wp_data, Config, Taxonomy } from '../../wp.config';  
import { Post } from '../components/posts-pages/page.models';

@Component({
    selector: 'post-meta', 
    template: `
    <h5 *ngIf="post">
        <small>
            <template [ngIf]="post.categories">Posted in 
                <span *ngFor="let cat of getCategoriesData(post)">
                    <a [routerLink]="['/category/' + cat.slug]">{{ cat.name }}</a>, 
                </span>
            </template>
            {{ post.date | date }}, by <a [routerLink]="['/author/' + authorPostSlug(post)]">{{ post.author_nicename }}</a>
        </small>
    </h5>
    `, 
    styles: [`a {color:inherit; text-decoration: underline;}`], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostMeta {
    @Input() post: Post = null;
    wp: Config; 
    constructor() {
        this.wp = Object.assign({}, wp_data);
    }

    getCategoriesData(post: Post): Array<Taxonomy> {
        return this.wp.categories
            .filter(c => post.categories.indexOf(c.term_id) > -1);
    }

    authorPostSlug(post: Post): string {
        let author = this.wp.authors.filter(a => a.ID == post.author);
        return author.length ? author[0].user_nicename : '';
    }
}