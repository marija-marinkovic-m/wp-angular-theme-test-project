/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { wp_data, Config } from '../../wp.config';

@Component({
    selector: 'knowledge-sidebar', 
    template: `
    <template [ngIf]="posts && posts.length">
        <h4>From this topic</h4>
        <ul>
            <li *ngFor="let post of posts">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <a [routerLink]="['/' + post.slug]">{{ post.title.rendered }}</a>
            </li>
        </ul>
    </template>
    `, 
    styleUrls: ['./sidebar.style.css'], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class KnowledgeSidebar {
    @Input() posts: any[];
    wp: Config; 
    constructor() {
        this.wp = Object.assign({}, wp_data);
    }
}