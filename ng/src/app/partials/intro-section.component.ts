/**
 * @module Partials
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { AppState } from '../app.service';

@Component({
    selector: 'intro-section', 
    template: `
    <div class="wrap" *ngIf="app.state.hasOwnProperty('routeInfo') && app.get('routeInfo')">
        <figure *ngIf="app.get('routeInfo').avatar"><img [src]="app.get('routeInfo').avatar"></figure>
        <i *ngIf="app.get('routeInfo').icon" [class]="app.get('routeInfo').icon"></i>
        <h1>{{ app.get('routeInfo').title }}</h1>
        <p *ngIf="app.get('routeInfo').desc" [innerHtml]="app.get('routeInfo').desc"></p>
    </div>
    `, 
    styles: [`
        :host {
            display: block;
            background: white;
            overflow:hidden;
            }
            .wrap {max-width: 1400px; padding: 25px 45px 30px;}
            i {
                display: inline-block; vertical-align: middle; 
                width: 28px; 
                margin: 0 20px -4px 0; 
                font-size: 32px; line-height: 40px; color: #ce0000;
            }
            h1 {
                display: inline-block; vertical-align: middle; 
                margin: 0; 
                font-size: 34px; font-weight: 200; line-height: 40px;
            }
            p {padding-left: 55px; margin-top: 20px;}
            figure {
                float: left; margin: 0 20px 20px 0; 
                overflow: hidden; 
                border-radius: 50%;
                }
                figure img {display: block;}
    `]
})
export class IntroSection {
    constructor(
        public app: AppState
    ) {}
}