/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'profile-sidebar', 
    template: `
        <template [ngIf]="contact && ((contact.phone && contact.phone.length && contact.phone[0] !== '') || (contact.email && contact.email.length && contact.email[0] !== ''))">
            <h4>Contact info</h4>
            <ul class="contact-links">
                <li *ngFor="let phone of contact.phone">
                    <a *ngIf="phone !== ''" [href]="'tel:' + phone"><i class="fa fa-phone" aria-hidden="true"></i> {{ phone }}</a>
                </li>
                <li *ngFor="let email of contact.email">
                    <a *ngIf="email !== ''" [href]="'mailto:' + email"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{ email }}</a>
                </li>
            </ul>
        </template>

        <template [ngIf]="social && (social.facebook || social.twitter || social.linkedin)">
            <h5>Social Networks: </h5>
            <div class="social-links">
                <a *ngIf="social.linkedin" [href]="social.linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <a *ngIf="social.facebook" [href]="social.facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a *ngIf="social.twitter" [href]="social.twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </template>

        <template [ngIf]="courses">
            <h4>Courses attended</h4>
            <ul class="courses-links">
                <li *ngFor="let course of courses">
                    <template [ngIf]="!course.doc_url">
                        <a [routerLink]="['/' + course.slug]">
                            <strong>{{ course.title.rendered }}</strong>
                            <small *ngIf="course.time">{{ course.time.start }}</small>
                        </a>
                    </template>
                    <template [ngIf]="course.doc_url">
                        <a [href]="course.doc_url" download>
                            <strong><i class="fa fa-cloud-download" aria-hidden="true"></i> {{ course.title.rendered }}</strong>
                        </a>
                    </template>
                </li>
            </ul>
        </template>
    `, 
    styleUrls: ['./sidebar.style.css', './sidebar-profile.style.css'], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileSidebar {

    @Input() contact: {
        phone: Array<string>, 
        email: Array<string>
    };
    @Input() social: {
        facebook: string, 
        twitter: string, 
        linkedin: string
    };
    @Input() courses: Array<{
        title: {
            rendered: string
        }, 
        slug: string, 
        time?: {
            all_day: boolean, 
            start: string, 
            end: string
        }, 
        doc_url?: string
    }>;

    constructor() {}
}