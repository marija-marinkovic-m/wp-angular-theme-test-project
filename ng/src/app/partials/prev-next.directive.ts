/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

interface Link {
    type: string; 
    page: number;
    path: Array<string>;
}

@Component({
    selector: 'prev-next', 
    template: `
        <template [ngIf]="links">
            <a *ngFor="let link of links" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" [ngClass]="link.type" [routerLink]=" generateLink(link) " [innerHtml]="formatLabel(link)"></a>
        </template>
    `,
    styles: [
        `
        :host {
            display: block;
            overflow: hidden; clear: both;
            margin: 30px 0;
        }
        .prev {float: left;}
        .next {float: right;}
        `
    ], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrevNextDirective {
    @Input() links: Array<Link>;

    constructor() {}

    formatLabel(link: Link) {
        return link.type === 'prev' ? `<i class="material-icons">skip_previous</i> ${link.type}` : `${link.type} <i class="material-icons">skip_next</i>`;
    }
    generateLink(link: Link): Array<any> {
        return [...link.path, {page: link.page}];
    }
}