/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem } from '../../wp.config';

@Component({
    selector: 'menu-item-link', 
    template: `
    <template [ngIf]="child || item.item_parent == '0'">
    <template [ngIf]="item.link_type == 'Category'">
        <a [routerLink]=" ['./category/' + item.slug] " class="mdl-navigation__link" routerLinkActive="active-link">
            <i *ngIf="item.class" class="material-icons" role="presentation">{{ item.class }}</i>{{ item.title }}
        </a>
    </template>
    <template [ngIf]="item.link_type == 'Custom Link'">
        <a target="_blank" href="{{item.url}}" class="mdl-navigation__link" routerLinkActive="active-link">
            <i *ngIf="item.class" class="material-icons" role="presentation">{{ item.class }}</i>{{ item.title }}
        </a>
    </template>
    <template [ngIf]="item.link_type == 'Page' || item.link_type == 'Post'">
        <a [routerLink]=" ['./' + item.slug] " class="mdl-navigation__link" routerLinkActive="active-link">
            <i *ngIf="item.class" class="material-icons" [ngClass]="item.class" role="presentation"></i>{{ item.title }}
        </a>
    </template>
    </template>
    `, 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuLinkItem {
    @Input() item: MenuItem;
    @Input() child: boolean = false;
    constructor() {}
}