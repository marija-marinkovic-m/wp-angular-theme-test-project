/**
 * @module Partials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { wp_data, Config, Taxonomy } from '../../wp.config';
import { Post } from '../components/posts-pages/page.models';

import { StripHtmlTagsPipe } from '../components/trainings/shared';

@Component({
    selector: 'loop-marketing', 
    template: `
    <article *ngIf="post">

        <a [href]="post.doc_url" download>
            <figure *ngIf="post.hasOwnProperty('featured_src')" [style.backgroundImage]="'url('+ post.featured_src +')'">
                <i [class]="'' === post.featured_src ? watermark : ''" aria-hidden="true"></i>
                <img [src]="imgPlaceholder">
            </figure>
        </a>
        <a [href]="post.doc_url" class="post-title" download>
            <h5 *ngIf="post.title">
                {{post.title.rendered}}
            </h5>
        </a>

        <div class="post-excerpt">
            <p>{{ post.excerpt.rendered | stripHtmlTags }}</p>
        </div>

    </article>
    `, 
    styles: [
        `
        article {margin: 15px;}
        figure {
            position: relative;
            background-color: #eee; background-size: cover; background-repeat: no-repeat; background-position: 50% 50%;
            }
            figure img {display: block; width: 100%; height: auto;}
            figure i {
                position: absolute; top: 50%; left: 0; right: 0; 
                display: block; 
                margin-top: -22.5px;
                text-align: center; font-size: 45px; color: #cfcfcf;
            }
        a figure, a figure i {transition: all 300ms ease;}
        a:hover figure {
            background-color: #dfdfdf;
            -webkit-filter: hue-rotate(40deg);
            filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' ><filter id='huerotate'><feColorMatrix type='hueRotate' values='40' /></filter></svg>#huerotate");
            filter:hue-rotate(40deg);
            }
            a:hover figure i {color: #fff;}
        `
    ], 
    pipes: [StripHtmlTagsPipe], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoopMarketingDirective {
    @Input() post: Post;
    @Input() watermark: string = 'fa fa-newspaper-o';
    imgPlaceholder: string = `${wp_data.template_directory}assets/img/img-placeholder.png`;
    wp: Config;
    constructor() {
        this.wp = Object.assign({}, wp_data);
    }

}