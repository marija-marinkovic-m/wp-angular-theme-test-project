/**
 * @module AppComponent
 *
 */
/**
 * 
 */
import { RouterConfig } from '@angular/router';

import { LoginComponent, AuthGuard, NotAuthorized } from './auth';

import { 
  NotFoundComponent, 
  PageComponent, PageGuard, BlogComponent, 
  MarketingComponent, MarketingComponentList, 
  Trainings, TrainingsList, TrainingsDetail, TrainingsGuard, TrainingEventsArchive, 
  Knowledge, KnowledgeList, KnowledgeDetail, KnowledgeGuard, 
  EditProfileGuard, ConfirmDeactivateProfileEditorGuard
} from './components';

import { Home } from './home';

export const routes: RouterConfig = [
	{ path: '',      redirectTo: 'home', pathMatch: 'full' },
	{ path: 'home',  component: Home, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent }, 
	{ path: 'forgot-password', component: 'ForgotPassword' }, 
	{ path: 'reset-password', component: 'ResetPassword', canActivate: [AuthGuard] }, 
	{ path: 'not_found', component: NotFoundComponent, canActivate: [AuthGuard] }, 
	{ path: 'not_authorized', component: NotAuthorized, canActivate: [AuthGuard] }, 

	{ path: 'agent-rankings', component: 'Agents', canActivate: [AuthGuard] }, 
	{ path: 'agents', redirectTo: 'agent-rankings', pathMatch: 'full' },
	{ path: 'profile/:slug', component: 'AgentProfile', canActivate: [AuthGuard] }, 
	{ path: 'profile/:slug/edit', component: 'ProfileEditor', canActivate: [AuthGuard, EditProfileGuard], canDeactivate: [ConfirmDeactivateProfileEditorGuard] }, 

	// training routes 
	{
		path: 'trainings', 
		component: Trainings, 
		canActivate: [AuthGuard], 
		children: [
			{ path: 'events-archive', component: TrainingEventsArchive }, 
			{ path: ':slug', component: TrainingsDetail, canActivate: [TrainingsGuard] }, 
			{ path: '', component: TrainingsList }
		]
	}, 
	{ path: 'training-type/:slug', redirectTo: 'trainings', pathMatch: 'full' }, 

	// marketing routes 
	{ 
		path: 'marketing', 
		component: MarketingComponent,  
		canActivate: [AuthGuard], 
		children: [
			{ path: ':slug', redirectTo: '', pathMatch: 'full' }, 
			{ path: '', component: MarketingComponentList }
		]
	}, 
	{ path: 'marketing-category/:slug', redirectTo: 'marketing', pathMatch: 'full' }, 
	{ path: 'marketing-guidelines/:slug', redirectTo: 'marketing', pathMatch: 'full' }, 


	// knowledge base routes 
	{
		path: 'knowledge-base', 
		component: Knowledge, 
		canActivate: [AuthGuard], 
		children: [
			{ path: ':slug', component: KnowledgeDetail, canActivate: [KnowledgeGuard] }, 
			{ path: '', component: KnowledgeList }
		]
	}, 
	{ path: 'knowledge_topic/:slug', redirectTo: 'knowledge-base', pathMatch: 'full' }, 


	// blog/post/page routes   
	{ path: 'news', component: BlogComponent, canActivate: [AuthGuard] }, 
	{ path: 'category/:slug', component: BlogComponent, data: {path: 'category/:slug'}, canActivate: [AuthGuard] }, 
	{ path: 'tag/:slug', component: BlogComponent, data: {path: 'tag/:slug'}, canActivate: [AuthGuard] }, 
	{ path: 'author/:slug', component: BlogComponent, data: {path: 'author/:slug'}, canActivate: [AuthGuard] }, 
	{ path: ':slug', component: PageComponent, canActivate: [AuthGuard, PageGuard] },

	// other
	{ path: '**', component: NotFoundComponent }
];

// Async load a component using Webpack's require with es6-promise-loader and webpack `require`
// asyncRoutes is needed for our @angularclass/webpack-toolkit that will allow us to resolve
// the component correctly
export const asyncRoutes: AsyncRoutes = {
	'Agents': require('es6-promise-loader!./agents'), 
	'AgentProfile': require('es6-promise-loader!./agents'), 
	'ForgotPassword': require('es6-promise-loader!./auth'), 
	'ResetPassword': require('es6-promise-loader!./auth'), 
	'ProfileEditor': require('es6-promise-loader!./components')
};


// Optimizations for initial loads
// An array of callbacks to be invoked after bootstrap to prefetch async routes
export const prefetchRouteCallbacks: Array<IdleCallbacks> = [
	asyncRoutes['Agents'], // es6-promise-loader returns a function
	asyncRoutes['AgentProfile'], 
	asyncRoutes['ForgotPassword'], 
	asyncRoutes['ResetPassword'], 
	asyncRoutes['ProfileEditor']
];


// Es6PromiseLoader and AsyncRoutes interfaces are defined in custom-typings
