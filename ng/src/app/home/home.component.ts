/**
 * @module DashboardLanding
 */
/**
 * 
 */
import { Component, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

import { HomeService, Widgets } from './home.service';
import { wp_data } from '../../wp.config';
import { AppState } from '../app.service';

import { LoopPostDirective } from '../partials';
import { EventTraining } from '../components/trainings/partials';

import { AgentRanking, AgentsData, AwardPipe } from '../agents';

@Component({
	providers: [
		HomeService
	],
	directives: [LoopPostDirective, EventTraining], 
	styleUrls: [ './home.style.css' ],
	templateUrl: './home.template.html', 
	encapsulation: ViewEncapsulation.None,
	pipes: [AwardPipe]
})
export class Home implements AfterViewInit {
	data: Widgets;
	currentNewsTab: string = '';

	rankingTypes: Array<string> = ['sales', 'letting'];
	agentRankings: Array<{
		slug: string; 
		agents: Observable<AgentRanking[]>
	}> = [];

	constructor(
		public appState: AppState, 
		private _title: Title, 
		private _dataService: HomeService, 
		private _router: Router, 
		private _activatedRoute: ActivatedRoute, 
		private _agentsData: AgentsData
	) {}

	ngOnInit() {
		this.appState.set('routeInfo', null);
		this._title.setTitle(wp_data.site_title);

		this._dataService.all().subscribe(
			(response:any) => {
				this.data = response;
				this.currentNewsTab = response.news[0].category_id;
			}, 
			e => this.exitRoute(e)
		);

		this._activatedRoute.params.subscribe(param => {
				if (typeof param['news-tab'] !== "undefined") {
					this.currentNewsTab = param['news-tab'];
				}
			}, 
			e => this.exitRoute(e)
		);

		// agent rankings tables 
		this.rankingTypes.forEach(type => {
			this.agentRankings.push({slug: type, agents: this._agentsData.topRankings(10, type)});
		});
	}

	ngAfterViewInit() {
		componentHandler.upgradeDom();
	}

	exitRoute(msg:string) {
		this._router.navigate(['/not_found']);
		return Observable.throw(msg);
	}

}
