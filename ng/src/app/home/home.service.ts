/**
 * @module DashboardLanding
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Post } from '../components/posts-pages/page.models';
import { wp_data, TaxonomyGroup } from '../../wp.config';

export interface Widgets {
    welcome: {
        img: string, 
        desc: string
    }, 
    news: Array<TaxonomyGroup>, 
    trainings: Array<Post>
}

@Injectable() 
export class HomeService {
    private mainUrl = wp_data.api_root + 'remax/v1/home';

    constructor(
        private _http: Http
    ) {}

    all(): Observable<Response> {
        return this._http.get(this.mainUrl).map(r => r.json());
    }
}