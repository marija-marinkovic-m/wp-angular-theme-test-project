/**
 * @module Common Modules
 */
/**
 * 
 */
/**
 * Inspired by PRIMENG http://www.primefaces.org/primeng/#/editor
 */
import { Component } from '@angular/core';

@Component({
    selector: 'header',
    template: '<ng-content></ng-content>'
})
export class Header {}

export { DomHandler } from './dom-handler';
export { Editor } from './quill-editor';