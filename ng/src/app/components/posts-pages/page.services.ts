/**
 * @module NewsAndPages
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { wp_data } from '../../../wp.config';

import { AppState } from '../../app.service';


/**
 * Data Sevice for fetching pages 
 */
@Injectable()
export class PageDataService {
    private checkUrl = wp_data.api_root + 'remax/v1/check';
    private stdUrl = wp_data.api_root + 'wp/v2';
    constructor(
        private _http: Http, 
        private _router: Router, 
        public state: AppState
    ) {}
    exists(slug: string):Observable<any> {
        return this._http.get(this.checkUrl + '/' + slug);
    }

    fetch(postID: number, postType: string = 'post', postParams: string = '') {
        let id = postID === null ? '' : '/' + postID;
        return this._http.get(this.stdUrl + '/' + postType + 's' + id + postParams);
    }

    get(link: string) {
        return this._http.get(link)
            .map(res => res.json());
    }
}


/**
 * Page Guard
 */
@Injectable()
export class PageGuard implements CanActivate {

    constructor(
        private _dataService: PageDataService, 
        private _router: Router, 
        private _state: AppState
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot
    ):Observable<boolean> | boolean {
        let currSlug = next.params['slug'];
        return this._dataService.exists(currSlug)
                    .map(res => {
                        let body = res.json();
                        this._state.set("post", {
                            "ID": body.ID, 
                            "post_type": body.post_type
                        });
                        return true;
                    })
                    .catch(() => {
                        this._router.navigate(['/not_found']);
                        return Observable.throw('Page does not exist');
                    });
    }
}

