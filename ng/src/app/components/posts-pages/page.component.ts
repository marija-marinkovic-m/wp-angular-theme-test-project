/**
 * @module NewsAndPages
 */
/**
 * PageComponent
 */
import { Component, OnInit, OnDestroy } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';


import { PageDataService } from './page.services';
import { Post } from './page.models';
import { AppState, routeInfoCollector } from '../../app.service';

import { NewsSidebarDirective, PostMeta } from '../../partials';

@Component({
    templateUrl: './page.template.html', 
    directives: [NewsSidebarDirective, PostMeta]
})
export class PageComponent implements OnInit, OnDestroy {
    slug: string;
    post: Post;
    
    paramSubscription: Subscription;
    constructor(
        private _route: ActivatedRoute, 
        private _appState: AppState, 
        private _dataService: PageDataService, 
        private _router: Router, 
        private _titleService: Title
    ) { }

    ngOnInit() {
        this.paramSubscription = this._route.params.subscribe(
            (res) => {
                this.slug = res['slug'];
                if (
                    this._appState.state.hasOwnProperty('post')
                    && this._appState.get('post').hasOwnProperty('ID') 
                ) {
                    let postObj = this._appState.get('post'), 
                        postType = postObj.hasOwnProperty('post_type') ? postObj.post_type : 'post';
                    this._dataService.fetch(postObj.ID, postType)
                        .subscribe(
                            res => {
                                this.post = res.json();
                                this._titleService.setTitle(this.post.title.rendered + ' - News');

                                if (this.post.type === "post") {
                                    this._appState.set('routeInfo', routeInfoCollector('News'));
                                } else {
                                    this._appState.set('routeInfo', {title: this.post.title.rendered});
                                }
                            }, 
                            err => this.exitRoute(err)
                        );
                } else {
                    this.exitRoute("State post not provided.");
                }
            }, 
            (err) => {console.error(err)}
        );
    }

    ngOnDestroy() {
        this.paramSubscription.unsubscribe();
    }

    exitRoute(msg:string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }

}