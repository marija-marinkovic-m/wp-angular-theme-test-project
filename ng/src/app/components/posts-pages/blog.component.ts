/**
 * @module NewsAndPages
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/throw';

import { AppState, routeInfoCollector } from '../../app.service';
import { PageDataService } from './page.services';
import { Post } from './page.models';

import { Title } from '@angular/platform-browser';

// directives 
import { LoopPostDirective, NewsSidebarDirective, PrevNextDirective } from '../../partials';

import { wp_data, Config } from '../../../wp.config';

@Component({
    templateUrl: './blog.template.html', 
    directives: [LoopPostDirective, NewsSidebarDirective, PrevNextDirective], 
    styleUrls: ['./blog.style.css'] 
})
export class BlogComponent implements OnInit, OnDestroy {
    posts: Array<Observable<Post>>;
    indexSubscription: Subscription;
    prevNextLinks: Array<any>;
    currPage: number;
    slug: string;

    wp: Config;

    constructor(
        private _dataService: PageDataService,
        private _router: Router, 
        private _route: ActivatedRoute, 
        private _title: Title, 
        private _appState: AppState
    ) {
        this.wp = Object.assign({}, wp_data);
    }

    ngOnInit() {
        let perPage = this.wp.posts_per_page, 
            path = this._route.snapshot.data['path'], 
            tax = '', 
            currRoutePath = ['../news'], 
            routeInfoDesc = '';

        this._route.params.subscribe(r => {
            this.currPage = +r['page'] || 1;
            this.slug = r['slug'] || '';

            switch (path) {
                case 'category/:slug':
                    let currCat = this.wp.categories.filter(c => c.slug === this.slug); 
                    tax = currCat.length ? `&categories=${currCat[0].term_id}` : '';
                    currRoutePath = ['../../category', this.slug];
                    routeInfoDesc = `Posted in <em>${currCat[0].name}</em> category`;

                    this._title.setTitle('News - ' + currCat[0].name);
                    break;
                case 'tag/:slug': 
                    let currTag = this.wp.tags.filter(t => t.slug === this.slug);
                    tax = currTag.length ? `&tags=${currTag[0].term_id}` : '';
                    currRoutePath = ['../../tag', this.slug];
                    routeInfoDesc = `Tagged under <em>'${currTag[0].name}'</em>`;

                    this._title.setTitle('News - ' + currTag[0].name);
                    break;
                case 'author/:slug': 
                    let currAuthor = this.wp.authors.filter(a => a.user_nicename === this.slug);
                    tax = currAuthor.length ? `&user=${currAuthor[0].ID}` : '';
                    currRoutePath = ['../../author', this.slug];
                    routeInfoDesc = `Posts by <strong>${currAuthor[0].display_name}</strong>`;

                    this._title.setTitle('Author - ' + currAuthor[0].display_name);
                    break;

                default: 
                    this._title.setTitle('News');
            }

            this._appState.set('routeInfo', routeInfoCollector('News', routeInfoDesc));

            this.indexSubscription = this._dataService.fetch(
                null, 
                'post', 
                `?per_page=${this.wp.posts_per_page}&page=${this.currPage}${tax}`
                )
                    .map((items) => {
                        let headerTotalPages = items.headers.get('X-WP-TotalPages');
                        this.prevNextLinks = [];
                        if (headerTotalPages) {
                            let total = +headerTotalPages, 
                                prev = this.currPage - 1, 
                                next = this.currPage + 1;
                            
                            if (this.currPage > 1) {
                                this.prevNextLinks.push({page: prev, type: 'prev', path: currRoutePath});
                            }
                            if (next <= total) {
                                this.prevNextLinks.push({page: next, type: 'next', path: currRoutePath});
                            }
                        }
                        return items.json();
                    })
                    .subscribe(
                        res => {
                            this.posts = res;
                        }, 
                        err => this.exitRoute(err)
                    );
        });
    }

    ngOnDestroy() {
        this.indexSubscription.unsubscribe();
        this.prevNextLinks = [];
    }

    exitRoute(msg:string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }
}