/**
 * @module NewsAndPages
 */
/**
 * 
 */
export interface Post {
    id: number;
    date: string; 
    modified: string; 
    type: string; 
    link: string;
    title: {
        rendered: string
    };
    content: {
        rendered: string 
    }; 
    categories: number[], 
    tags: number[], 
    excerpt: {
        rendered: string
    };
    _links: {
        "wp:featuredmedia": [
            {
                "embeddable": boolean, 
                "href": string
            }
        ]
    }, 
    author: number
}