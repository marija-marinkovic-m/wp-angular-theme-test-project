/**
 * @module TrainingsShared
 */
/**
 * 
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'localizeMonth'
}) 
export class LocalizeMonth implements PipeTransform {
    transform(date: string):string {
        const locale = "en-us";
        let objDate = new Date(date); 
        return objDate.toLocaleString(locale, {month: 'short'});
    }
}