/**
 * @module TrainingsShared
 */
/**
 * 
 */
export { StripHtmlTagsPipe } from './strip-html.pipe';
export { LocalizeMonth } from './localized-month.pipe';