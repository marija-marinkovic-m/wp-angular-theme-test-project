/**
 * @module TrainingsComponent
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { TrainingsService } from './trainings.service';
import { AppState, routeInfoCollector } from '../../app.service';

import { wp_data, Config, ANIM_SPEED } from '../../../wp.config';

import { PrevNextDirective } from '../../partials';
import { EventTraining } from './partials';

@Component({
    template: `
    <div *ngIf="!posts" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active is-upgraded"></div>
    <main *ngIf="posts" class="mdl-grid">
        <div class="mdl-cell mdl-cell--8-col-desktop mdl-cell--12-col">
            <div class="mdl-tabs is-upgraded mdl-card mdl-shadow--2p">
                <div class="mdl-tabs__tab-bar">
                    <a class="is-active mdl-tabs__tab">Archive</a>
                </div>
                <div class="is-active mdl-tabs__panel mdl-grid">
                    <loop-event *ngFor="let post of posts" [post]="post"></loop-event>
                    <prev-next [links]="prevNextLinks"></prev-next>
                </div>
            </div>
        </div>
    </main>
    `, 
    directives: [PrevNextDirective, EventTraining], 
    styles: [`
        loop-event {
            display: block; overflow: hidden; clear: both; 
            padding: 20px 0; margin: 0 40px 20px;
            border-bottom: 1px solid #eee;
        }
        .mdl-tabs__panel loop-event:last-child {border-bottom: none;}
    `]
})
export class TrainingEventsArchive implements OnInit, OnDestroy {
    wp: Config;
    indexSubscription: Subscription;
    posts: Array<any>;
    prevNextLinks: Array<any>;
    currPage: number;

    constructor(
        private _dataService: TrainingsService, 
        private _router: Router, 
        private _route: ActivatedRoute, 
        private _appState: AppState, 
        public title: Title
    ) {
        this.wp = Object.assign({}, wp_data);
    }

    ngOnInit() {
        const MainTitle = 'Training Events Archive';
        const currRoutePath = ['/trainings/events-archive'];
        this.title.setTitle(MainTitle);
        this._appState.set('routeInfo', routeInfoCollector(MainTitle));

        this._route.params.subscribe(params => {
            this.currPage = +params['page'] || 1;
            this.indexSubscription = this._dataService
                .eventsArchive(`&page=${this.currPage}&per_page=${this.wp.posts_per_page}`)
                .map((items:any) => {
                    let headerTotalPages = items.headers.get('X-WP-TotalPages');
                        this.prevNextLinks = [];
                        if (headerTotalPages) {
                            let total = +headerTotalPages, 
                                prev = this.currPage - 1, 
                                next = this.currPage + 1;
                            
                            if (this.currPage > 1) {
                                this.prevNextLinks.push({page: prev, type: 'prev', path: currRoutePath});
                            }
                            if (next <= total) {
                                this.prevNextLinks.push({page: next, type: 'next', path: currRoutePath});
                            }
                        }
                        return items.json();
                })
                .subscribe(
                    res => {
                        this.posts = res;
                    }, 
                    err => this.exitRoute(err)
                );
        });
    }

    ngOnDestroy() {
        this.indexSubscription.unsubscribe();
        this.prevNextLinks = [];
    }

    exitRoute(msg:string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }
}