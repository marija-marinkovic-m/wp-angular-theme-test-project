/**
 * @module TrainingsComponent
 */
/**
 * Main trainings component
 */
export * from './trainings.component';
export * from './trainings-list.component';
export * from './trainings-detail.component';
export * from './trainings-events-archive.component';
export * from './trainings.service';