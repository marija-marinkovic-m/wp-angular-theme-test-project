/**
 * @module TrainingsComponent
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AppState, routeInfoCollector } from '../../app.service';

@Component({
    template: '<router-outlet></router-outlet>'
})
export class Trainings {
    constructor(
        private _title: Title, 
        private _appState: AppState
    ) {
        const CurrTitle = 'Trainings';
        _title.setTitle(CurrTitle);
        _appState.set('routeInfo', routeInfoCollector(CurrTitle));
    }
}