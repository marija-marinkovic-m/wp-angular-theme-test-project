/**
 * @module TrainingsComponent
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/throw';

import { TrainingsService } from './trainings.service';
import { VideoTraining, DocumentTraining, EventTraining } from './partials';

@Component({
    templateUrl: './trainings-list.template.html', 
    directives: [VideoTraining, EventTraining, DocumentTraining], 
    styles: [`
    .training-events {margin-bottom: 30px;}
    .training-videos {margin-bottom: 30px; width: 100%}
    .training-docs {margin-bottom: 30px; width: 100%}
    `]
})
export class TrainingsList implements OnInit, OnDestroy, AfterViewInit {

    public data: Observable<Object>;
    public sub: Subscription;

    constructor(
        private _dataService: TrainingsService, 
        private _router: Router, 
        private _route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.sub = this._dataService.all()
            .subscribe(
                r => {
                    this.data = r;
                }, 
                e => this.exitRoute(e)
            );
    }

    ngAfterViewInit() {
        componentHandler.upgradeDom(); // upgrade all mdl components
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    
    exitRoute(msg: string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }

}