/**
 * @module TrainingsPartials
 */
/**
 * 
 */
export * from './loop-document.component';
export * from './loop-event.component';
export * from './loop-video.component';
export { DateTag } from './date.component'; 
export { SidebarEvent } from './sidebar-event.component';