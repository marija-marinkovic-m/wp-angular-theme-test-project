/**
 * @module TrainingsPartials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { StripHtmlTagsPipe } from '../shared';

@Component({
    selector: 'loop-video', 
    template: `
    <article *ngIf="post">
        <a [routerLink]="['/' + post.slug]"><figure class="video" [style.backgroundImage]="post.featured_src !== '' ? post.featured_src : null"></figure></a>
        <div class="clearfix">
            <a [routerLink]="['/' + post.slug]"><h5>{{ post.title.rendered }}</h5></a>
            <p>{{ post.excerpt.rendered | stripHtmlTags }}</p>
        </div>
    </article>
    `, 
    styleUrls: ['./loop.style.css'], 
    pipes: [StripHtmlTagsPipe], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoTraining {
    @Input() post: any;
    constructor() {}
}