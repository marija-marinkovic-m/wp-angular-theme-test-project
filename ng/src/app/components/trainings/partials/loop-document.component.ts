/**
 * @module TrainingsPartials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core'; 
import { StripHtmlTagsPipe } from '../shared';

@Component({
    selector: 'loop-document', 
    template: `
    <article *ngIf="post">
        <a [attr.href]="post.doc_url" download><figure class="document" [style.backgroundImage]="post.featured_src !== '' ? 'url(' + post.featured_src + ')' : null"></figure></a>
        <div class="clearfix">
            <a [attr.href]="post.doc_url" download><h5>{{ post.title.rendered }}</h5></a>
            <p>{{ post.excerpt.rendered | stripHtmlTags }}</p>
        </div>
    </article>
    `, 
    styleUrls: ['./loop.style.css'], 
    pipes: [StripHtmlTagsPipe], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentTraining {
    @Input() post: any;
    constructor() {}
}