/**
 * @module TrainingsPartials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core'; 
import { LocalizeMonth } from '../shared';

@Component({
    selector: 'date-tag',
    template: `
    <time [attr.datetime]="date">
        <span class="month">{{ date | localizeMonth }}</span>
        <span class="datum">{{ date | date:"d" }}</span>
    </time>`,
    styles: [`
    :host {display: block;}
    time {
        display: block; width: 80px; height: 80px; 
        border: 1px solid #eee;
    }
    span {
        display: block; 
        border: 1px solid #fff; 
        text-transform: uppercase; text-align: center; font-size: 13px; line-height: 26px;
        }
        span.month {background: #f3f3f3;}
        span.datum {
            font-size: 30px; font-weight: bold; 
            color: #ce0000; line-height: 52px;
        }
    `], 
    pipes: [LocalizeMonth], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateTag {
    @Input() date: string;
    constructor() {}
}