/**
 * @module TrainingsPartials
 */
/**
 * 
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { DateTag } from './date.component';
import { StripHtmlTagsPipe } from '../shared';

@Component({
    selector: 'loop-event', 
    template: `
    <article *ngIf="post">
        <a [routerLink]="['/' + post.slug]"><date-tag [date]="post.time.start"></date-tag></a>
        <div class="clearfix">
            <a [routerLink]="['/' + post.slug]"><h5>{{ post.title.rendered }}</h5></a>
            <h6 *ngIf="post.place.venue !== ''">Venue: {{ post.place.venue }}</h6>
            <p>{{ post.excerpt.rendered | stripHtmlTags }}</p>
        </div>
    </article>
    `, 
    styleUrls: ['./loop.style.css'], 
    directives: [DateTag], 
    pipes: [StripHtmlTagsPipe], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventTraining {
    @Input() post: any;
    constructor() {}
}