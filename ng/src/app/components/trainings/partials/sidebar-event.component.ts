/**
 * @module TrainingsPartials
 */
/**
 * 
 */
import { Component, Input, provide, ChangeDetectionStrategy } from '@angular/core';
import { 
    GOOGLE_MAPS_DIRECTIVES, 
    GOOGLE_MAPS_PROVIDERS, 
    LazyMapsAPILoaderConfig
} from '../shared/angular2-google-maps/core';

interface MapData {
    lng: number;
    lat: number; 
    zoom?: number;
}

@Component({
    selector: 'sidebar-event', 
    template: `
    <h4>Training info</h4>
    <template [ngIf]="time">
        <dl>
            <dt>Training start: </dt>
            <dd><p>{{ time.start | date:format }}</p></dd>
        </dl>
        <dl *ngIf="time.end">
            <dt>Training end: </dt>
            <dd><p>{{ time.end | date:format }}</p></dd>
        </dl>
    </template>
    <template [ngIf]="place">
        <dl *ngIf="place.venue !== ''">
            <dt>Venue: </dt>
            <dd class="preformated"><p>{{ place.venue }}</p></dd>
        </dl>
        <dl *ngIf="place.map">
            <dt>Location: </dt>
            <dd><sebm-google-map [latitude]="place.map.lat" [longitude]="place.map.lng" [zoom]="place.map.zoom">
                <sebm-google-map-marker [latitude]="place.map.lat" [longitude]="place.map.lng"></sebm-google-map-marker>
            </sebm-google-map></dd>
        </dl>
    </template>
    `, 
    directives: [GOOGLE_MAPS_DIRECTIVES], 
    providers: [GOOGLE_MAPS_PROVIDERS, provide(LazyMapsAPILoaderConfig, {useFactory: () => {
        let config = new LazyMapsAPILoaderConfig();
        config.apiKey = 'AIzaSyC-Z70EIvkN0wCTpUNtak3jhkgY6pM8Y2g';
        return config;
    }})], 
    styles: [`
        sebm-google-map {display: block; width: 100%; height: 300px}
        h4 {
            padding-bottom: 6px;
            font-size: 26px; font-weight: 300; color: #222;
            border-bottom: 1px solid #cfcfcf;
        }
        dl {
            margin-bottom: 26px;
            }
            dl dt {
                font-weight: bold; font-size: 15px; line-height: 26px;
            }
            dl dd {
                margin: 0; padding: 0;
            }
    `], 
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarEvent {
    @Input() time: {all_day: boolean, start: Date, end?: Date};
    @Input() place: {venue: string, map?: MapData}
    constructor() {}
    get format() {
        return this.time.all_day ? "longDate" : "medium";
    }
}