/**
 * @module TrainingsComponent
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { wp_data } from '../../../wp.config';
import { AppState } from '../../app.service';
import { Post } from '../posts-pages/page.models';

/**
 * Get first trainings and store in AppState obj
 */
@Injectable()
export class TrainingsService {
    private customRoot = wp_data.api_root + 'remax/v1';
    private checkUrl = this.customRoot + '/check';
    private mainUrl = this.customRoot + '/trainings';
    private singlePostUrl = this.customRoot + '/training';
    private eventArchiveUrl = this.mainUrl + '?events-archive';

    constructor(
        private _http: Http, 
        private _appState: AppState
    ) {}

    exists(slug: string):Observable<Response> {
        return this._http.get(`${this.checkUrl}/${slug}?type=trainings`);
    }

    all(): Observable<any> {
        // check if object exists in AppState
        // if it doesn't get it via http and store
        const currState = this._appState.state;
        if (currState.hasOwnProperty('trainings')) {
            console.info('`trainings` founded in appState');
            return new Observable((sub) => {
                sub.next(this._appState.get('trainings'));
                sub.complete();
            });
        } else {
            return this._http.get(this.mainUrl)
                .map(res => {
                    let body = res.json();
                    this._appState.set('trainings', body);
                    return body;
                });
        }
    }

    one(id: number): Observable<Post> {
        return this._http.get(this.singlePostUrl + '/' + id)
            .map(res => res.json());
    }

    eventsArchive(params:string):Observable<Response> {
        return this._http.get(this.eventArchiveUrl + params); // no mapping because of need for response 'X-WP-TotalPages' header value
    }
}

/**
 * Trainings Guard 
 */
@Injectable() 
export class TrainingsGuard implements CanActivate {
    constructor(
        private _dataService: TrainingsService, 
        private _router: Router, 
        private _appState: AppState
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot, 
        prev: RouterStateSnapshot
    ) {
        let currSlug = next.params['slug'];
        return this._dataService.exists(currSlug)
            .map(res => {
                let body = res.json();
                this._appState.set('post', {
                    "ID": body.ID, 
                    "post_type": body.post_type
                });
                return true;
            })
            .catch(() => {
                this._router.navigate(['/not_found']);
                return Observable.throw('Page does not exist');
            });
    }
}