/**
 * @module TrainingsComponent
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { AppState } from '../../app.service';
import { TrainingsService } from './trainings.service';

import { DateTag, SidebarEvent } from './partials';

import { DomSanitizationService } from '@angular/platform-browser';
import { Post } from '../posts-pages/page.models';

@Component({
    templateUrl: './trainings-detail.template.html', 
    directives: [DateTag, SidebarEvent], 
    styles: [`
        .title-bar .mdl-tabs__tab {
            display: flex; justify-content: flex-start;
            padding-top: 20px; padding-bottom: 20px;
            }
            .title-bar .mdl-tabs__tab h1, .title-bar .mdl-tabs__tab date-tag {
                align-self: center; margin: 0;
            }
            .title-bar .mdl-tabs__tab date-tag {margin-right: 20px;}
        aside {padding-left: 25px;}
        @media (max-width: 839px) {
            aside {padding: 0;}
        }
    `],
    changeDetection: ChangeDetectionStrategy.Default
})
export class TrainingsDetail implements OnInit, OnDestroy {
    private sub: Subscription;
    private paramSub: Subscription;
    public post: Post;

    constructor(
        private _dataService: TrainingsService, 
        private _router: Router, 
        private _appState: AppState, 
        public _sanitizer: DomSanitizationService, 
        public activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.paramSub = this.activatedRoute.params.subscribe(params => {
            if (this._appState.state.hasOwnProperty('post') && this._appState.state.post.hasOwnProperty('ID')) {
                this.subscribeToDetail();
            } else {
                this.exitRoute('Training single Screen Guard didn\'t produce needed parameters');
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        this.paramSub.unsubscribe();
    }

    exitRoute(msg:string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }

    subscribeToDetail() {
        this.sub = this._dataService.one(+this._appState.state.post.ID)
                    .subscribe(
                        r => {
                            this.post = r;
                            console.log(this.post);
                        }, 
                        e => this.exitRoute(e)
                    );
    }

    get content():any {
        return this._sanitizer.bypassSecurityTrustHtml(this.post.content.rendered);
    }
}