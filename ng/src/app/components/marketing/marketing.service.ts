/**
 * @module Marketing
 */
/**
 * 
 */
import { Injectable } from '@angular/core';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { wp_data } from '../../../wp.config';
import { AppState } from '../../app.service';

/**
 * Get marketing object and store it in AppState Object 
 */
@Injectable()
export class MarketingDataService {
    private checkUrl = wp_data.api_root + 'remax/v1/check';
    private url = wp_data.api_root + 'remax/v1/marketing';
    private oneUrl = wp_data.api_root + 'wp/v2/marketing/';

    constructor(
        private _http: Http, 
        public appState: AppState 
    ) {}

    exists(slug: string): Observable<any> {
        return this._http.get(`${this.checkUrl}/${slug}?type=marketing`);
    }

    get():Observable<any> {
        // Check if object exists on AppState 
        // If it doesn't get it via http and store 
        const currState = this.appState.state;
        if (currState.hasOwnProperty('marketing')) {
            return new Observable(sub => {
                sub.next(this.appState.get('marketing'));
                sub.complete();
            });
        } 

        return this._http.get(this.url)
            .map(res => {
                let data = res.json();
                this.appState.set('marketing', data);
                return data;
            });
    }

    one(id: number):Observable<any> {
        return this._http.get(this.oneUrl + id)
            .map(res => res.json());
    }

}