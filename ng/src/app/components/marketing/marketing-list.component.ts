/**
 * @module Marketing
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/throw';

import { MarketingDataService } from './marketing.service';
import { LoopMarketingDirective } from '../../partials';

import { ANIM_SPEED } from '../../../wp.config';

@Component({
    template: `
    <div *ngIf="!data" class="mdl-spinner mdl-js-spinner is-active is-upgraded"></div>
    <main *ngIf="data" class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2p">
            <div class="mdl-tabs mdl-js-tabs is-upgraded">
                <div class="mdl-tabs__tab-bar">
                    <a *ngFor="let guideline of data" [routerLink]=" ['../marketing', {tab: guideline.term.slug}] " class="mdl-tabs__tab" [ngClass]="currentTab === guideline.term.slug ? 'is-active' : ''">{{ guideline.term.name }}</a>
                </div>

                <div *ngFor="let guideline of data" [attr.id]="guideline.term.slug" [ngClass]="currentTab === guideline.term.slug ? 'is-active' : ''" class="mdl-tabs__panel mdl-grid">
                    <div class="mdl-grid">
                        <div *ngFor="let group of guideline.groups" class="mdl-cell mdl-cell--12-col clearfix">
                            <h2>{{ group.category_name }}</h2>

                            <section class="mdl-grid">
                                <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet" *ngFor="let post of group.posts">
                                    <loop-marketing [post]="post" watermark="fa fa-cloud-download"></loop-marketing>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    `, 
    directives: [LoopMarketingDirective], 
    styles: [`
        h2 {margin: 40px 30px 0; font-size: 26px; color: #222; font-weight: 100;}
    `]
})
export class MarketingComponentList implements OnInit, OnDestroy, AfterViewInit {

    public data: Observable<Object>;
    public sub: Subscription;
    public currentTab: string = '';
    constructor(
        private _dataService: MarketingDataService, 
        private _router: Router, 
        private _route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.sub = this._dataService.get()
            .subscribe(
                r => {
                    this.data = r;
                    this.currentTab = r[0].term.slug;
                }, 
                e => this.exitRoute(e)
            );

        this._router.routerState.parent(this._route).params
            .subscribe(
                d => {
                    if (typeof d['tab'] !== "undefined") {
                        this.currentTab = d['tab'];
                    }
                }, 
                e => this.exitRoute(e)
            );
    }

    ngAfterViewInit() {
        componentHandler.upgradeDom(); // upgrade all mdl components
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    exitRoute(msg: string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }

}