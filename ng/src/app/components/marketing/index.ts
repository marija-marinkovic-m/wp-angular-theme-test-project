/**
 * @module Marketing
 */
/**
 * 
 */
export * from './marketing.component';
export * from './marketing-list.component';
export * from './marketing.service';