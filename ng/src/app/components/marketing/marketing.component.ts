/**
 * @module Marketing
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { AppState, routeInfoCollector } from '../../app.service';
import { Title } from '@angular/platform-browser';

import { PageDataService } from '../posts-pages/page.services';

@Component({
    template: '<router-outlet></router-outlet>'
})
export class MarketingComponent {

    constructor(
        private _title: Title, 
        private _appState: AppState, 
        private _pageDataService: PageDataService
    ) {
        const CurrTitle = 'Marketing';

        _title.setTitle(CurrTitle);
        _appState.set('routeInfo', null);
    }

    ngOnInit() {
        this._pageDataService.exists('marketing')
            .map(r => r.json())
            .subscribe(data => {
                this._appState.set('routeInfo', routeInfoCollector('Marketing', data.post_content));
            });
    }
}