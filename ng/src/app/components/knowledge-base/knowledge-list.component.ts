/**
 * @module Knowledge
 */
/**
 * 
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/throw';

import { KnowledgeService } from './knowledge.service';

@Component({
    template: `
    <div *ngIf="!data" class="mdl-spinner mdl-js-spinner is-active is-upgraded"></div>
    <main *ngIf="data" class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2p mdl-grid">
            <div *ngFor="let group of data" class="topic mdl-cell mdl-cell--6-col">
                <h3>{{ group.category_name }}</h3>
                <ul>
                    <li *ngFor="let post of group.posts">
                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                        <a routerLink="/{{post.slug}}">{{ post.title.rendered }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </main>
    `, 
    styles: [`
    .topic {padding: 0 35px;}
    h3 {
        padding-bottom: 8px;
        font-size: 26px; font-weight: 200; color: #222;
        border-bottom: 1px solid #eee;
    }
    ul {margin: 0 0 40px; padding: 0;}
        ul li {
            list-style: none; 
            position: relative; z-index: 1;
            padding-left: 20px;
            border-bottom: 1px solid #eee;
        }
            ul li i {
                position: absolute; top: 10px; left: 0px; z-index: 2; 
                display: block; color: #ce0000; 
            }
            ul li a {display: block; padding: 5px 0;}
    `]
})
export class KnowledgeList implements OnInit, OnDestroy {
    public data: Observable<Object>;
    public sub: Subscription; 

    constructor(
        private _dataService: KnowledgeService, 
        private _router: Router, 
        private _route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.sub = this._dataService.all()
            .subscribe(
                r => this.data = r, 
                e => this.exitRoute(e)
            );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    exitRoute(msg: string) {
        this._router.navigate(['/not_found']);
        return Observable.throw(msg);
    }

}