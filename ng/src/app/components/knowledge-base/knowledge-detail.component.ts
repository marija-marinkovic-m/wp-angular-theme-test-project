/**
 * @module Knowledge
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AppState } from '../../app.service';
import { KnowledgeService } from './knowledge.service';

import { Post } from '../posts-pages/page.models';
import { KnowledgeSidebar } from '../../partials';

@Component({ 
    template: `
    <div *ngIf="!post" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active is-upgraded"></div>
    <main *ngIf="post" class="mdl-grid">
        <div class="mdl-cell mdl-cell--9-col mdl-card mdl-shadow--2p mdl-tabs is-upgraded">
            <div class="title-bar mdl-tabs__tab-bar">
                <a class="is-active mdl-tabs__tab">
                    <h1>{{ post.title.rendered }}</h1>
                </a>
            </div>
            <section class="main-placeholder content" [innerHtml]="post.content.rendered"></section>
        </div>
        <div class="mdl-cell mdl-cell--3-col">
            <knowledge-sidebar [posts]="topicPosts"></knowledge-sidebar>
        </div>
    </main>
    `,
    directives: [KnowledgeSidebar]
})
export class KnowledgeDetail {
    private sub: Subscription;
    public paramSub: Subscription;
    public post: Post;
    public topicPosts: Array<Post>;

    constructor(
        private _dataService: KnowledgeService, 
        private _router: Router, 
        private _appState: AppState, 
        private _activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.paramSub = this._activatedRoute.params.subscribe(params => {
            if (this._appState.state.hasOwnProperty('post') && this._appState.state.post.hasOwnProperty('ID')) {
                if (!this._appState.state.hasOwnProperty('knowledgeBase')) {
                    this._dataService.all().subscribe(response => {
                        this.subscribeToDetail();
                    });
                } else {
                    this.subscribeToDetail();
                }
            } else {
                this._router.navigate(['/not_found']);
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
        this.paramSub.unsubscribe();
    }

    subscribeToDetail():void {
        this.sub = this._dataService.one(+this._appState.state.post.ID)
                .subscribe(
                    r => {
                        this.topicPosts = this._appState.state.knowledgeBase
                            .filter(topic => r.knowledge_topic.indexOf(topic.category_id) > -1)
                            .map(i => i.posts)
                            .reduce((total, start) => total.concat(start))
                            .filter(post => post.ID !== r.id);
                        this.post = r;
                    }, 
                    e => console.error(e)
                );
    }
}