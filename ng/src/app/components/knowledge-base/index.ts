/**
 * @module Knowledge
 */
/**
 * 
 */
export * from './knowledge.service';
export * from './knowledge.component';
export * from './knowledge-list.component';
export * from './knowledge-detail.component';
