/**
 * @module Knowledge
 */
/**
 * 
 */
import { Injectable } from '@angular/core';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { wp_data } from '../../../wp.config';
import { AppState } from '../../app.service';

/**
 * Get knowledgeBase object and store it in AppState Object 
 */
@Injectable()
export class KnowledgeService {
    private checkUrl = wp_data.api_root + 'remax/v1/check';
    private url = wp_data.api_root + 'remax/v1/knowledge-base';
    private oneUrl = wp_data.api_root + 'wp/v2/knowledge_base/';

    constructor(
        private _http: Http, 
        public appState: AppState 
    ) {}

    exists(slug: string): Observable<any> {
        return this._http.get(`${this.checkUrl}/${slug}?type=knowledge_base`);
    }

    all():Observable<any> {
        // Check if object exists on AppState 
        // If it doesn't get it via http and store 
        const currState = this.appState.state;
        if (currState.hasOwnProperty('knowledgeBase')) {
            console.info('knowledge_base founded in appState');
            return new Observable(sub => {
                sub.next(this.appState.get('knowledgeBase'));
                sub.complete();
            });
        } 

        return this._http.get(this.url)
            .map(res => {
                let data = res.json();
                this.appState.set('knowledgeBase', data);
                return data;
            });
    }

    one(id: number):Observable<any> {
        return this._http.get(this.oneUrl + id)
            .map(res => res.json());
    }

}


/*
* KnowledgeGuard 
*/
@Injectable()
export class KnowledgeGuard implements CanActivate {
    constructor(
        private _dataService: KnowledgeService, 
        private _router: Router, 
        private _state: AppState
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot, 
        prev: RouterStateSnapshot
    ):Observable<boolean> | boolean {
        let currSlug = next.params['slug'];
        return this._dataService.exists(currSlug)
            .map(res => {
                let body = res.json();
                this._state.set("post", {
                    "ID": body.ID, 
                    "post_type": body.post_type
                });
                return true;
            })
            .catch(() => {
                this._router.navigate(['/not_found']);
                return Observable.throw('Page does not exist');
            });
    }
}