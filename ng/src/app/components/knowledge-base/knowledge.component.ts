/**
 * @module Knowledge
 */
/**
 * 
 */
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AppState, routeInfoCollector } from '../../app.service';

@Component({
    template: '<router-outlet></router-outlet>'
})
export class Knowledge {
    constructor(
        private _title: Title, 
        private _appState: AppState
    ) {
        const CurrTitle = 'Knowledge Base';
        _title.setTitle(CurrTitle);
        _appState.set('routeInfo', routeInfoCollector(CurrTitle));
    }
}