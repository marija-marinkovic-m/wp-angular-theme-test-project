/**
 * @module Search
 */
/**
 * 
 */
import { Component, OnInit, ElementRef, ViewChild, Renderer } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { SearchService, SearchResult } from './search.service';

@Component({
    selector: 'search-remax', 
    template: `
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
            <label class="mdl-button mdl-js-button mdl-button--icon" for="sample6">
                <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
                <input #term (keyup)="search(term.value)" class="mdl-textfield__input" type="text" id="sample6">
                <label class="mdl-textfield__label" for="sample-expandable">Search Input</label>
            </div>
        </div>
        <div [ngClass]="{'is-active': loading}" class="mdl-spinner is-upgraded mdl-js-spinner"></div>
        <ul #results class="results-list mdl-list" [ngClass]="{'hide': resultsListHidden}">
            <li class="mdl-list__item mdl-list__item--two-line" *ngFor="let item of items | async">
                <a [routerLink]="[item.slug]" class="mdl-list__item-primary-content">
                    <i class="mdl-list__item-icon" [ngClass]="item.icon"></i>
                    <span class="results-title">{{ item.title }}</span>
                    <span class="mdl-list__item-sub-title results-title">{{ item.desc }}</span>
                </a>
            </li>
        </ul>
    `, 
    providers: [SearchService], 
    styles: [`
        :host {position: relative; display: block;}
        .results-list {
            position: absolute; top: 53px; left: 32px;
            max-height: 480px;
            background: #ffffff;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
            overflow: auto;
            }
            .results-list a {width: 100%;}
            .results-title {
                display: block; max-width: 300px;
                overflow: hidden; white-space: nowrap; text-overflow: ellipsis;
            }
            .results-list.hide {display: none;}
    `]
})
export class Search {
    @ViewChild('results') results: ElementRef;
    @ViewChild('term') termInput: ElementRef;

    loading: boolean;
    items: Observable<SearchResult[]>;
    searchTermStream: Subject<string> = new Subject<string>();
    resultsListHidden: boolean = true;
    uglyFixTimeout: any;

    constructor(
        private _dataService: SearchService, 
        private _renderer: Renderer
    ) {}

    ngOnInit() {
        this.searchTermStream
            .debounceTime(400)
            .subscribe((term:string) => {
                this.items = this._dataService.search(term)
                    .map(i => {
                        this.loading = false;
                        return i;
                    });
            });

        this.results.nativeElement.addEventListener('click', () => {
            this.resultsListHidden = true;
        });

        this.termInput.nativeElement.addEventListener('blur', () => {
            setTimeout(() => {
                this.results.nativeElement.style.opacity = 0;
                this.uglyFixTimeout = setTimeout(() => {
                    this.results.nativeElement.style.display = 'none';
                }, 1000);
            }, 100);
        });
        this.termInput.nativeElement.addEventListener('focus', () => {
            clearTimeout(this.uglyFixTimeout);
            this.results.nativeElement.removeAttribute('style');
            this.resultsListHidden = false;
        });
    }

    search(term: string) {
        this.loading = !(term.length < 3);
        this.searchTermStream.next(term);
    }
}