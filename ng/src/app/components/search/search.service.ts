/**
 * @module Search
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { wp_data, Config } from '../../../wp.config';

export interface SearchResponse {
    data: Array<SearchResult>
}
export interface SearchResult {
    title: string;
    slug: string;
}

@Injectable()
export class SearchService {
    private wp: Config = Object.assign({}, wp_data);
    private searchUrl = this.wp.api_root + 'remax/v1/search';

    constructor(
        private _http: Http
    ) {}

    search(term: string):Observable<SearchResult[]> {
        if (term.length >= 3) {
            let searchTerm = encodeURI(term);
            return this._http
                .get(`${this.searchUrl}?term=${searchTerm}`)
                .map(i => {
                    let body = i.json();
                    return body.data;
                });
        } 
        return Observable.of([]);
    
    }
}