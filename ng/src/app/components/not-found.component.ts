/**
 * @module 404Component
 */
/**
 * 
 */
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AppState, routeInfoCollector } from '../app.service';

@Component({
    template: `
    <main class="mdl-grid">
        <div class="mdl-cell mdl-cell--9-col mdl-card mdl-shadow--2p">
            <div class="main-placeholder content">
                <h5>Try search...</h5>
            </div>
        </div>
    </main>`
})
export class NotFoundComponent implements OnInit {
    constructor(
        public titleService: Title, 
        private _appState: AppState
    ) {}

    ngOnInit() {
        this.titleService.setTitle('Resource Not Found');
        this._appState.set('routeInfo', routeInfoCollector('Resource not found', '', 'fa fa-exclamation-triangle'));
    }
}