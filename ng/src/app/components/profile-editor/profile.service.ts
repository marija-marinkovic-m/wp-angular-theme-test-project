/**
 * @module Profile Editor
 */
/**
 * Profile service and Guard from editing 
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { wp_data, Config } from '../../../wp.config';
import { Agent } from '../../agents';
import { User } from '../../auth/shared';

import { ProfileEditor } from './profile-edit.component';

interface ProfileFormData {
    email: string;
    displayName: string;
    bio: string;
    publicEmail: string; 
    publicPhone: string; 
    facebook: string;
    twitter: string; 
    linkedin: string;
}

@Injectable()
export class ProfileService {
    private wp: Config = Object.assign({}, wp_data);
    private dataUrl = this.wp.api_root + 'remax/v1/users';

    constructor(
        private _http: Http
    ) {}

    dataGetter(slug: string):Observable<Agent> {
        return this._http
                .get(`${this.dataUrl}/${slug}`)
                .map(i => i.json());
    }
    dataSetter(values: ProfileFormData, slug: string): Observable<Response> {
        let userEditor: User = JSON.parse(localStorage.getItem('authorized'));
        let editorId = userEditor.ID;
        let body = new FormData();

        body.append("action", "rmx_update_user");
        body.append("editor_id", editorId);

        for (var prop in values) {
            body.append(prop, values[prop]);
        }

        return this._http.post(`${this.dataUrl}/${slug}`, body)
            .map(r => r.json());
    }
}


/**
 * CanActivate edit profile: 
 * check if user has capabilities to edit current profile
 */
@Injectable()
export class EditProfileGuard implements CanActivate {
    constructor(
        private _router: Router
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot
    ) {
        const currUser = JSON.parse(localStorage.getItem('authorized'));
        const loggedIn:boolean = null !== currUser;

        if ( loggedIn && 
            (currUser.user_nicename === next.params['slug'] || currUser.roles.indexOf('administrator') > -1) ) {
            return true;
        }
        return false;
    }
}

/**
 * CanDeactivate ProfileEditor
 * check if changes have been made 
 */
export class ConfirmDeactivateProfileEditorGuard implements CanDeactivate<ProfileEditor> {
    canDeactivate(
        target: ProfileEditor
    ) {
        if (target.hasChanges()) {
            return window.confirm('Do you really want to exit profile editor? All changes will be lost.');
        }
        return true;
    }
}