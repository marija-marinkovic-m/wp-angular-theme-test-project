/**
 * @module Profile Editor
 */
/**
 * 
 */
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Header, Editor } from '../../common';
import { ProfileService } from './profile.service';
import { ValidationService, ControlMessages, AuthService } from '../../auth';

import { Agent } from '../../agents';
import { routeInfoCollector, AppState } from '../../app.service';


@Component({
    template: `
    <div *ngIf="!user" class="mdl-spinner mdl-js-spinner is-active is-upgraded"></div>
    <main *ngIf="user && profileForm" class="mdl-grid">
        <div class="mdl-cell mdl-cell--9-col mdl-card mdl-shadow--2p mdl-tabs is-upgraded">
            <section class="main-placeholder content">
                <h2><strong>Edit: </strong>{{ user.name }}</h2>
                <form novalidate [formGroup]="profileForm" (submit)="false" class="profileForm">

                    <div class="form-field mdl-textfield mdl-js-textfield">
                        
                        <label for="user-email"><h3>Email</h3> <span class="info">This email will be used for notifications from RemaxIntranet administrators, and will not be shown publicly.</span></label>
        
                        <input formControlName="email" class="mdl-textfield__input" type="text" id="user-email">

                        <control-messages [control]="profileForm.controls.email"></control-messages>
                        
                        
                    </div>

                    <div class="form-field mdl-textfield mdl-js-textfield">
                        <label for="display-name"><h3>Public name</h3></label>
                        <input formControlName="displayName" class="mdl-textfield__input" type="text" id="user-email">

                        <control-messages [control]="profileForm.controls.displayName"></control-messages>
                    </div>

                    <div class="form-field">
                        <h3>About me </h3>
                        <p-editor formControlName="bio" [style]="{'height': '320px'}"></p-editor>
                        <!-- <p>Value: {{ profileForm.controls.bio.value || 'empty' }}</p> -->
                    </div>

                    <div class="form-field mdl-textfield mdl-js-textfield">
                        <label for="contact-phone">
                            <h3>Contact telephone number(s)</h3> 
                            <span class="info"><strong>One telephone number per line.</strong><br>These phone numbers will be available for other agents.</span>
                        </label>

                        <textarea formControlName="publicPhone" class="mdl-textfield__input" type="text" rows="4" id="contact-phone"></textarea>
                    </div>

                    <div class="form-field mdl-textfield mdl-js-textfield">
                        <label for="contact-email">
                            <h3>Contact email address(es)</h3> 
                            <span class="info"><strong>One email address per line.</strong><br>These emails will be available for other agents.</span>
                        </label>

                        <textarea formControlName="publicEmail" class="mdl-textfield__input" type="text" rows="4" id="contact-email"></textarea>
                    </div>

                    <div class="form-field mdl-textfield mdl-js-textfield">
                        <h3>Social networks </h3>
                        <!-- FACEBOOK -->
                        <p><label for="fb"><h4>Facebook</h4></label>
                        <input formControlName="facebook" class="mdl-textfield__input" type="text" id="fb"></p>
                        <!-- TWITTER -->
                        <p><label for="tw"><h4>Twitter</h4></label>
                        <input formControlName="twitter" class="mdl-textfield__input" type="text" id="tw"></p>
                        <!-- LINKEDIN -->
                        <p><label for="in"><h4>Linkedin</h4></label>
                        <input formControlName="linkedin" class="mdl-textfield__input" type="text" id="in"></p>
                    </div>

                    <button (click)="onSubmit()" [disabled]="!profileForm.valid || formSubmissionDone" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                        Save Changes
                    </button>
                    
                </form>

                <p *ngIf="responseMsg">{{ responseMsg }}</p>

            </section>
        </div>
    </main>
    `, 
    styles: [`
    .profileForm {margin-bottom: 25px;}
    .profileForm .mdl-textfield {width: 100%}
        .profileForm .mdl-textfield label {display: block; margin-bottom: 20px;}
        .profileForm .mdl-textfield label h3 {margin-bottom: 0;}
    .profileForm .info {font-style: italic; color: #ccc;}
    `], 
    directives: [Header, Editor, ControlMessages], 
    providers: [ProfileService]
})
export class ProfileEditor implements OnInit, AfterViewInit {
    
    public user: Agent;
    public profileForm: FormGroup;
    public slug: string;
    public formSubmissionDone: boolean = false;
    public responseMsg: string;

    constructor(
        private _activatedRoute: ActivatedRoute, 
        private _dataService: ProfileService, 
        private _titleService: Title, 
        private _appState: AppState, 
        private _formBuilder: FormBuilder, 
        private _authService: AuthService
    ) {}

    ngOnInit() {
        this._activatedRoute.params.subscribe(
            params => {
                this.slug = params['slug'];
                this._dataService.dataGetter(this.slug)
                    .subscribe(data => {
                        this.user = data;
                        this._titleService.setTitle(this.user.name);
                        this._appState.set('routeInfo', routeInfoCollector(this.user.name, this.user.office, '', this.user.avatar_urls['96']));

                        this.buildForm();
                    });
            }
        );
    }

    ngAfterViewInit() {
        componentHandler.upgradeDom();
    }

    buildForm() {
        let pubEmail = this.user.contact.hasOwnProperty('email') && this.user.contact.email.length ? this.user.contact.email.join("\n\r") : '';
        let pubPhone = this.user.contact.hasOwnProperty('phone') && this.user.contact.phone.length ? this.user.contact.phone.join("\n\r") : '';
        let fb = this.user.social.facebook || '';
        let tw = this.user.social.twitter || '';
        let linkedin = this.user.social.linkedin || '';

        this.profileForm = this._formBuilder.group({
            email: [this.user.email, [Validators.required, ValidationService.emailValidator]], 
            displayName: [this.user.name, [Validators.required]], 
            bio: [this.user.bio], 
            publicEmail: [pubEmail], 
            publicPhone: [pubPhone], 
            facebook: [fb], 
            twitter: [tw], 
            linkedin: [linkedin]
        });
    }

    onSubmit() {
        if (this.profileForm.dirty && !this.formSubmissionDone) {
            this._dataService.dataSetter(this.profileForm.value, this.slug)
                .subscribe(
                    r => {
                        this.formSubmissionDone = true;
                        this.responseMsg = r['msg'];

                        this._authService.setUserData().subscribe(r => {
                            let currentHref = window.location.href;
                            window.location.assign(currentHref.substr(0, currentHref.lastIndexOf('/edit')));
                        });

                    }, 
                    e => {
                        let error = e.json();
                        this.responseMsg = error['message'];
                    }
                );
        } else {
            window.alert('You haven\'t made any change.');
        }
    }

    hasChanges() {
        if (this.profileForm.dirty && !this.formSubmissionDone) {
            return true;
        }
        return false;
    }
}