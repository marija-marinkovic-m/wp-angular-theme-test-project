/**
 * @module AppComponent 
 * @preferred Application shell component
 */
/**
 * 
 */
export * from './app.component';
export * from './app.service';

import { AuthService, AuthGuard } from './auth';
import { AppState } from './app.service';
import { 
	PageGuard, PageDataService, 
	MarketingDataService, 
	TrainingsGuard, TrainingsService, 
  	KnowledgeGuard, KnowledgeService, 
	ConfirmDeactivateProfileEditorGuard, EditProfileGuard
} from './components';

import { AgentsData } from './agents';

// Application wide providers
export const APP_PROVIDERS = [
	AuthService, AuthGuard, 
	AppState, 
	PageGuard, PageDataService, 
	MarketingDataService, 
	TrainingsGuard, TrainingsService, 
	KnowledgeGuard, KnowledgeService, 
	AgentsData, 
	ConfirmDeactivateProfileEditorGuard, EditProfileGuard
];
