/**
 * @module AppService
 * 
 */
/**
 * 
 */
import { Injectable } from '@angular/core';
import { HmrState } from 'angular2-hmr';

import { wp_data } from '../wp.config';

@Injectable()
export class AppState {
  // @HmrState() is used by HMR to track the state of any object during HMR (hot module replacement)
  @HmrState() _state = { };

  constructor() {}

  // already return a clone of the current state
  get state() {
    return this._state = this._clone(this._state);
  }
  // never allow mutation
  set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }


  get(prop?: any) {
    // use our state getter for the clone
    const state = this.state;
    return state.hasOwnProperty(prop) ? state[prop] : state;
  }

  set(prop: string, value: any) {
    // internally mutate our state
    return this._state[prop] = value;
  }


  _clone(object) {
    // simple object clone
    return JSON.parse(JSON.stringify( object ));
  }  
}

export interface RouteInfo {
	title?: string, 
	icon?: string, 
	desc?: string, 
	avatar?: string
};

export const routeInfoCollector = (title: string, desc?: any, icon?: string, avatar?: string):RouteInfo => {
	let routeInfo: RouteInfo = {}
	routeInfo.title = title;

	if (typeof desc !== "undefined" && desc !== '') {
		routeInfo.desc = desc;
	}

	if (typeof icon !== "undefined" && icon !== '') {
		routeInfo.icon = icon;
	} else {
		let currentMenuObj = wp_data.menu.filter(i => {
			let regEx = new RegExp(`^${routeInfo.title}$`, 'i');
			return regEx.test(i.title) && i.class !== '';
		});
		if (currentMenuObj.length) {
			routeInfo.icon = currentMenuObj[0].class;
		}
	}

	if (typeof avatar !== "undefined" && avatar !== '') {
		routeInfo.avatar = avatar;
	}

	return routeInfo;
};
