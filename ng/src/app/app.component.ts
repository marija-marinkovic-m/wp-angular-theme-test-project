/**
 * @module AppComponent 
 * @preferred 
 * 
 * Application shell component
 */
/**
 * 
 */
/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation, AfterViewInit, trigger, state, style, transition, animate } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppState } from './app.service';
import { wp_data, MenuItem, Config, ANIM_SPEED } from '../wp.config';
import { MenuLinkItem, IntroSection } from './partials';

import { AuthService, User } from './auth';
import { Search } from './components/search';

const LoggedIn: boolean = null !== localStorage.getItem('authorized');

/*
 * App Component
 * Top Level Component
 */
@Component({
	selector: 'app',
	encapsulation: ViewEncapsulation.None,
	styleUrls: [
	'./app.style.css'
	],
	templateUrl: LoggedIn ? './app.template.html' : './app.template.stripped.html', 
	directives: [MenuLinkItem, IntroSection, Search], 
	animations: [
		trigger('viewState', [
			state('in', style({opacity: 1})), 
			state('void', style({opacity: 0})), 
			transition('void => *', [
				style({opacity: 0.35}), 
				animate('2s ease-in')
			])
		])
	]
})
export class App implements AfterViewInit {
	wp: Config;
	user: User;
	remaxIntranetLogo: string = `${wp_data.template_directory}assets/img/logo.png`;
	agentAvatarObs: Observable<string>;
	agentAvatarUrl: string;


	constructor(
		public appState: AppState, 
		private _authService: AuthService
	) {}

	ngOnInit() {
		this.wp = Object.assign({}, wp_data);
		this.user = JSON.parse(localStorage.getItem('authorized'));
		this.wp.menu.map(el => {
			el.children = this.pullChildItems(el);
			return el;
		});
		this.appState.set('routeInfo', null);

		this.agentAvatarObs = this._authService.getAvatar('48')
			.map(r => this.agentAvatarUrl = r);
	}

	ngAfterViewInit() {
		componentHandler.upgradeDom(); // upgrade all mdl components
	}

	pullChildItems(item: MenuItem) {
		let currentItem = Object.assign({}, item);
		return this.wp.menu.filter(i => +i.item_parent === currentItem.item_id);
	}

	onLogout() {
		this._authService.logout()
			.subscribe(
				r => location.assign(this.wp.site_url)
			);
	}

}
